package aygx.mybatis.plus.service.impl;

import aygx.mybatis.plus.entity.User;
import aygx.mybatis.plus.mapper.UserMapper;
import aygx.mybatis.plus.service.UserService;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author 暗影孤星
 * @date 2022/3/12 11:47
 * @description ServiceImpl实现了IService，提供了IService中基础功能的实现
 * 若ServiceImpl无法满足业务需求，则可以使用自定的UserService定义方法，并在实现类中实现
 */
@Service
@DS("master")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
