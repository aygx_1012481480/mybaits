package aygx.mybatis.plus;

import aygx.mybatis.plus.entity.Product;
import aygx.mybatis.plus.entity.User;
import aygx.mybatis.plus.mapper.ProductMapper;
import aygx.mybatis.plus.mapper.UserMapper;
import aygx.mybatis.plus.service.UserService;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author 暗影孤星
 * @date 2022/3/12 11:50
 * @description
 */
@Slf4j
@SpringBootTest
public class MybatisPlusApplicationTests {
    @Resource
    private UserMapper userMapper;
    @Resource
    private UserService userService;
    @Resource
    private ProductMapper productMapper;

    @Test
    void testSelectList() {
        // selectList()方法的参数：封装了查询条件 null 表示无条件
        userMapper.selectList(null).forEach(System.out::println);
    }

    @Test
    public void testSelect() {

        //按id查询
        System.out.println(userMapper.selectById(1));

        // 按id列表查询
        userMapper.selectBatchIds(Stream.of(1, 2, 3).collect(Collectors.toList())).forEach(System.out::println);

        // 按条件查询 此处使用列名
        Map<String, Object> map = new HashMap<>();
        map.put("name", "Jone");
        map.put("age", 18);
        userMapper.selectByMap(map).forEach(System.out::println);
    }

    /**
     * 插入
     */
    @Test
    public void testInsert() {

        User user = new User("暗影孤星", 18, "123456789!qq.com");
        System.out.println("影响的行数：" + userMapper.insert(user));
        System.out.println("id自动回填：" + user.getId());
    }

    /**
     * 根据ID修改
     */
    @Test
    public void testUpdate() {
        System.out.println("影响的行数：" + userMapper.updateById(User.builder().id(1L).age(28).build()));
    }

    /**
     * 根据ID删除
     */
    @Test
    public void testDelete() {
        System.out.println("影响的行数：" + userMapper.deleteById(1L));
    }

    /**
     * 根据ID批量删除
     */
    @Test
    public void testDeleteBatchIds() {
        System.out.println("受影响行数：" + userMapper.deleteBatchIds(Arrays.asList(1L, 2L, 3L)));
    }

    /**
     * 根据map集合中所设置的条件删除记录
     */
    @Test
    public void testDeleteByMap() {
        System.out.println("受影响行数：" + userMapper.deleteByMap(JSONUtil.createObj().set("age", 18).set("name", "暗影孤星")));
    }

    /**
     * 根据ID查询
     */
    @Test
    public void testSelectById() {
        System.out.println(userMapper.selectById(4L));
    }

    /**
     * 根据ID集合查询
     */
    @Test
    public void testSelectBatchIds() {
        userMapper.selectBatchIds(Arrays.asList(4L, 5L)).forEach(System.out::println);
    }

    /**
     * 根据map条件查询
     */
    @Test
    public void testSelectByMap() {
        userMapper.selectByMap(JSONUtil.createObj().set("age", 18).set("name", "暗影孤星")).forEach(System.out::println);
    }

    /**
     * 批量插入
     */
    @Test
    public void testSaveBatch() {
        // SQL长度有限制，海量数据插入单条SQL无法实行，
        // 因此MP将批量插入放在了通用Service中实现，而不是通用Mapper
        ArrayList<User> users = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setName("ybc" + i);
            user.setAge(20 + i);
            users.add(user);
        }
        userService.saveBatch(users);
    }

    /**
     * 获取总记录数据
     */
    @Test
    public void testGetCount() {
        System.out.println("总记录数：" + userService.count());
    }

    @Test
    public void test01() {
        // SELECT id,name,age,email FROM user WHERE (name LIKE ? AND age BETWEEN ? AND ? AND email IS NOT NULL) ORDER BY age DESC,id ASC
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>()
                .like("name", "a")
                .between("age", 20, 30)
                .isNotNull("email")
                .orderByDesc("age")
                .orderByAsc("id");
        userMapper.selectList(queryWrapper).forEach(System.out::println);
    }

    @Test
    public void test02() {
        // 条件构造器也可以构建删除语句的条件
        // DELETE FROM user WHERE (email IS NULL)
        System.out.println("受影响的行数：" + userMapper.delete(new QueryWrapper<User>().isNull("email")));
    }

    @Test
    public void test03() {
        // UPDATE user SET age=?, email=? WHERE (name LIKE ? AND age > ? OR email IS NULL)
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>()
                .like("name", "a")
                .gt("age", 20)
                .or()
                .isNull("email");
        User user = new User();
        user.setAge(18);
        user.setEmail("123456789@.com");
        System.out.println("受影响的行数：" + userMapper.update(user, queryWrapper));
    }

    @Test
    public void test04() {
        // lambda表达式内的逻辑优先运算
        // UPDATE user SET name=?, email=? WHERE (name LIKE ? AND (age > ? OR email IS NULL))
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>()
                .like("name", "a")
                .and(i -> i.gt("age", 20).or().isNull("email"));
        System.out.println("受影响的行数：" + userMapper.update(User.builder().name("aygx").email("123456789@email").build(), queryWrapper));
    }

    @Test
    public void test05() {
        // SELECT name,age FROM user
        // selectMaps()返回Map集合列表，通常配合select()使用，避免User对象中没有被查询到的列值为null
        userMapper.selectMaps(new QueryWrapper<User>().select("name", "age")).forEach(System.out::println);
    }

    @Test
    public void test06() {
        // SELECT id,name,age,email FROM user WHERE (id IN (select id from user where id <= 3))
        userMapper.selectList(new QueryWrapper<User>().inSql("id", "select id from user where id <= 3")).forEach(System.out::println);
    }

    @Test
    public void test07() {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<User>()
                .set("age", 18)
                .like("name", "a")
                .set("email", "123456789@qq.com.com")
                .and(i -> i.gt("age", 20).or().isNull("email"));
        // UPDATE user SET age=?,email=? WHERE (name LIKE ? AND (age > ? OR email IS NULL))
        System.out.println("受影响的行数：" + userMapper.update(null, updateWrapper));
        // UPDATE user SET name=?, age=?, age=?,email=? WHERE (name LIKE ? AND (age > ? OR email IS NULL))
        System.out.println("受影响的行数：" + userMapper.update(User.builder().name("暗影孤星").age(12).build(), updateWrapper));

    }

    @Test
    public void test08() {

        // SELECT id,name,age,email FROM user WHERE (age >= ? AND age <= ? AND name LIKE ?)
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
                .ge(User::getAge, 10)
                .le(User::getAge, 20)
                .like(User::getName, "aygx");
        userMapper.selectList(queryWrapper).forEach(System.out::println);
    }

    @Test
    public void test09() {
        // UPDATE user SET age=?,email=? WHERE (name LIKE ? AND (age < ? OR email IS NULL))
        LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<User>()
                .set(User::getAge, 18)
                .like(User::getName, "aygx")
                .set(User::getEmail, "123456789@qq.com")
                .and(i -> i.lt(User::getAge, 24).or().isNull(User::getEmail));

        System.out.println("受影响的行数：" + userMapper.update(new User(), updateWrapper));
    }

    @Test
    public void testPage() {
        // 设置分页参数
        Page<User> page = new Page<>(1, 5);
        userMapper.selectPage(page, null);
        // 获取分页数据
        page.getRecords().forEach(System.out::println);
        System.out.println("当前页数：" + page.getCurrent());
        System.out.println("每页显示条数：" + page.getSize());
        System.out.println("总记录数：" + page.getTotal());
        System.out.println("总页数：" + page.getPages());
        System.out.println("是否有上一页：" + page.hasPrevious());
        System.out.println("是否有下一页：" + page.hasNext());
    }

    @Test
    public void testSelectPageVo() {
        //设置分页参数
        Page<User> page = new Page<>(1, 5);
        userMapper.selectPageVo(page, 20);
        //获取分页数据
        page.getRecords().forEach(System.out::println);
        System.out.println("当前页： " + page.getCurrent());
        System.out.println("每页显示的条数： " + page.getSize());
        System.out.println("总记录数： " + page.getTotal());
        System.out.println("总页数： " + page.getPages());
        System.out.println("是否有上一页： " + page.hasPrevious());
        System.out.println("是否有下一页： " + page.hasNext());
    }

@Test
public void testConcurrentUpdate() {
    // 1、小李
    Product p1 = productMapper.selectById(1L);
    System.out.println("小李取出的价格： " + p1.getPrice());
    // 2、小王
    Product p2 = productMapper.selectById(1L);
    System.out.println("小王取出的价格： " + p2.getPrice());

    // 3、小李将价格加了50元，存入了数据库
    p1.setPrice(p1.getPrice() + 50);
    System.out.println("小李修改结果： " + productMapper.updateById(p1));
    // 4、小王将商品减了30元，存入了数据库
    p2.setPrice(p2.getPrice() - 30);
    int result2 = productMapper.updateById(p2);
    if (0 == result2) {
        // 失败重试，重新获取version并更新
        p2 = productMapper.selectById(1L);
        p2.setPrice(p2.getPrice() - 30);
        result2 = productMapper.updateById(p2);
    }
    System.out.println("小王修改结果： " + result2);
    // 最后的结果
    System.out.println("最后的结果： " + productMapper.selectById(1L).getPrice());
}
}
