package aygx.mybatis.repository;

import aygx.mybatis.entity.Person;
import org.apache.ibatis.annotations.Select;

/**
 * @author 暗影孤星
 * @date 2022/03/12 13:37
 * @description
 */
public interface PersonRepository {
    /**
     * 根据ID获取用户数据
     *
     * @param id 主键
     * @return 用户数据
     */
    @Select("SELECT * FROM person WHERE id=#{id}")
    Person selectById(Integer id);
}
