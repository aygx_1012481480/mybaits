package aygx.mybatis.entity;

import lombok.Data;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2021/7/5 23:56
 * @description 班级
 */
@Data
public class Classes {

    /**
     * 名称
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 学生
     */
    private List<Student> studentList;
}
