package aygx.mybatis.test;

import aygx.mybatis.repository.CardRepository;
import aygx.mybatis.repository.ClassesRepository;
import aygx.mybatis.repository.StudentRepository;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;

/**
 * @author 暗影孤星
 * @date 2021/7/6 23:12
 * @description
 */
public class AnnotationTest {
    @Test
    public void oneToOne() throws Exception {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //4.获取OneToOneMapper接口的实现类对象
        CardRepository repository = sqlSession.getMapper(CardRepository.class);

        //5.调用实现类的方法，接收结果
        repository.selectAll().forEach(System.out::println);


        //7.释放资源
        sqlSession.close();
        inputStream.close();
    }

    @Test
    public void oneToMany() throws Exception {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        ClassesRepository repository = sqlSession.getMapper(ClassesRepository.class);

        //5.调用实现类的方法，接收结果
        repository.selectAll().forEach(System.out::println);

        //7.释放资源
        sqlSession.close();
        inputStream.close();
    }

    @Test
    public void manyToMany() throws Exception {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        StudentRepository mapper = sqlSession.getMapper(StudentRepository.class);

        //5.调用实现类的方法，接收结果
        mapper.selectList().forEach(System.out::println);

        //7.释放资源
        sqlSession.close();
        inputStream.close();
    }
}
