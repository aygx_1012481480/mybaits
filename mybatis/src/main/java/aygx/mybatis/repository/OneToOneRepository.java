package aygx.mybatis.repository;

import aygx.mybatis.entity.Card;
import aygx.mybatis.entity.Person;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2022/03/12 12:38
 * @description
 */
public interface OneToOneRepository {
    List<Card> selectAll();

    /**
     * 获取身份证数据
     *
     * @param id 身份证
     * @return 人员
     */
    Card getCard(@Param("id") Integer id);

//    /**
//     * 获取员工信息
//     *
//     * @param id 人员ID
//     * @return 人员
//     */
//    Person getPerson(@Param("id") Integer id);
}
