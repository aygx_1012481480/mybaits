package aygx.mybatis.repository;

import aygx.mybatis.entity.Classes;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2022/03/12 14:01
 * @description
 */
public interface ClassesRepository {

    /**
     * 获取班级数据
     *
     * @return 班级数据
     */
    @Select("SELECT * FROM classes")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(
                    property = "studentList",                                                      // 被包含对象的变量名
                    javaType = List.class,                                                         // 被包含对象的实际数据类型
                    column = "id",                                                                 // 根据查询出的classes表的id字段来查询student表
                    many = @Many(select = "aygx.mybatis.repository.StudentRepository.selectByCid") // many=@Many一对多查询的固定写法
            )
    })
    List<Classes> selectAll();
}
