package aygx.mybatis.test;

import aygx.mybatis.entity.Student;
import aygx.mybatis.repository.ManyToManyRepository;
import aygx.mybatis.repository.OneToManyRepository;
import aygx.mybatis.repository.OneToOneRepository;
import aygx.mybatis.repository.StudentRepository;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.datasource.unpooled.UnpooledDataSource;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.junit.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 暗影孤星
 * @date 2022/03/12 10:37
 * @description
 */
public class MybatisTest {

    @Test
    public void selectList() throws IOException {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 执行查询（namespace+id，映射实体）根据属性名称和字段一一对应
        List<Student> studentList = sqlSession.selectList("aygx.mybatis.repository.StudentMapper.selectAll", Student.class);

        System.out.println(studentList);

        inputStream.close();
        sqlSession.close();
    }

    @Test
    public void config() throws IOException {
        TransactionFactory jdbcTransactionFactory = new JdbcTransactionFactory();
        DataSource dataSource = new UnpooledDataSource("com.mysql.jdbc.Driver", "jdbc:mysql://127.0.0.1:3306/mybatis?useSSL=false", "root", "123456");
        Configuration configuration = new Configuration(new Environment("development", jdbcTransactionFactory, dataSource));
        configuration.setLazyLoadingEnabled(true);
        configuration.addMapper(StudentRepository.class);
        configuration.getTypeAliasRegistry().registerAlias(Student.class);

        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

    }

    @Test
    public void proxy() throws IOException {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);
        // 执行查询（namespace+id，映射实体）根据属性名称和字段一一对应
        List<Student> studentList = studentRepository.selectAll();

        System.out.println(studentList);

        inputStream.close();
        sqlSession.close();

    }

    @Test
    public void ifSql() throws IOException {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);
        // 执行查询（namespace+id，映射实体）根据属性名称和字段一一对应
        Student student = new Student();
        List<Student> studentList;
        studentList = studentRepository.selectNameAndAge(student);
        System.out.println(studentList);

        student.setName("暗");
        studentList = studentRepository.selectNameAndAge(student);
        System.out.println(studentList);

        student.setAge(18);
        studentList = studentRepository.selectNameAndAge(student);
        System.out.println(studentList);

        student.setName(null);
        studentList = studentRepository.selectNameAndAge(student);
        System.out.println(studentList);

        inputStream.close();
        sqlSession.close();
    }

    @Test
    public void chooseSql() throws IOException {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);
        // 执行查询（namespace+id，映射实体）根据属性名称和字段一一对应
        Student student = new Student();
        List<Student> studentList;
        studentList = studentRepository.selectChoose(student);
        System.out.println(studentList);

        student.setName("暗");
        studentList = studentRepository.selectChoose(student);
        System.out.println(studentList);

        student.setAge(18);
        studentList = studentRepository.selectChoose(student);
        System.out.println(studentList);

        student.setName(null);
        studentList = studentRepository.selectChoose(student);
        System.out.println(studentList);

        inputStream.close();
        sqlSession.close();
    }

    @Test
    public void whereSql() throws IOException {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);
        // 执行查询（namespace+id，映射实体）根据属性名称和字段一一对应
        Student student = new Student();
        List<Student> studentList;
        studentList = studentRepository.selectWhere(student);
        System.out.println(studentList);

        student.setName("暗");
        studentList = studentRepository.selectWhere(student);
        System.out.println(studentList);

        student.setAge(18);
        studentList = studentRepository.selectWhere(student);
        System.out.println(studentList);

        student.setName(null);
        studentList = studentRepository.selectWhere(student);
        System.out.println(studentList);

        inputStream.close();
        sqlSession.close();
    }

    @Test
    public void setSql() throws IOException {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);
        Student student = new Student();
        student.setId(2);
        studentRepository.updateSet(student);

        student.setName("暗");
        studentRepository.updateSet(student);

        student.setAge(18);
        studentRepository.updateSet(student);

        student.setName(null);
        studentRepository.updateSet(student);

        sqlSession.commit();
        inputStream.close();
        sqlSession.close();
    }

    @Test
    public void foreachSql() throws IOException {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);

        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("暗", 15));
        studentList.add(new Student("影", 16));
        studentList.add(new Student("孤", 17));
        studentList.add(new Student("星", 18));
        studentRepository.insertForeach(studentList);

        sqlSession.commit();
        inputStream.close();
        sqlSession.close();
    }

    @Test
    public void page() throws IOException {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);

        //设置分页参数(当前页，煤业行数)
        PageHelper.startPage(2, 2);
        List<Student> studentList = studentRepository.selectAll();
        System.out.println(studentList);

        PageInfo<Student> pageInfo = new PageInfo<>(studentList);
        System.out.println(pageInfo);

        sqlSession.close();
        inputStream.close();
    }

    @Test
    public void oneToOne() throws Exception {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //4.获取OneToOneMapper接口的实现类对象
        OneToOneRepository oneToOneRepository = sqlSession.getMapper(OneToOneRepository.class);

        //5.调用实现类的方法，接收结果
        oneToOneRepository.selectAll().forEach(System.out::println);


        //7.释放资源
        sqlSession.close();
        inputStream.close();
    }

    @Test
    public void oneToMany() throws Exception {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        OneToManyRepository mapper = sqlSession.getMapper(OneToManyRepository.class);

        //5.调用实现类的方法，接收结果
        mapper.selectAll().forEach(System.out::println);

        //7.释放资源
        sqlSession.close();
        inputStream.close();
    }

    @Test
    public void manyToMany() throws Exception {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();

        ManyToManyRepository mapper = sqlSession.getMapper(ManyToManyRepository.class);

        //5.调用实现类的方法，接收结果
        mapper.selectAll().forEach(System.out::println);

        //7.释放资源
        sqlSession.close();
        inputStream.close();
    }

}
