package aygx.mybatis.test;

import aygx.mybatis.repository.StudentRepository;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author 暗影孤星
 * @date 2022/4/19 19:18
 * @description 缓存
 */
public class CacheTest {

    @Test
    public void levelOne() throws IOException {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 获取mapper
        //DEBUG - ==>  Preparing: SELECT * FROM student
        //DEBUG - ==> Parameters:
        //DEBUG - <==      Total: 5
        //[Student(id=1, name=暗影孤星, age=18, createTime=2022-03-23T20:16:29, updateTime=2022-03-23T20:16:29, courseList=null), Student(id=2, name=暗, age=16, createTime=2022-03-23T20:16:52, updateTime=2022-03-23T20:16:55, courseList=null), Student(id=3, name=影, age=14, createTime=2022-03-23T20:17:05, updateTime=2022-03-23T20:17:07, courseList=null), Student(id=4, name=孤, age=12, createTime=2022-03-23T20:17:32, updateTime=2022-03-23T20:17:34, courseList=null), Student(id=5, name=星, age=10, createTime=2022-03-23T20:17:46, updateTime=2022-03-23T20:17:48, courseList=null)]
        //[Student(id=1, name=暗影孤星, age=18, createTime=2022-03-23T20:16:29, updateTime=2022-03-23T20:16:29, courseList=null), Student(id=2, name=暗, age=16, createTime=2022-03-23T20:16:52, updateTime=2022-03-23T20:16:55, courseList=null), Student(id=3, name=影, age=14, createTime=2022-03-23T20:17:05, updateTime=2022-03-23T20:17:07, courseList=null), Student(id=4, name=孤, age=12, createTime=2022-03-23T20:17:32, updateTime=2022-03-23T20:17:34, courseList=null), Student(id=5, name=星, age=10, createTime=2022-03-23T20:17:46, updateTime=2022-03-23T20:17:48, courseList=null)]
        System.out.println(sqlSession.getMapper(StudentRepository.class).selectAll());
        System.out.println(sqlSession.getMapper(StudentRepository.class).selectAll());

        // 关闭资源
        inputStream.close();
        sqlSession.close();
    }

    @Test
    public void levelTwo() throws IOException {
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory1 = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("MybatisConfig.xml"));
        // 获取SqlSession
        SqlSession sqlSession1 = sqlSessionFactory1.openSession();
        // 获取mapper
        //DEBUG - ==>  Preparing: SELECT * FROM student
        //DEBUG - ==> Parameters:
        //DEBUG - <==      Total: 5
        //[Student(id=1, name=暗影孤星, age=18, createTime=2022-03-23T20:16:29, updateTime=2022-03-23T20:16:29, courseList=null), Student(id=2, name=暗, age=16, createTime=2022-03-23T20:16:52, updateTime=2022-03-23T20:16:55, courseList=null), Student(id=3, name=影, age=14, createTime=2022-03-23T20:17:05, updateTime=2022-03-23T20:17:07, courseList=null), Student(id=4, name=孤, age=12, createTime=2022-03-23T20:17:32, updateTime=2022-03-23T20:17:34, courseList=null), Student(id=5, name=星, age=10, createTime=2022-03-23T20:17:46, updateTime=2022-03-23T20:17:48, courseList=null)]
        System.out.println(sqlSession1.getMapper(StudentRepository.class).selectAll());

        // 关闭资源
        sqlSession1.close();

        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory2 = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("MybatisConfig.xml"));
        // 获取SqlSession
        SqlSession sqlSession2 = sqlSessionFactory2.openSession();
        System.out.println(sqlSession2.getMapper(StudentRepository.class).selectAll());


        sqlSession2.close();
    }
}
