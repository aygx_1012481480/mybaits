package aygx.mybatis.plus;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * @author 暗影孤星
 * @date 2022/3/13 15:47
 * @description
 */
public class FastAutoGeneratorTest {
    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql:///spring_boot_mybatis_plus?serverTimezone=GMT%2B8&characterEncoding=utf-8", "root", "123456")
                .globalConfig(builder -> {
                    builder.author("暗影孤星")                     // 设置作者
                            .fileOverride()                      // 覆盖已生成文件
                            .outputDir("G:\\01_Note\\01_Java\\03_JavaFrame\\02_Mybatis\\02_Code\\mybatis_plus\\src\\test")                   // 指定输出目录
                            .commentDate("yyyy-MM-dd hh:mm:ss"); //  指定注释日期格式化
                })
                .packageConfig(builder -> {
                    builder.parent("aygx")                                                  // 设置父包名
                            .moduleName("system")                                           // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapper, "/aygx")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("user");       // 设置需要生成的表名
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }


}
