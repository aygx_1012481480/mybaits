package aygx.mybatis.repository;

import aygx.mybatis.entity.Card;
import aygx.mybatis.entity.Classes;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2021/7/6 0:04
 * @description
 */
public interface OneToManyRepository {
    List<Classes> selectAll();
}
