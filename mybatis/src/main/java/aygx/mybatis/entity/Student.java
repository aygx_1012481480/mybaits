package aygx.mybatis.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 暗影孤星
 * @date 2022/03/12 10:37
 * @description 学生
 */
@Data
@NoArgsConstructor
public class Student implements Serializable {

    /**
     * 名称
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 课程
     */
    private List<Course> courseList;

    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }


}
