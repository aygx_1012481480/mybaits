package aygx.mybatis.entity;

import lombok.Data;

/**
 * @author 暗影孤星
 * @date 2021/7/6 22:46
 * @description 课程
 */
@Data
public class Course {
    /**
     * 名称
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;
}
