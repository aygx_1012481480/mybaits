# 1. Mybatis 概述

## 1. JDBC 问题

1.  `SQL` 语句和 `java` 代码高度耦合，不易维护。
2.  频繁创建和销毁数据库的连接会造成系统资源浪费从而影响系统性能。
3.  需要手动编写代码开始事务处理。
4.  存在大量重复代码，只有中间操作数据库的部分代码不同。
5.  需要手动解析结果集，一个一个字段的获取和注入。
6.  不支持缓存技术，无法优化。

## 2. ORM 概述

全称 `Object Relational Mapping` （对象关系映射）。它的作用是在关系型数据库和对象之间作一个映射，我们操作对象时 `ORM` 框架会根据映射完成对数据库的操作，无需和复杂的 `SQL` 语句打交道。

## 3. Mybatis 介绍

### 1. Mybatis 概述

`MyBatis` 本是 `Apache` 的一个开源项目 `iBatis`，2010年这个项目由 `Apache Software Foundation` 迁移到了`Google Code` 且改名为 `MyBatis` 。2013年11月迁移到 `GitHub`。

`Mybatis` 是一个优秀的基于 `java` 的持久层框架，它内部封装了 `JDBC`，使开发者只需要关注 `SQL` 语句本身，而不需要花费精力去处理加载驱动、创建连接、创建 `statement` 等繁杂的过程。

`Mybatis` 通过 `xml` 或注解的方式将要执行的各种 `statement` 配置起来，并通过 `java` 对象和 `statement` 中 `SQL` 的动态参数进行映射生成最终执行的 `SQL` 语句。最后 `Mybatis` 框架执行 `SQL` 并将结果映射为 `java` 对象并返回。采用 `ORM` 思想解决了实体和数据库映射的问题，对 `JDBC` 进行了封装，屏蔽了 `JDBC API` 底层访问细节，使我们不用与 `JDBC API` 打交道，就可以完成对数据库的持久化操作。

### 2. Mybatis 官网

https://mybatis.org/mybatis-3/zh/index.html

### 3. Mybatis 工作原理

![image-20210926112022615](img/image-20210926112022615.png)

1.  读取 `MyBatis` 配置文件。`mybatis-config.xml` 是 `MyBatis` 的全局配置文件，配置了 `MyBatis` 的运行环境等信息。
2.  加载 `SQL` 映射文件。该文件中配置了操作数据库的 `SQL` 语句，需要在 `mybatis-config.xml` 中配置加载。`mybatis-config.xml` 文件可以加载多个映射文件，每个文件对应数据库中的一张表。
3.  构造会话工厂。通过 `MyBatis` 的环境等配置信息构建会话工厂 `SqlSessionFactory`。
4.  创建会话对象。由会话工厂创建 `SqlSession` 对象，该对象中包含了执行 `SQL` 语句的所有方法。
5.  `Executor` 执行器。`MyBatis` 底层定义了一个 `Executor` 接口来操作数据库，它将根据 `SqlSession` 传递的参数动态地生成需要执行的 `SQL` 语句，同时负责查询缓存的维护。
6.  `MappedStatement` 对象。在 `Executor` 接口的执行方法中有一个 `MappedStatement` 类型的参数，该参数是对映射信息的封装，用于存储要映射的 `SQL` 语句的 id、参数等信息。
7.  输入参数映射。输入参数类型可以是 `Map`、`List` 等集合类型，也可以是基本数据类型和 `POJO` 类型。输入参数映射过程类似于 `JDBC` 对 `preparedStatement` 对象设置参数的过程。
8.  输出结果映射。输出结果类型可以是 `Map`、 `List` 等集合类型，也可以是基本据类型和 `POJO`类型。输出结果映射过程类似于 `JDBC` 对结果集的解析过程。

### 4. Mybatis 优劣

#### 1. 特点

1.    `MyBatis` 是支持定制化 `SQL`、存储过程以及高级映射的优秀的持久层框架。
2.    `MyBatis` 避免了几乎所有的 `JDBC` 代码和手动设置参数以及获取结果集。
3.    `MyBatis` 可以使用简单的 `XML` 或注解用于配置和原始映射，将接口和 `java` 的 `POJO` 映射成数据库中的记录。
4.   `MyBatis` 是一个 半自动的 `ORM`框架。因为还需要手写 `SQL` 语句。

#### 2. 与 Hibernate 比较

`Hibernate`是一个全自动的 `ORM` 框架。`Hibernate` 创建了 `Java` 对象和数据库表之间的完整映射，可以完全以面向对象的思想来操作数据库，不需要手写 `SQL` 语句。而 `MyBatis` 中还需要手写 `SQL` 语句，所以是半自动化的，工作量要大于 `Hibernate`。也正是由于自定义 `SQL` 语句，所以其灵活性、可优化性就超过了 `Hibernate`。

`Hibernate` 封装了 `SQL` 语句，由开发者对对象操作 `Hibernate` 来生成 `SQL` 语句。虽然也可以通过映射配置来控制生成的 `SQL` 语句，但是对于要生成复杂的 `SQL` 语句很难实现或者实现后导致性能的丢失。而 `MyBatis` 将写 `SQL` 语句的工作丢给开发者可以更加精确的定义 `SQL`，更加灵活也便于优化性能。完成同样功能的两条 `SQL` 语句的性能可能相差十几倍到几十倍，在高并发、快响应要求下的互联网系统中，对性能的影响更明显。

`MyBatis` 对存储过程可提供很好的支持。另外对于新手学习 `Hibernate` 时间成本比 `Mybatis` 大很多。

总之，因为 `MyBatis` 具有封装少、映射多样化、支持存储过程、可以进行 `SQL` 语句优化等特点，符合互联网高并发、大数据、高性能、高响应的要求，使它取代 `Hibernate` 成为了 `Java` 互联网中首选的持久框架。而对于对性能要求不高的比如内部管理系统、`ERP` 等可以使用 `Hibernate`。

# 2. Mybatis 入门

## 1. SQL

```mysql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for student  学生
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `age` int(11) NOT NULL COMMENT '年龄',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生' ROW_FORMAT = Dynamic;
INSERT INTO `student` VALUES (1, '暗影孤星', 18, NOW(), NOW());

SET FOREIGN_KEY_CHECKS = 1;
```

## 2. pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>aygx</groupId>
    <artifactId>mybatis</artifactId>
    <version>1.0.0</version>
    
    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.encoding>UTF-8</maven.compiler.encoding>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    </properties>

    <dependencies>

        <!--Mysql -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.49</version>
        </dependency>

        <!-- Mybatis -->
        <dependency>
            <groupId>org.mybatis</groupId>
            <artifactId>mybatis</artifactId>
            <version>3.5.5</version>
        </dependency>

        <!-- 分页插件 -->
        <dependency>
            <groupId>com.github.pagehelper</groupId>
            <artifactId>pagehelper</artifactId>
            <version>5.2.0</version>
        </dependency>

        <!-- 日志 -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>1.7.30</version>
        </dependency>

        <!-- 实体工具 -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.18</version>
        </dependency>

        <!-- Test -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.1</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>
```

## 3. StudentRepository.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!--MyBatis的DTD约束 指定mapper跟标签-->
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!--
    mapper：核心根标签
    namespace属性：名称空间，类似于包名。配合log4j.logger.aygx.mybatis.repository=debug可以输出SQL
-->
<mapper namespace="aygx.mybatis.repository.StudentMapper">
    <!--
        select：查询功能的标签
        id属性：唯一标识
        resultType属性：指定结果映射对象类型
        parameterType属性：指定参数映射对象类型
    -->
    <select id="selectAll" resultType="aygx.mybatis.entity.Student">
        SELECT * FROM student
    </select>
</mapper>
```

## 4. MybatisConfig.xml

核心配置文件主要用于配置连接数据库的环境以及 `MyBatis` 的全局配置信息

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!--MyBatis的DTD约束 指定configuration跟标签-->
<!DOCTYPE configuration PUBLIC "-//mybatis.org//DTD Config 3.0//EN" "http://mybatis.org/dtd/mybatis-3-config.dtd">

<!--configuration 核心根标签-->
<configuration>

    <!-- 配置 -->
    <settings>
        <!-- 日志实现 -->
        <setting name="logImpl" value="SLF4J"/>
        <!-- 驼峰映射 -->
        <setting name="mapUnderscoreToCamelCase" value="true"/>
    </settings>

    <!-- environments配置数据库环境。default属性指当前环境-->
    <environments default="mysql">
        <!-- environment配置具体数据库环境  id属性唯一标识-->
        <environment id="mysql">
            <!-- transactionManager事务管理。  type属性，采用JDBC默认的事务-->
            <transactionManager type="JDBC"/>
            <!-- dataSource数据源信息   type属性 连接池-->
            <dataSource type="POOLED">
                <property name="driver" value="com.mysql.jdbc.Driver"/>
                <property name="url" value="jdbc:mysql://127.0.0.1:3306/mybatis?useSSL=false"/>
                <property name="username" value="root"/>
                <property name="password" value="123456"/>
            </dataSource>
        </environment>
    </environments>

    <!-- mappers引入映射配置文件 -->
    <mappers>
        <!-- mapper 引入指定的映射配置文件   resource属性指定映射配置文件的名称 -->
        <mapper resource="StudentRepository.xml"/>
    </mappers>
</configuration>
```

## 5. log4j.properties

```properties
#定义全局日志级别
log4j.rootLogger=error,stdout,logfile
#包级别日志
log4j.logger.aygx.mybatis.repository=debug
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.Target=System.err
log4j.appender.stdout.layout=org.apache.log4j.SimpleLayout
log4j.appender.logfile=org.apache.log4j.FileAppender
log4j.appender.logfile.File=${user.dir}/mybatis.log
log4j.appender.logfile.layout=org.apache.log4j.PatternLayout
log4j.appender.logfile.layout.ConversionPattern=%d{yyyy-MM-dd HH:mm:ss} %l %F %p %m%n
```

## 6. Student

```java
package aygx.mybatis.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 暗影孤星
 * @date 2022/03/12 10:37
 * @description 学生
 */
@Data
@NoArgsConstructor
public class Student {

    /**
     * 名称
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;
}
```

## 7. test

```java
@Test
public void selectList() throws IOException {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();
    // 执行查询（namespace+id，映射实体）根据属性名称和字段一一对应
    List<Student> studentList = sqlSession.selectList("aygx.mybatis.repository.StudentMapper.selectAll", Student.class);

    System.out.println(studentList);

    inputStream.close();
    sqlSession.close();
}
```

![1623929345136](img/1623929345136.png)

# 3. API

## 1. Resources

`MyBatis` 封装的工具类 `Resources`，在 `org.apache.ibatis.io` 包中。`Resources.getResourceAsStream()` 可以从类路径下或者其他资源加载文件。

## 2. SqlSessionFactoryBuilder

`SqlSessionFactoryBuilder` 有五个 `build()` 方法，允许从不同的资源中创建 `SqlSessionFactory` 实例。 

```java
SqlSessionFactory build(InputStream inputStream)
SqlSessionFactory build(InputStream inputStream, String environment)
SqlSessionFactory build(InputStream inputStream, Properties properties)
SqlSessionFactory build(InputStream inputStream, String environment, Properties properties)
SqlSessionFactory build(Configuration config)
```

第一种方法是最常用的，它接受一个指向 `Mybatis Config XML` 文件的 `InputStream` 实例。可选的参数是 `environment` 和 `properties`。`environment` 决定加载哪种环境，包括数据源和事务管理器。比如： 

```xml
<!-- environments配置数据库环境。default属性指当前环境-->
<environments default="mysql">
    <!-- environment配置具体数据库环境  id属性唯一标识-->
    <environment id="mysql">
        <!-- transactionManager事务管理。  type属性，采用JDBC默认的事务-->
        <transactionManager type="JDBC"/>
        <!-- dataSource数据源信息   type属性 连接池-->
        <dataSource type="POOLED">
            <property name="driver" value="com.mysql.jdbc.Driver"/>
            <property name="url" value="jdbc:mysql://127.0.0.1:3306/mybatis?useSSL=false"/>
            <property name="username" value="root"/>
            <property name="password" value="123456"/>
        </dataSource>
    </environment>
</environments>
```

如果调用了带 `environment` 参数的 `build()` ，那么 `MyBatis` 将使用该环境对应的配置。如果指定了一个无效的环境会收到空指针异常的错误。如果调用了不带 `environment` 参数的 `build()` ，那么就会使用默认的环境配置。如果调用了接受 `properties` 实例的方法，那么 `MyBatis` 就会加载这些属性并在配置中提供使用。可以用 `${propName}` 形式引用这些配置值。最后一个方法接受一个 `Configuration` 实例。`Configuration` 类包含了对一个 `SqlSessionFactory` 实例可能关心的所有内容。在检查配置时，`Configuration` 类很有用，它允许查找和操纵 `SQL` 映射（但当应用开始接收请求时不推荐使用）。之前所有配置开关都存在于 `Configuration` 类，只不过它们是以 `Java API` 形式暴露的。

以下是一个简单的示例，演示如何手动配置 `Configuration` 实例，然后将它传递给 `build()` 方法来创建 `SqlSessionFactory`。

```java
@Test
public void config() throws IOException {
    TransactionFactory jdbcTransactionFactory = new JdbcTransactionFactory();
    DataSource dataSource = new UnpooledDataSource("com.mysql.jdbc.Driver", "jdbc:mysql://127.0.0.1:3306/mybatis?useSSL=false", "root", "123456");
    Configuration configuration = new Configuration(new Environment("development", jdbcTransactionFactory,dataSource ));
    configuration.setLazyLoadingEnabled(true);
    configuration.addMapper(StudentRepository.class);
    configuration.getTypeAliasRegistry().registerAlias(Student.class);

    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();
}
```

## 3. SqlSessionFactory

`SqlSessionFactory` 的最佳作用域是应用作用域。

`SqlSessionFactory` 有六个方法创建 `SqlSession` 实例。

```java
SqlSession openSession()
SqlSession openSession(boolean autoCommit)
SqlSession openSession(Connection connection)
SqlSession openSession(TransactionIsolationLevel level)
SqlSession openSession(ExecutorType execType, TransactionIsolationLevel level)
SqlSession openSession(ExecutorType execType)
SqlSession openSession(ExecutorType execType, boolean autoCommit)
SqlSession openSession(ExecutorType execType, Connection connection)
Configuration getConfiguration();
```

默认的 `openSession()` 方法没有参数，它会创建具备如下特性的 `SqlSession`：

- 事务作用域将会开启（也就是不自动提交）。
- 将由当前环境配置的 `DataSource` 实例中获取 `Connection` 对象。
- 事务隔离级别将会使用驱动或数据源的默认设置。
- 预处理语句不会被复用，也不会批量处理更新。

没有提供同时设置 `Connection` 和 `autoCommit`的方法，这是因为 `MyBatis` 会依据传入的 `Connection` 来决定是否启用 `autoCommit`。对于事务隔离级别，`MyBatis` 使用了一个 `Java` 枚举包装器来表示，称为 `TransactionIsolationLevel`，事务隔离级别支持 `JDBC` 的五个隔离级别（`NONE`、`READ_UNCOMMITTED`、`READ_COMMITTED`、`REPEATABLE_READ` 和 `SERIALIZABLE`），并且与预期的行为一致。 

`ExecutorType` 这个枚举类型定义了三个值:

- `ExecutorType.SIMPLE`：该类型的执行器没有特别的行为。它为每个语句的执行创建一个新的预处理语句。
- `ExecutorType.REUSE`：该类型的执行器会复用预处理语句。
- `ExecutorType.BATCH`：该类型的执行器会批量执行所有更新语句，如果 SELECT 在多个更新中间执行，将在必要时将多条更新语句分隔开来，以方便理解。

`getConfiguration()` 会返回一个 `Configuration` 实例，你可以在运行时使用它来检查 `MyBatis` 的配置。 

## 4. SqlSession

`SqlSessions` 是由 `SqlSessionFactory` 实例创建的。通过 `SqlSession` 接口来执行命令，获取映射器实例和管理事务。当 `Mybatis` 与一些依赖注入框架（如 `Spring` ）搭配使用时，`SqlSession` 将被依赖注入框架创建并注入，不需要使用 `SqlSessionFactoryBuilder` 或者 `SqlSessionFactory`。

每个线程都应该有它自己的 `SqlSession` 实例。`SqlSession` 的实例不是线程安全的，因此是不能被共享的，所以它的最佳的作用域是请求或方法作用域。 绝对不能将 `SqlSession` 实例的引用放在一个类的静态域，甚至一个类的实例变量也不行。 也绝不能将 `SqlSession` 实例的引用放在任何类型的托管作用域中，比如 `Servlet` 框架中的 `HttpSession`。 换句话说，每次收到 `HTTP` 请求，就可以打开一个 `SqlSession`，返回一个响应后，就关闭它。

### 1. 语句执行方法

这些方法被用来执行定义在 `SQL` 映射 XML 文件中的 `SELECT`、`INSERT`、`UPDATE` 和 `DELETE` 语句。每一方法都接受语句的 `ID` 以及参数对象，参数可以是原始类型（支持自动装箱或包装类）、`JavaBean`、`POJO` 或 `Map`。 

```java
<T> T selectOne(String statement, Object parameter)
<E> List<E> selectList(String statement, Object parameter)
<T> Cursor<T> selectCursor(String statement, Object parameter)
<K,V> Map<K,V> selectMap(String statement, Object parameter, String mapKey)
int insert(String statement, Object parameter)
int update(String statement, Object parameter)
int delete(String statement, Object parameter)
```

`selectOne` 和 `selectList` 的不同仅仅是 `selectOne` 必须返回一个对象或 `null` 值。如果返回值多于一个，就会抛出异常。`selectMap` 会将返回对象的其中一个属性作为 `key` 值，将对象作为 `value` 值，从而将多个结果集转为 `Map` 类型值。 游标（`Cursor`）与列表（`List`）返回的结果相同，不同的是，游标借助迭代器实现了数据的惰性加载。 `insert`、`update` 以及 `delete` 方法返回的值表示受该语句影响的行数。 

```java
try (Cursor<MyEntity> entities = session.selectCursor(statement, param)) {
   for (MyEntity entity:entities) {
      // 处理单个实体
   }
}
```

### 2. 事务控制方法

如果已经设置了自动提交或使用了外部事务管理器，这些方法就没什么作用了。如果正在使用由 `Connection` 实例控制的 `JDBC` 事务管理器，那么这四个方法就会派上用场： 

```java
void commit()
void commit(boolean force)
void rollback()
void rollback(boolean force)
```

默认情况下 `MyBatis` 不会自动提交事务，除非它侦测到调用了插入、更新或删除方法改变了数据库。如果没有使用这些方法提交修改，那么你可以在 `commit` 和 `rollback` 方法参数中传入 `true` 值，来保证事务被正常提交（注意，在自动提交模式或者使用了外部事务管理器的情况下，设置 `force` 值对 `session` 无效）。大部分情况下无需调用 `rollback()`，因为 `MyBatis` 会在没有调用 `commit` 时完成回滚操作。不过，当在一个可能多次提交或回滚的 `session` 中详细控制事务，回滚操作就派上用场了。 

### 3. 本地缓存

`Mybatis` 使用到了两种缓存：本地缓存（`local cache`）和二级缓存（`second level cache`）。

每当一个新 `session` 被创建，`MyBatis` 就会创建一个与之相关联的本地缓存。任何在 `session` 执行过的查询结果都会被保存在本地缓存中，所以当再次执行参数相同的相同查询时就不需要实际查询数据库了。本地缓存将会在做出修改、事务提交或回滚，以及关闭 `session` 时清空。

默认情况下，本地缓存数据的生命周期等同于整个 `session` 的周期。由于缓存会被用来解决循环引用问题和加快重复嵌套查询的速度，所以无法将其完全禁用。但是可以通过设置 `localCacheScope=STATEMENT` 来只在语句执行时使用缓存。如果 `localCacheScope` 被设置为 `SESSION`，对于某个对象，`MyBatis` 将返回在本地缓存中唯一对象的引用。对返回的对象做出的任何修改将会影响本地缓存的内容，进而将会影响到在本次 `session` 中从缓存返回的值。因此不要对 `MyBatis` 所返回的对象作出更改，以防后患。

可以随时调用以下方法来清空本地缓存：

```java
void clearCache()
```

### 4. SqlSession 被关闭

```java
void close()
```

打开的任何 `session`，都要保证它们被妥善关闭，这很重要。保证妥善关闭的最佳代码模式是这样 ：

```java
SqlSession session = sqlSessionFactory.openSession();
try (SqlSession session = sqlSessionFactory.openSession()) {
    // 假设下面三行代码是你的业务逻辑
    session.insert(...);
    session.update(...);
    session.delete(...);
    session.commit();
}
```

### 5. 映射器

```java
<T> T getMapper(Class<T> type)
```

一个映射器类就是一个仅需声明与 `SqlSession` 方法相匹配方法的接口。 

```java
public interface AuthorMapper {
  // (Author) selectOne("selectAuthor",5);
  Author selectAuthor(int id);
    
  // (List<Author>) selectList(“selectAuthors”)
  List<Author> selectAuthors();
    
  // (Map<Integer,Author>) selectMap("selectAuthors", "id")
  @MapKey("id")
  Map<Integer, Author> selectAuthors();
    
  // insert("insertAuthor", author)
  int insertAuthor(Author author);
    
  // updateAuthor("updateAuthor", author)
  int updateAuthor(Author author);
    
  // delete("deleteAuthor",5)
  int deleteAuthor(int id);
}
```

每个映射器方法签名应该匹配相关联的 `SqlSession` 方法，由方法名匹配映射语句的 `ID`。 返回类型必须匹配期望的结果类型，返回单个值时返回类型应该是返回值的类，返回多个值时，则为数组或集合类，另外也可以是游标（`Cursor`）。所有常用的类型都是支持的，包括：原始类型、`Map`、`POJO` 和 `JavaBean`。映射器接口不需要去实现任何接口或继承自任何类。只要方法签名可以被用来唯一识别对应的映射语句。映射器接口可以继承自其他接口。在使用 `XML` 来绑定映射器接口时，保证语句处于合适的命名空间中即可。唯一的限制是，不能在两个具有继承关系的接口中拥有相同的方法签名。 可以传递多个参数给一个映射器方法。在多个参数的情况下，默认它们将会以 param 加上它们在参数列表中的位置来命名，比如：#{param1}、#{param2}等。如果自定义参数的名称，那么可以在参数上使用 @Param("paramName") 注解。 

#### 映射器注解

| 使用对象 | 注解                                                         |                         XML 等价形式                         | 描述                                                         |
| :------- | :----------------------------------------------------------- | :----------------------------------------------------------: | :----------------------------------------------------------- |
| `类`     | `@CacheNamespace`                                            | ``          | 为给定的命名空间（比如类）配置缓存。属性：`implemetation`、`eviction`、`flushInterval`、`size`、`readWrite`、`blocking`、`properties`。 |                                                              |
| N/A      | `@Property`                                                  | ``          | 指定参数值或占位符（placeholder）（该占位符能被 `mybatis-config.xml` 内的配置属性替换）。属性：`name`、`value`。（仅在 MyBatis 3.4.2 以上可用） |                                                              |
| `类`     | `@CacheNamespaceRef`                                         | ``          | 引用另外一个命名空间的缓存以供使用。注意，即使共享相同的全限定类名，在 XML 映射文件中声明的缓存仍被识别为一个独立的命名空间。属性：`value`、`name`。如果你使用了这个注解，你应设置 `value` 或者 `name` 属性的其中一个。`value` 属性用于指定能够表示该命名空间的 Java 类型（命名空间名就是该 Java 类型的全限定类名），`name` 属性（这个属性仅在 MyBatis 3.4.2 以上可用）则直接指定了命名空间的名字。 |                                                              |
| `方法`   | `@ConstructorArgs`                                           | undefined``undefined | 收集一组结果以传递给一个结果对象的构造方法。属性：`value`，它是一个 `Arg` 数组。 |                                                              |
| N/A      | `@Arg`                                                       | ````         | ConstructorArgs 集合的一部分，代表一个构造方法参数。属性：`id`、`column`、`javaType`、`jdbcType`、`typeHandler`、`select`、`resultMap`。id 属性和 XML 元素 `` 相似，它是一个布尔值，表示该属性是否用于唯一标识和比较对象。从版本 3.5.4 开始，该注解变为可重复注解。 |                                                              |
| `方法`   | @TypeDiscriminator                                           | ``          | 决定使用何种结果映射的一组取值（case）。属性：`column`、`javaType`、`jdbcType`、`typeHandler`、`cases`。cases 属性是一个 `Case` 的数组。 |                                                              |
| N/A      | `@Case`                                                      | ``          | 表示某个值的一个取值以及该取值对应的映射。属性：`value`、`type`、`results`。results 属性是一个 `Results` 的数组，因此这个注解实际上和 `ResultMap` 很相似，由下面的 `Results` 注解指定。 |                                                              |
| `方法`   | `@Results`                                                   | ``          | 一组结果映射，指定了对某个特定结果列，映射到某个属性或字段的方式。属性：`value`、`id`。value 属性是一个 `Result` 注解的数组。而 id 属性则是结果映射的名称。从版本 3.5.4 开始，该注解变为可重复注解。 |                                                              |
| N/A      | `@Result`                                                    | ````         | 在列和属性或字段之间的单个结果映射。属性：`id`、`column`、`javaType`、`jdbcType`、`typeHandler`、`one`、`many`。id 属性和 XML 元素 `` 相似，它是一个布尔值，表示该属性是否用于唯一标识和比较对象。one 属性是一个关联，和 `` 类似，而 many 属性则是集合关联，和 `` 类似。这样命名是为了避免产生名称冲突。 |                                                              |
| N/A      | `@One`                                                       | ``          | 复杂类型的单个属性映射。属性： `select`，指定可加载合适类型实例的映射语句（也就是映射器方法）全限定名； `fetchType`，指定在该映射中覆盖全局配置参数 `lazyLoadingEnabled`； `resultMap`(available since 3.5.5), which is the fully qualified name of a result map that map to a single container object from select result； `columnPrefix`(available since 3.5.5), which is column prefix for grouping select columns at nested result map. **提示** 注解 API 不支持联合映射。这是由于 Java 注解不允许产生循环引用。 |                                                              |
| N/A      | `@Many`                                                      | ``          | 复杂类型的集合属性映射。属性： `select`，指定可加载合适类型实例集合的映射语句（也就是映射器方法）全限定名； `fetchType`，指定在该映射中覆盖全局配置参数 `lazyLoadingEnabled` `resultMap`(available since 3.5.5), which is the fully qualified name of a result map that map to collection object from select result； `columnPrefix`(available since 3.5.5), which is column prefix for grouping select columns at nested result map. **提示** 注解 API 不支持联合映射。这是由于 Java 注解不允许产生循环引用。 |                                                              |
| `方法`   | `@MapKey`                                                    |                                                              | 供返回值为 Map 的方法使用的注解。它使用对象的某个属性作为 key，将对象 List 转化为 Map。属性：`value`，指定作为 Map 的 key 值的对象属性名。 |
| `方法`   | `@Options`                                                   |                        映射语句的属性                        | 该注解允许你指定大部分开关和配置选项，它们通常在映射语句上作为属性出现。与在注解上提供大量的属性相比，`Options` 注解提供了一致、清晰的方式来指定选项。属性：`useCache=true`、`flushCache=FlushCachePolicy.DEFAULT`、`resultSetType=DEFAULT`、`statementType=PREPARED`、`fetchSize=-1`、`timeout=-1`、`useGeneratedKeys=false`、`keyProperty=""`、`keyColumn=""`、`resultSets=""`, `databaseId=""`。注意，Java 注解无法指定 `null` 值。因此，一旦你使用了 `Options` 注解，你的语句就会被上述属性的默认值所影响。要注意避免默认值带来的非预期行为。 The `databaseId`(Available since 3.5.5), in case there is a configured `DatabaseIdProvider`, the MyBatis use the `Options` with no `databaseId` attribute or with a `databaseId` that matches the current one. If found with and without the `databaseId` the latter will be discarded.      注意：`keyColumn` 属性只在某些数据库中有效（如 Oracle、PostgreSQL 等）。要了解更多关于 `keyColumn` 和 `keyProperty` 可选值信息，请查看“insert, update 和 delete”一节。 |
| `方法`   | `@Insert``@Update``@Delete``@Select`                         | ````````       | 每个注解分别代表将会被执行的 SQL 语句。它们用字符串数组（或单个字符串）作为参数。如果传递的是字符串数组，字符串数组会被连接成单个完整的字符串，每个字符串之间加入一个空格。这有效地避免了用 Java 代码构建 SQL 语句时产生的“丢失空格”问题。当然，你也可以提前手动连接好字符串。属性：`value`，指定用来组成单个 SQL 语句的字符串数组。 The `databaseId`(Available since 3.5.5), in case there is a configured `DatabaseIdProvider`, the MyBatis use a statement with no `databaseId` attribute or with a `databaseId` that matches the current one. If found with and without the `databaseId` the latter will be discarded. |                                                              |
| `方法`   | `@InsertProvider``@UpdateProvider``@DeleteProvider``@SelectProvider` | ````````       | 允许构建动态 SQL。这些备选的 SQL 注解允许你指定返回 SQL 语句的类和方法，以供运行时执行。（从 MyBatis 3.4.6 开始，可以使用 `CharSequence` 代替 `String` 来作为返回类型）。当执行映射语句时，MyBatis 会实例化注解指定的类，并调用注解指定的方法。你可以通过 `ProviderContext` 传递映射方法接收到的参数、"Mapper interface type" 和 "Mapper method"（仅在 MyBatis 3.4.5 以上支持）作为参数。（MyBatis 3.4 以上支持传入多个参数） 属性：`value`、`type`、`method`、`databaseId`。 `value` and `type` 属性用于指定类名 (The `type` attribute is alias for `value`, you must be specify either one. But both attributes can be omit when specify the `defaultSqlProviderType` as global configuration)。 `method` 用于指定该类的方法名（从版本 3.5.1 开始，可以省略 `method` 属性，MyBatis 将会使用 `ProviderMethodResolver` 接口解析方法的具体实现。如果解析失败，MyBatis 将会使用名为 `provideSql` 的降级实现）。**提示** 接下来的“SQL 语句构建器”一章将会讨论该话题，以帮助你以更清晰、更便于阅读的方式构建动态 SQL。 The `databaseId`(Available since 3.5.5), in case there is a configured `DatabaseIdProvider`, the MyBatis will use a provider method with no `databaseId` attribute or with a `databaseId` that matches the current one. If found with and without the `databaseId` the latter will be discarded. |                                                              |
| `参数`   | `@Param`                                                     |                             N/A                              | 如果你的映射方法接受多个参数，就可以使用这个注解自定义每个参数的名字。否则在默认情况下，除 `RowBounds` 以外的参数会以 "param" 加参数位置被命名。例如 `#{param1}`, `#{param2}`。如果使用了 `@Param("person")`，参数就会被命名为 `#{person}`。 |
| `方法`   | `@SelectKey`                                                 | ``          | 这个注解的功能与 `` 标签完全一致。该注解只能在 `@Insert` 或 `@InsertProvider` 或 `@Update` 或 `@UpdateProvider` 标注的方法上使用，否则将会被忽略。如果标注了 `@SelectKey` 注解，MyBatis 将会忽略掉由 `@Options` 注解所设置的生成主键或设置（configuration）属性。属性：`statement` 以字符串数组形式指定将会被执行的 SQL 语句，`keyProperty` 指定作为参数传入的对象对应属性的名称，该属性将会更新成新的值，`before` 可以指定为 `true` 或 `false` 以指明 SQL 语句应被在插入语句的之前还是之后执行。`resultType` 则指定 `keyProperty` 的 Java 类型。`statementType` 则用于选择语句类型，可以选择 `STATEMENT`、`PREPARED` 或 `CALLABLE` 之一，它们分别对应于 `Statement`、`PreparedStatement` 和 `CallableStatement`。默认值是 `PREPARED`。 The `databaseId`(Available since 3.5.5), in case there is a configured `DatabaseIdProvider`, the MyBatis will use a statement with no `databaseId` attribute or with a `databaseId` that matches the current one. If found with and without the `databaseId` the latter will be discarded. |                                                              |
| `方法`   | `@ResultMap`                                                 |                             N/A                              | 这个注解为 `@Select` 或者 `@SelectProvider` 注解指定 XML 映射中 `` 元素的 id。这使得注解的 select 可以复用已在 XML 中定义的 ResultMap。如果标注的 select 注解中存在 `@Results` 或者 `@ConstructorArgs` 注解，这两个注解将被此注解覆盖。 |
| `方法`   | `@ResultType`                                                |                             N/A                              | 在使用了结果处理器的情况下，需要使用此注解。由于此时的返回类型为 void，所以 Mybatis 需要有一种方法来判断每一行返回的对象类型。如果在 XML 有对应的结果映射，请使用 `@ResultMap` 注解。如果结果类型在 XML 的 `` 元素中指定了，就不需要使用其它注解了。否则就需要使用此注解。比如，如果一个标注了 @Select 的方法想要使用结果处理器，那么它的返回类型必须是 void，并且必须使用这个注解（或者 @ResultMap）。这个注解仅在方法返回类型是 void 的情况下生效。 |
| 方法     | `@Flush`                                                     |                             N/A                              | 如果使用了这个注解，定义在 Mapper 接口中的方法就能够调用 `SqlSession#flushStatements()` 方法。（Mybatis 3.3 以上可用） |

# 4. 配置文件

核心配置文件需要加入以下约束

```xml
<!--MyBatis的DTD约束-->
<!DOCTYPE configuration PUBLIC "-//mybatis.org//DTD Config 3.0//EN" "http://mybatis.org/dtd/mybatis-3-config.dtd">
```

`DTD` 约束表明配置文件中组要一对 `<configuration>` 标签作为父标签，根据 `<configuration>` 的源码得知其他子标签的顺序

```xml
<!ELEMENT configuration (properties?, settings?, typeAliases?, typeHandlers?, objectFactory?, objectWrapperFactory?, reflectorFactory?, plugins?, environments?, databaseIdProvider?, mappers?)>
```

 `DTD` 说明：

1、`DTD` 声明始终以 `!DOCTYPE` 开头，空一格后跟着文档根元素的名称。

2、根元素名：`configuration`。所以每一个标签库定义文件都是以 `taglib` 为根元素的，否则就不会验证通过。

3、`PUBLIC "-//mybatis.org//DTD Config 3.0//EN`，这是一个公共 `DTD` 的名称（私有的使用 `SYSTEM` 表示）。它是以 `-`开头的表示这个 `DTD` 不是一个标准组织制定的。（如果是 `ISO` 标准化组织批准的，以 `ISO` 开头）。接着是双斜杠 `//`，然后是 `DTD` 所有者的名字，接着是双斜杠 `//`，然后是 `DTD` 描述的文档类型，再然后是 `//` 和 `ISO 639` 语言标识符。

4、`http://mybatis.org/dtd/mybatis-3-config.dtd` 表示这个 `DTD` 的位置。`xml` 分析器首先会以某种机制查找公共 `DTD` 的名称，查到了则以此为标准，如果查不到再到 `DTD` 位置上去找。

## 1. 属性（properties）

这些属性可以在外部进行配置，并可以进行动态替换。既可以在典型的 `Java` 属性文件中配置这些属性，也可以在 `properties` 元素的子元素中设置。例如 

```xml
<!-- 属性 -->
<properties resource="jdbc.properties">
    <property name="username" value="root"/>
    <property name="password" value="654321"/>
</properties>
```

```properties
# jdbc.properties
username=root
password=123456
url=jdbc:mysql://127.0.0.1:3306/mybatis?useSSL=false
driver=com.mysql.jdbc.Driver
```

设置好的属性可以在整个配置文件中用来替换需要动态配置的属性值。比如: 

```xml
<dataSource type="POOLED">
    <property name="driver" value="com.mysql.jdbc.Driver"/>
    <property name="url" value="${url}"/>
    <property name="username" value="${username}"/>
    <property name="password" value="${password}"/>
</dataSource>
```

`username` 和 `password` 、以及 `url` 属性将会由 `jdbc.properties` 文件中对应的值来替换。也可以在 `SqlSessionFactoryBuilder.build()` 方法中传入属性值。例如： 

```java
SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(reader, props);

// ... 或者 ...

SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(reader, environment, props);
```

如果一个属性在不只一个地方进行了配置，那么，`MyBatis` 将按照下面的顺序来加载：

- 首先读取在 `properties` 元素体内指定的属性。
- 然后根据 `properties` 元素中的 `resource` 属性读取类路径下属性文件，或根据 `url` 属性指定的路径读取属性文件，并覆盖之前读取过的同名属性。
- 最后读取作为方法参数传递的属性，并覆盖之前读取过的同名属性。

因此，通过方法参数传递的属性具有最高优先级，`resource/url` 属性中指定的配置文件次之，最低优先级的则是 `properties` 元素中指定的属性。

从 MyBatis 3.4.2 开始可以为占位符指定一个默认值。例如： 

```xml
<dataSource type="POOLED">
  <!-- ... -->
  <!-- 如果属性 'username' 没有被配置，'username' 属性的值将为 'root' -->
  <property name="username" value="${username:root}"/> 
</dataSource>
```

这个特性默认是关闭的。要启用这个特性，需要添加一个特定的属性来开启这个特性。例如： 

```xml
<properties resource="org/mybatis/example/config.properties">
  <!-- ... -->
  <!-- 启用默认值特性 -->
  <property name="org.apache.ibatis.parsing.PropertyParser.enable-default-value" value="true"/> 
</properties>
```

如果在属性名中使用了 `":"` 字符（如：`db:username`），或者在 SQL 映射中使用了 OGNL 表达式的三元运算符（如： `${tableName != null ? tableName : 'global_constants'}`），就需要设置特定的属性来修改分隔属性名和默认值的字符。例如： 

```xml
<properties resource="org/mybatis/example/config.properties">
  <!-- ... -->
  <!-- 修改默认值的分隔符 -->
  <property name="org.apache.ibatis.parsing.PropertyParser.default-value-separator" value="?:"/> 
</properties>
<!-- ... -->
<dataSource type="POOLED">
  <!-- ... -->
  <property name="username" value="${db:username?:root}"/>
</dataSource>
```

## 2. 设置（settings）

| 设置名                    | 描述                                                         | 有效值                                                       | 默认值                                       |
| ------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | -------------------------------------------- |
| cacheEnabled              | 全局性地开启或关闭所有映射器配置文件中已配置的任何缓存。     | true \|false                                                 | true                                         |
| lazyLoadingEnabled        | 延迟加载的全局开关。当开启时，所有关联对象都会延迟加载。 特定关联关系中可通过设置 `fetchType` 属性来覆盖该项的开关状态。 | true \|false                                                 | false                                        |
| mapUnderscoreToCamelCase  | 是否开启驼峰命名自动映射                                     | true \|false                                                 | false                                        |
| useGeneratedKeys          | 允许 `JDBC` 支持自动生成主键，需要数据库驱动支持。如果设置为 `true`，将强制使用自动生成主键。尽管一些数据库驱动不支持此特性，但仍可正常工作。 | true \|false                                                 | false                                        |
| defaultStatementTimeout   | 设置超时时间，它决定数据库驱动等待数据库响应的秒数。         | 任意正整数                                                   | null                                         |
| logPrefix                 | 指定 MyBatis 增加到日志名称的前缀。                          | 任何字符串                                                   | 未设置                                       |
| logImpl                   | 指定 MyBatis 所用日志的具体实现，未指定时将自动查找。        | SLF4J \| LOG4J(deprecated since 3.5.9) \| LOG4J2 \| JDK_LOGGING \| COMMONS_LOGGING \| STDOUT_LOGGING \| NO_LOGGING | 未设置                                       |
| localCacheScope           | MyBatis 利用本地缓存机制（Local Cache）防止循环引用和加速重复的嵌套查询。 默认值为 SESSION，会缓存一个会话中执行的所有查询。 若设置值为 STATEMENT，本地缓存将仅用于执行语句，对相同 SqlSession 的不同查询将不会进行缓存。 | SESSION \|STATEMENT                                          | SESSION                                      |
| aggressiveLazyLoading     | 开启时，任一方法的调用都会加载该对象的所有延迟加载属性。 否则，每个延迟加载属性会按需加载（参考 `lazyLoadTriggerMethods`)。 | true \|false                                                 | false （在 3.4.1 及之前的版本中默认为 true） |
| lazyLoadTriggerMethods    | 指定对象的哪些方法触发一次延迟加载                           | 用逗号分隔的方法列表。                                       | equals,clone,hashCode,toString               |
| proxyFactory              | 指定 Mybatis 创建可延迟加载对象所用到的代理工具。            | CGLIB \| JAVASSIST                                           | JAVASSIST （MyBatis 3.3 以上）               |
| multipleResultSetsEnabled | 是否允许单个语句返回多结果集（需要数据库驱动支持）。         | true \|false                                                 | true                                         |
| useColumnLabel            | 使用列标签代替列名。实际表现依赖于数据库驱动，具体可参考数据库驱动的相关文档，或通过对比测试来观察。 | true \|false                                                 | true                                         |
| autoMappingBehavior       | 指定 MyBatis 应如何自动映射列到字段或属性。 NONE 表示关闭自动映射；PARTIAL 只会自动映射没有定义嵌套结果映射的字段。 FULL 会自动映射任何复杂的结果集（无论是否嵌套）。 | NONE, PARTIAL, FULL                                          | PARTIAL                                      |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |
|                           |                                                              |                                                              |                                              |

一个配置完整的 `settings` 元素的示例如下： 

```xml
<settings>
  <setting name="cacheEnabled" value="true"/>
  <setting name="lazyLoadingEnabled" value="true"/>
  <setting name="multipleResultSetsEnabled" value="true"/>
  <setting name="useColumnLabel" value="true"/>
  <setting name="useGeneratedKeys" value="false"/>
  <setting name="autoMappingBehavior" value="PARTIAL"/>
  <setting name="autoMappingUnknownColumnBehavior" value="WARNING"/>
  <setting name="defaultExecutorType" value="SIMPLE"/>
  <setting name="defaultStatementTimeout" value="25"/>
  <setting name="defaultFetchSize" value="100"/>
  <setting name="safeRowBoundsEnabled" value="false"/>
  <setting name="mapUnderscoreToCamelCase" value="false"/>
  <setting name="localCacheScope" value="SESSION"/>
  <setting name="jdbcTypeForNull" value="OTHER"/>
  <setting name="lazyLoadTriggerMethods" value="equals,clone,hashCode,toString"/>
</settings>
```

## 3. 类型别名（typeAliases）

类型别名可为 `Java` 类型设置一个缩写名字。 它仅用于 `XML` 配置，意在降低冗余的全限定类名书写。例如： 

```xml
<typeAliases>
  <typeAlias alias="Author" type="domain.blog.Author"/>
  <typeAlias alias="Blog" type="domain.blog.Blog"/>
  <typeAlias alias="Comment" type="domain.blog.Comment"/>
  <typeAlias alias="Post" type="domain.blog.Post"/>
  <typeAlias alias="Section" type="domain.blog.Section"/>
  <typeAlias alias="Tag" type="domain.blog.Tag"/>
</typeAliases>
```

当这样配置时，`Blog` 可以用在任何使用 `domain.blog.Blog` 的地方。也可以指定一个包名，MyBatis 会在包名下面搜索需要的 Java Bean，比如：

```xml
<typeAliases>
  <package name="domain.blog"/>
</typeAliases>
```

每一个在包 `domain.blog` 中的 Java Bean，在没有注解的情况下，会使用 Bean 的首字母小写的非限定类名来作为它的别名。 比如 `domain.blog.Author` 的别名为 `author`；若有注解，则别名为其注解值。见下面的例子： 

```java
@Alias("author")
public class Author {
    ...
}
```

下面是一些为常见的 Java 类型内建的类型别名。它们都是不区分大小写的，注意，为了应对原始类型的命名重复，采取了特殊的命名风格。 

| 别名       | 映射的类型 |
| :--------- | :--------- |
| _byte      | byte       |
| _long      | long       |
| _short     | short      |
| _int       | int        |
| _integer   | int        |
| _double    | double     |
| _float     | float      |
| _boolean   | boolean    |
| string     | String     |
| byte       | Byte       |
| long       | Long       |
| short      | Short      |
| int        | Integer    |
| integer    | Integer    |
| double     | Double     |
| float      | Float      |
| boolean    | Boolean    |
| date       | Date       |
| decimal    | BigDecimal |
| bigdecimal | BigDecimal |
| object     | Object     |
| map        | Map        |
| hashmap    | HashMap    |
| list       | List       |
| arraylist  | ArrayList  |
| collection | Collection |
| iterator   | Iterator   |

## 4. 类型处理器（typeHandlers）

`MyBatis` 在设置预处理语句（`PreparedStatement`）中的参数或从结果集中取出一个值时， 都会用类型处理器将获取到的值以合适的方式转换成 `Java` 类型。下表描述了一些默认的类型处理器。从 3.4.5 开始，MyBatis 默认支持 `JSR-310`（日期和时间 API） 

| 类型处理器                   | Java 类型                       | JDBC 类型                                                    |
| :--------------------------- | :------------------------------ | :----------------------------------------------------------- |
| `BooleanTypeHandler`         | `java.lang.Boolean`, `boolean`  | 数据库兼容的 `BOOLEAN`                                       |
| ByteTypeHandler              | `java.lang.Byte`, `byte`        | 数据库兼容的 `NUMERIC` 或 `BYTE`                             |
| `ShortTypeHandler`           | `java.lang.Short`, `short`      | 数据库兼容的 `NUMERIC` 或 `SMALLINT`                         |
| `IntegerTypeHandler`         | `java.lang.Integer`, `int`      | 数据库兼容的 `NUMERIC` 或 `INTEGER`                          |
| `LongTypeHandler`            | `java.lang.Long`, `long`        | 数据库兼容的 `NUMERIC` 或 `BIGINT`                           |
| `FloatTypeHandler`           | `java.lang.Float`, `float`      | 数据库兼容的 `NUMERIC` 或 `FLOAT`                            |
| `DoubleTypeHandler`          | `java.lang.Double`, `double`    | 数据库兼容的 `NUMERIC` 或 `DOUBLE`                           |
| `BigDecimalTypeHandler`      | `java.math.BigDecimal`          | 数据库兼容的 `NUMERIC` 或 `DECIMAL`                          |
| `StringTypeHandler`          | `java.lang.String`              | `CHAR`, `VARCHAR`                                            |
| `ClobReaderTypeHandler`      | `java.io.Reader`                | -                                                            |
| `ClobTypeHandler`            | `java.lang.String`              | `CLOB`, `LONGVARCHAR`                                        |
| `NStringTypeHandler`         | `java.lang.String`              | `NVARCHAR`, `NCHAR`                                          |
| `NClobTypeHandler`           | `java.lang.String`              | `NCLOB`                                                      |
| `BlobInputStreamTypeHandler` | `java.io.InputStream`           | -                                                            |
| `ByteArrayTypeHandler`       | `byte[]`                        | 数据库兼容的字节流类型                                       |
| `BlobTypeHandler`            | `byte[]`                        | `BLOB`, `LONGVARBINARY`                                      |
| `DateTypeHandler`            | `java.util.Date`                | `TIMESTAMP`                                                  |
| `DateOnlyTypeHandler`        | `java.util.Date`                | `DATE`                                                       |
| `TimeOnlyTypeHandler`        | `java.util.Date`                | `TIME`                                                       |
| `SqlTimestampTypeHandler`    | `java.sql.Timestamp`            | `TIMESTAMP`                                                  |
| `SqlDateTypeHandler`         | `java.sql.Date`                 | `DATE`                                                       |
| `SqlTimeTypeHandler`         | `java.sql.Time`                 | `TIME`                                                       |
| `ObjectTypeHandler`          | `Any`                           | `OTHER` 或未指定类型                                         |
| `EnumTypeHandler`            | `Enumeration Type`              | `VARCHAR` 或任何兼容的字符串类型，用来存储枚举的名称（而不是索引序数值） |
| `EnumOrdinalTypeHandler`     | `Enumeration Type`              | 任何兼容的 `NUMERIC` 或 `DOUBLE` 类型，用来存储枚举的序数值（而不是名称）。 |
| `SqlxmlTypeHandler`          | `java.lang.String`              | `SQLXML`                                                     |
| `InstantTypeHandler`         | `java.time.Instant`             | `TIMESTAMP`                                                  |
| `LocalDateTimeTypeHandler`   | `java.time.LocalDateTime`       | `TIMESTAMP`                                                  |
| `LocalDateTypeHandler`       | `java.time.LocalDate`           | `DATE`                                                       |
| `LocalTimeTypeHandler`       | `java.time.LocalTime`           | `TIME`                                                       |
| `OffsetDateTimeTypeHandler`  | `java.time.OffsetDateTime`      | `TIMESTAMP`                                                  |
| `OffsetTimeTypeHandler`      | `java.time.OffsetTime`          | `TIME`                                                       |
| `ZonedDateTimeTypeHandler`   | `java.time.ZonedDateTime`       | `TIMESTAMP`                                                  |
| `YearTypeHandler`            | `java.time.Year`                | `INTEGER`                                                    |
| `MonthTypeHandler`           | `java.time.Month`               | `INTEGER`                                                    |
| `YearMonthTypeHandler`       | `java.time.YearMonth`           | `VARCHAR` 或 `LONGVARCHAR`                                   |
| `JapaneseDateTypeHandler`    | `java.time.chrono.JapaneseDate` | `DATE`                                                       |

可以重写已有的类型处理器或创建自己的类型处理器来处理不支持的或非标准的类型。 具体做法为：实现 `org.apache.ibatis.type.TypeHandler` 接口， 或继承一个很便利的类 `org.apache.ibatis.type.BaseTypeHandler`， 并且可以（可选地）将它映射到一个 JDBC 类型。比如：

```java
@MappedJdbcTypes(JdbcType.VARCHAR)
public class ExampleTypeHandler extends BaseTypeHandler<String> {

  @Override
  public void setNonNullParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType) throws SQLException {
    ps.setString(i, parameter);
  }

  @Override
  public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
    return rs.getString(columnName);
  }

  @Override
  public String getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
    return rs.getString(columnIndex);
  }

  @Override
  public String getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
    return cs.getString(columnIndex);
  }
}
```

```xml
<!-- mybatis-config.xml -->
<typeHandlers>
  <typeHandler handler="org.mybatis.example.ExampleTypeHandler"/>
</typeHandlers>
```

使用上述的类型处理器将会覆盖已有的处理 `Java String` 类型的属性以及 `VARCHAR` 类型的参数和结果的类型处理器。 要注意 `MyBatis` 不会通过检测数据库元信息来决定使用哪种类型，所以必须在参数和结果映射中指明字段是 `VARCHAR` 类型，以使其能够绑定到正确的类型处理器上。这是因为 `MyBatis` 直到语句被执行时才清楚数据类型。

## 5. 插件（plugins）

`MyBatis` 允许在映射语句执行过程中的某一点进行拦截调用。默认情况下，`MyBatis` 允许使用插件来拦截的方法调用包括： 

- Executor (update, query, flushStatements, commit, rollback, getTransaction, close, isClosed)
- ParameterHandler (getParameterObject, setParameters)
- ResultSetHandler (handleResultSets, handleOutputParameters)
- StatementHandler (prepare, parameterize, batch, update, query)

通过 `MyBatis` 提供的强大机制，使用插件是非常简单的，只需实现 `Interceptor` 接口，并指定想要拦截的方法签名即可。 

```java
@Intercepts({@Signature(
  type= Executor.class,
  method = "update",
  args = {MappedStatement.class,Object.class})})
public class ExamplePlugin implements Interceptor {
  private Properties properties = new Properties();
  public Object intercept(Invocation invocation) throws Throwable {
    // 根据需要进行预处理
    Object returnObject = invocation.proceed();
    // 根据需要进行后处理
    return returnObject;
  }
  public void setProperties(Properties properties) {
    this.properties = properties;
  }
}
```

```xml
<!-- mybatis-config.xml -->
<plugins>
  <plugin interceptor="org.mybatis.example.ExamplePlugin">
    <property name="someProperty" value="100"/>
  </plugin>
</plugins>
```

上面的插件将会拦截在 `Executor` 实例中所有的 `update` 方法调用， 这里的 `Executor` 是负责执行底层映射语句的内部对象。 

## 6. 环境配置（environments）

`MyBatis` 可以配置成适应多种环境，这种机制有助于将 `SQL` 映射应用于多种数据库之中。**尽管可以配置多个环境，但每个 SqlSessionFactory 实例只能选择一种环境。**如果你想连接两个数据库，就需要创建两个 `SqlSessionFactory` 实例，每个数据库对应一个。

`environments` 元素定义了如何配置环境。

```xml
<environments default="development">
  <environment id="development">
    <transactionManager type="JDBC">
      <property name="..." value="..."/>
    </transactionManager>
    <dataSource type="POOLED">
      <property name="driver" value="${driver}"/>
      <property name="url" value="${url}"/>
      <property name="username" value="${username}"/>
      <property name="password" value="${password}"/>
    </dataSource>
  </environment>
</environments>
```

 注意一些关键点:

- 默认使用的环境 ID（比如：`default="development"`）。
- 每个 `environment` 元素定义的环境 ID（比如：`id="development"`）。
- 事务管理器的配置（比如：`type="JDBC"`）。
- 数据源的配置（比如：`type="POOLED"`）。

### 1.  事务管理器（transactionManager）

 在 `MyBatis` 中有两种类型的事务管理器（也就是 `type="[JDBC|MANAGED]"`）： 

- `JDBC`：这个配置直接使用了 `JDBC` 的提交和回滚设施，它依赖从数据源获得的连接来管理事务作用域。
- `MANAGED`：这个配置几乎没做什么。它从不提交或回滚一个连接，而是让容器来管理事务的整个生命周期（比如 `JEE` 应用服务器的上下文）。 默认情况下它会关闭连接。然而一些容器并不希望连接被关闭，因此需要将 closeConnection 属性设置为 false 来阻止默认的关闭行为。例如:

```xml
<transactionManager type="MANAGED">
  <property name="closeConnection" value="false"/>
</transactionManager>
```

如果使用 `Spring + MyBatis`则没有必要配置事务管理器，因为 `Spring` 模块会使用自带的管理器来覆盖前面的配置。这两种事务管理器类型都不需要设置任何属性。它们其实是类型别名，换句话说，可以用 `TransactionFactory` 接口实现类的全限定名或类型别名代替它们。

```java
public interface TransactionFactory {
  default void setProperties(Properties props) { // 从 3.5.2 开始，该方法为默认方法
    // 空实现
  }
  Transaction newTransaction(Connection conn);
  Transaction newTransaction(DataSource dataSource, TransactionIsolationLevel level, boolean autoCommit);
}
```

在事务管理器实例化后所有在 `XML` 中配置的属性将会被传递给 `setProperties()` 方法。你的实现还需要创建一个 `Transaction`接口的实现类，这个接口也很简单： 

```java
public interface Transaction {
  Connection getConnection() throws SQLException;
  void commit() throws SQLException;
  void rollback() throws SQLException;
  void close() throws SQLException;
  Integer getTimeout() throws SQLException;
}
```

使用这两个接口，可以完全自定义 `MyBatis` 对事务的处理。 

### 2.  **数据源（dataSource）** 

`dataSource` 元素使用标准的 `JDBC` 数据源接口来配置 `JDBC` 连接对象的资源。  有三种内建的数据源类型（也就是 `type="[UNPOOLED|POOLED|JNDI]"`）： 

**`UNPOOLED`**：这个数据源的实现会每次请求时打开和关闭连接。性能表现则依赖于使用的数据库，对某些数据库来说，使用连接池并不重要，这个配置就很适合这种情形。`UNPOOLED` 类型的数据源仅仅需要配置以下 5 种属性：

- `driver` – 这是 `JDBC` 驱动的 `Java` 类全限定名（并不是 `JDBC` 驱动中可能包含的数据源类）。
- `url` – 这是数据库的 `JDBC URL` 地址。
- `username` – 登录数据库的用户名。
- `password` – 登录数据库的密码。
- `defaultTransactionIsolationLevel` – 默认的连接事务隔离级别。
- `defaultNetworkTimeout` – 等待数据库操作完成的默认网络超时时间（单位：毫秒）。查看 `java.sql.Connection#setNetworkTimeout()` 的 API 文档以获取更多信息。

作为可选项，你也可以传递属性给数据库驱动。只需在属性名加上 `driver.` 前缀即可，例如：`driver.encoding=UTF8` ，这将通过 `DriverManager.getConnection(url, driverProperties)` 方法传递值为 `UTF8` 的 `encoding` 属性给数据库驱动。

**POOLED**：这种数据源的实现利用“池”的概念将 `JDBC` 连接对象组织起来，避免了创建新的连接实例时所必需的初始化和认证时间。 这种处理方式很流行，能使并发 `Web` 应用快速响应请求。

除了上述提到 `UNPOOLED` 下的属性外，还有更多属性用来配置 `POOLED` 的数据源：

- `poolMaximumActiveConnections` – 在任意时间可存在的活动（正在使用）连接数量，默认值：10
- `poolMaximumIdleConnections` – 任意时间可能存在的空闲连接数。
- `poolMaximumCheckoutTime` – 在被强制返回之前，池中连接被检出（checked out）时间，默认值：20000 毫秒（即 20 秒）
- `poolTimeToWait` – 这是一个底层设置，如果获取连接花费了相当长的时间，连接池会打印状态日志并重新尝试获取一个连接（避免在误配置的情况下一直失败且不打印日志），默认值：20000 毫秒（即 20 秒）。
- `poolMaximumLocalBadConnectionTolerance` – 这是一个关于坏连接容忍度的底层设置， 作用于每一个尝试从缓存池获取连接的线程。 如果这个线程获取到的是一个坏的连接，那么这个数据源允许这个线程尝试重新获取一个新的连接，但是这个重新尝试的次数不应该超过 `poolMaximumIdleConnections` 与 `poolMaximumLocalBadConnectionTolerance` 之和。 默认值：3（新增于 3.4.5）
- `poolPingQuery` – 发送到数据库的侦测查询，用来检验连接是否正常工作并准备接受请求。默认是 `NO PING QUERY SET`，这会导致多数数据库驱动出错时返回恰当的错误消息。
- `poolPingEnabled` – 是否启用侦测查询。若开启，需要设置 `poolPingQuery` 属性为一个可执行的 SQL 语句（最好是一个速度非常快的 SQL 语句），默认值：false。
- `poolPingConnectionsNotUsedFor` – 配置 poolPingQuery 的频率。可以被设置为和数据库连接超时时间一样，来避免不必要的侦测，默认值：0（即所有连接每一时刻都被侦测 — 当然仅当 poolPingEnabled 为 true 时适用）。

**JNDI** – 这个数据源实现是为了能在如 EJB 或应用服务器这类容器中使用，容器可以集中或在外部配置数据源，然后放置一个 JNDI 上下文的数据源引用。这种数据源配置只需要两个属性：

- `initial_context` – 这个属性用来在 InitialContext 中寻找上下文（即，initialContext.lookup(initial_context)）。这是个可选属性，如果忽略，那么将会直接从 InitialContext 中寻找 data_source 属性。
- `data_source` – 这是引用数据源实例位置的上下文路径。提供了 initial_context 配置时会在其返回的上下文中进行查找，没有提供时则直接在 InitialContext 中查找。

和其他数据源配置类似，可以通过添加前缀“env.”直接把属性传递给 InitialContext。比如：env.encoding=UTF8，这就会在 InitialContext 实例化时往它的构造方法传递值为 `UTF8` 的 `encoding` 属性。

可以通过实现接口 `org.apache.ibatis.datasource.DataSourceFactory` 来使用第三方数据源实现：

```java
public interface DataSourceFactory {
  void setProperties(Properties props);
  DataSource getDataSource();
}
```

 `org.apache.ibatis.datasource.unpooled.UnpooledDataSourceFactory` 可被用作父类来构建新的数据源适配器，比如下面这段插入 C3P0 数据源所必需的代码： 

```java
import org.apache.ibatis.datasource.unpooled.UnpooledDataSourceFactory;
import com.mchange.v2.c3p0.ComboPooledDataSource;

public class C3P0DataSourceFactory extends UnpooledDataSourceFactory {

  public C3P0DataSourceFactory() {
    this.dataSource = new ComboPooledDataSource();
  }
}
```

为了令其工作，记得在配置文件中为每个希望 MyBatis 调用的 setter 方法增加对应的属性。 下面是一个可以连接至 PostgreSQL 数据库的例子： 

```xml
<dataSource type="org.myproject.C3P0DataSourceFactory">
  <property name="driver" value="org.postgresql.Driver"/>
  <property name="url" value="jdbc:postgresql:mydb"/>
  <property name="username" value="postgres"/>
  <property name="password" value="root"/>
</dataSource>
```

## 7. 数据库厂商标识（databaseIdProvider）

MyBatis 可以根据不同的数据库厂商执行不同的语句，这种多厂商的支持是基于映射语句中的 `databaseId` 属性。 MyBatis 会加载带有匹配当前数据库 `databaseId` 属性和所有不带 `databaseId` 属性的语句。 如果同时找到带有 `databaseId` 和不带 `databaseId` 的相同语句，则后者会被舍弃。 为支持多厂商特性，只要像下面这样在 mybatis-config.xml 文件中加入 `databaseIdProvider` 即可： 

```xml
<databaseIdProvider type="DB_VENDOR" />
```

databaseIdProvider 对应的 DB_VENDOR 实现会将 databaseId 设置为 `DatabaseMetaData#getDatabaseProductName()` 返回的字符串。 由于通常情况下这些字符串都非常长，而且相同产品的不同版本会返回不同的值，你可能想通过设置属性别名来使其变短： 

```xml
<databaseIdProvider type="DB_VENDOR">
  <property name="SQL Server" value="sqlserver"/>
  <property name="DB2" value="db2"/>
  <property name="Oracle" value="oracle" />
</databaseIdProvider>
```

在提供了属性别名时，databaseIdProvider 的 DB_VENDOR 实现会将 databaseId 设置为数据库产品名与属性中的名称第一个相匹配的值，如果没有匹配的属性，将会设置为 “null”。 在这个例子中，如果 `getDatabaseProductName()` 返回“Oracle (DataDirect)”，databaseId 将被设置为“oracle”。

你可以通过实现接口 `org.apache.ibatis.mapping.DatabaseIdProvider` 并在 mybatis-config.xml 中注册来构建自己的 DatabaseIdProvider：

```java
public interface DatabaseIdProvider {
  default void setProperties(Properties p) { // 从 3.5.2 开始，该方法为默认方法
    // 空实现
  }
  String getDatabaseId(DataSource dataSource) throws SQLException;
}
```

## 8. 映射器（mappers）

使用相对于类路径的资源引用，或完全限定资源定位符（包括 `file:///` 形式的 URL），或类名和包名等直接告诉 MyBatis 到哪里去找映射文件,例如： 

```xml
<!-- 使用相对于类路径的资源引用 -->
<mappers>
  <mapper resource="org/mybatis/builder/AuthorMapper.xml"/>
  <mapper resource="org/mybatis/builder/BlogMapper.xml"/>
  <mapper resource="org/mybatis/builder/PostMapper.xml"/>
</mappers>
```

```xml
<!-- 使用完全限定资源定位符（URL） -->
<mappers>
  <mapper url="file:///var/mappers/AuthorMapper.xml"/>
  <mapper url="file:///var/mappers/BlogMapper.xml"/>
  <mapper url="file:///var/mappers/PostMapper.xml"/>
</mappers>
```

```xml
<!-- 使用映射器接口实现类的完全限定类名 -->
<mappers>
  <mapper class="org.mybatis.builder.AuthorMapper"/>
  <mapper class="org.mybatis.builder.BlogMapper"/>
  <mapper class="org.mybatis.builder.PostMapper"/>
</mappers>
```

```xml
<!-- 将包内的映射器接口实现全部注册为映射器 -->
<mappers>
  <package name="org.mybatis.builder"/>
</mappers>
```

# 5. 映射文件

映射配置文件需要加入以下配置文件

```xml
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
```

`DTD` 约束表明配置文件中组要一对 `<mapper>` 标签作为父标签，根据 `<mapper>` 的源码得知其他子标签的顺序。

```xml
<!ELEMENT mapper (cache-ref | cache | resultMap* | parameterMap* | sql* | insert* | update* | delete* | select* )+>
```

`SQL` 映射文件只有很少的几个顶级元素（按照应被定义的顺序列出）：

| 元素        | 备注                                                       |
| ----------- | ---------------------------------------------------------- |
| `cache`     | 该命名空间的缓存配置                                       |
| `cache-ref` | 引用其它命名空间的缓存配置                                 |
| `resultMap` | 描述如何从数据库结果集中加载对象，是最复杂也是最强大的元素 |
| `sql`       | 可被其它语句引用的可重用语句块                             |
| `insert`    | 映射插入语句                                               |
| `update`    | 映射更新语句                                               |
| `delete`    | 映射删除语句                                               |
| `select`    | 映射查询语句                                               |

## 1. select

-  参数是基本数据类型，使用 `#{param1}` 来接收数据（其实可以是任意名称）。
-  参数是引用数据类型，使用 `#(属性名)` 来接收数据，底层调用的是其 `getter` 方法。如果没有 `getter` 方法，就会直接找同名属性。

| 属性            | 描述                                                         |
| :-------------- | :----------------------------------------------------------- |
| `id`            | 在命名空间中唯一的标识符，可以被用来引用这条语句。           |
| `parameterType` | 将会传入这条语句的参数的类全限定名或别名。这个属性是可选的，因为 `MyBatis` 可以通过类型处理器（`TypeHandler`）推断出具体传入语句的参数，默认值为未设置（`unset`）。 |
| `resultType`    | 期望从这条语句中返回结果的类全限定名或别名。如果返回的是集合，那应该设置为集合包含的类型，而不是集合本身的类型。 `resultType` 和 `resultMap` 之间只能同时使用一个。 |
| `resultMap`     | 对外部 `resultMap` 的命名引用。`resultType` 和 `resultMap` 之间只能同时使用一个。 |
| `flushCache`    | 将其设置为 `true` 后，只要语句被调用都会导致本地缓存和二级缓存被清空。默认值：`false`。 |
| `useCache`      | 将其设置为 `true` 后，将会导致本条语句的结果被二级缓存缓存起来。默认值：对 `select` 元素为 `true`。 |
| `timeout`       | 这个设置是在抛出异常之前，驱动程序等待数据库返回请求结果的秒数。默认值为未设置（`unset`）（依赖数据库驱动）。 |
| `fetchSize`     | 这是一个给驱动的建议值，尝试让驱动程序每次批量返回的结果行数等于这个设置值。 默认值为未设置（`unset`）（依赖驱动）。 |
| `statementType` | 可选 `STATEMENT`，`PREPARED` 或 `CALLABLE`。这会让 `MyBatis` 分别使用 `Statement`，`PreparedStatement` 或 `CallableStatement`，默认值：`PREPARED`。 |
| `resultSetType` | `FORWARD_ONLY`，`SCROLL_SENSITIVE`, `SCROLL_INSENSITIVE` 或 `DEFAULT`（等价于 `unset`） 中的一个，默认值为 `unset` （依赖数据库驱动）。 |
| `databaseId`    | 如果配置了数据库厂商标识（`databaseIdProvider`），`MyBatis` 会加载所有不带 `databaseId` 或匹配当前 `databaseId` 的语句；如果带和不带的语句都有，则不带的会被忽略。 |
| `resultOrdered` | 这个设置仅针对嵌套结果 `select` 语句：如果为 `true`将会假设包含了嵌套结果集或是分组，当返回一个主结果行时，就不会产生对前面结果集的引用。 这就使得在获取嵌套结果集的时候不至于内存不够用。默认值：`false`。 |
| `resultSets`    | 这个设置仅适用于多结果集的情况。它将列出语句执行后返回的结果集并赋予每个结果集一个名称，多个名称之间以逗号分隔。 |

```xml
<select
  id="selectPerson"
  parameterType="int"
  parameterMap="deprecated"
  resultType="hashmap"
  resultMap="personResultMap"
  flushCache="false"
  useCache="true"
  timeout="10"
  fetchSize="256"
  statementType="PREPARED"
  resultSetType="FORWARD_ONLY">
```

```xml
<select id="selectPerson" parameterType="int" resultType="hashmap">
  SELECT * FROM PERSON WHERE ID = #{id}
</select>
```

这个语句名为 `selectPerson`，接受一个 `int`（或 `Integer`）类型的参数，并返回一个 `HashMap` 类型的对象，其中的键是列名，值是结果行中的对应值。 `#{id}` 告诉 `MyBatis` 创建一个预处理语句（`PreparedStatement`）参数，在 `JDBC` 中，这样的一个参数在 `SQL` 中会由一个 `?` 来标识，并被传递到一个新的预处理语句中。

```java
// 近似的 JDBC 代码，非 MyBatis 代码...
String selectPerson = "SELECT * FROM PERSON WHERE ID=?";
PreparedStatement ps = conn.prepareStatement(selectPerson);
ps.setInt(1,id);
```

## 2. insert、update、delete

| 属性               | 描述                                                         |
| :----------------- | :----------------------------------------------------------- |
| `id`               | 在命名空间中唯一的标识符，可以被用来引用这条语句。           |
| `parameterType`    | 将会传入这条语句的参数的类全限定名或别名。这个属性是可选的，因为 `MyBatis` 可以通过类型处理器（`TypeHandler`）推断出具体传入语句的参数，默认值为未设置（`unset`）。 |
| `flushCache`       | 将其设置为 `true` 后，只要语句被调用，都会导致本地缓存和二级缓存被清空，默认值：（对 `insert`、`update` 和 `delete` 语句）`true`。 |
| `timeout`          | 这个设置是在抛出异常之前，驱动程序等待数据库返回请求结果的秒数。默认值为未设置（`unset`）（依赖数据库驱动）。 |
| `statementType`    | 可选 `STATEMENT`，`PREPARED` 或 `CALLABLE`。这会让 `MyBatis` 分别使用 `Statement`，`PreparedStatement` 或 `CallableStatement`，默认值：`PREPARED`。 |
| `useGeneratedKeys` | （仅适用于 `insert` 和 `update`）这会令 `MyBatis` 使用 `JDBC` 的 `getGeneratedKeys` 方法来取出由数据库内部生成的主键（比如：像 `MySQL` 和 `SQL Server` 这样的关系型数据库管理系统的自动递增字段），默认值：`false`。 |
| `keyProperty`      | （仅适用于 `insert` 和 `update`）指定能够唯一识别对象的属性，`MyBatis` 会使用 `getGeneratedKeys` 的返回值或 `insert` 语句的 `selectKey` 子元素设置它的值，默认值：未设置（`unset`）。如果生成列不止一个，可以用逗号分隔多个属性名称。 |
| `keyColumn`        | （仅适用于 `insert` 和 `update`）设置生成键值在表中的列名，在某些数据库（像 `PostgreSQL`）中，当主键列不是表中的第一列的时候，是必须设置的。如果生成列不止一个，可以用逗号分隔多个属性名称。 |
| `databaseId`       | 如果配置了数据库厂商标识（`databaseIdProvider`），``MyBatis` 会加载所有不带 `databaseId` 或匹配当前 `databaseId` 的语句；如果带和不带的语句都有，则不带的会被忽略。 |

```xml
<insert
  id="insertAuthor"
  parameterType="domain.blog.Author"
  flushCache="true"
  statementType="PREPARED"
  keyProperty=""
  keyColumn=""
  useGeneratedKeys=""
  timeout="20">

<update
  id="updateAuthor"
  parameterType="domain.blog.Author"
  flushCache="true"
  statementType="PREPARED"
  timeout="20">

<delete
  id="deleteAuthor"
  parameterType="domain.blog.Author"
  flushCache="true"
  statementType="PREPARED"
  timeout="20">
```

```xml
<insert id="insertAuthor">
  insert into Author (id,username,password,email,bio)
  values (#{id},#{username},#{password},#{email},#{bio})
</insert>

<update id="updateAuthor">
  update Author set
    username = #{username},
    password = #{password},
    email = #{email},
    bio = #{bio}
  where id = #{id}
</update>

<delete id="deleteAuthor">
  delete from Author where id = #{id}
</delete>
```

如果数据库支持自动生成主键的字段（比如 `MySQL` 和 `SQL Server`），那么你可以设置 `useGeneratedKeys="true"`，然后再把 `keyProperty` 设置为目标属性就好了。例如如果 `Author` 表已经在 `id` 列上使用了自动生成，那么语句可以修改为： 

```xml
<insert id="insertAuthor" useGeneratedKeys="true"
    keyProperty="id">
  insert into Author (username,password,email,bio)
  values (#{username},#{password},#{email},#{bio})
</insert>
```

如果数据库还支持多行插入, 你也可以传入一个 `Author` 数组或集合，并返回自动生成的主键。 

```xml
<insert id="insertAuthor" useGeneratedKeys="true"
    keyProperty="id">
  insert into Author (username, password, email, bio) values
  <foreach item="item" collection="list" separator=",">
    (#{item.username}, #{item.password}, #{item.email}, #{item.bio})
  </foreach>
</insert>
```

不支持自动生成主键列的数据库和可能不支持自动生成主键的 `JDBC` 驱动，`MyBatis` 有另外一种方法来生成主键。 

```xml
<insert id="insertAuthor">
  <selectKey keyProperty="id" resultType="int" order="BEFORE">
    select CAST(RANDOM()*1000000 as INTEGER) a from SYSIBM.SYSDUMMY1
  </selectKey>
  insert into Author
    (id, username, password, email,bio, favourite_section)
  values
    (#{id}, #{username}, #{password}, #{email}, #{bio}, #{favouriteSection,jdbcType=VARCHAR})
</insert>
```

在上面的示例中，首先会运行 `selectKey` 元素中的语句，并设置 `Author` 的 `id`，然后才会调用插入语句。这样就实现了数据库自动生成主键类似的行为，同时保持了 `Java` 代码的简洁。 

| 属性            | 描述                                                         |
| :-------------- | :----------------------------------------------------------- |
| `keyProperty`   | `selectKey` 语句结果应该被设置到的目标属性。如果生成列不止一个，可以用逗号分隔多个属性名称。 |
| `keyColumn`     | 返回结果集中生成列属性的列名。如果生成列不止一个，可以用逗号分隔多个属性名称。 |
| `resultType`    | 结果的类型。通常 MyBatis 可以推断出来，但是为了更加准确，写上也不会有什么问题。MyBatis 允许将任何简单类型用作主键的类型，包括字符串。如果生成列不止一个，则可以使用包含期望属性的 Object 或 Map。 |
| `order`         | 可以设置为 `BEFORE` 或 `AFTER`。如果设置为 `BEFORE`，那么它首先会生成主键，设置 `keyProperty` 再执行插入语句。如果设置为 `AFTER`，那么先执行插入语句，然后是 `selectKey` 中的语句 - 这和 Oracle 数据库的行为相似，在插入语句内部可能有嵌入索引调用。 |
| `statementType` | 和前面一样，MyBatis 支持 `STATEMENT`，`PREPARED` 和 `CALLABLE` 类型的映射语句，分别代表 `Statement`, `PreparedStatement` 和 `CallableStatement` 类型。 |

## 3. SQL

这个元素可以用来定义可重用的 `SQL` 代码片段以便在其它语句中使用。 参数可以静态地（在加载的时候）确定下来，并且可以在不同的 `include` 元素中定义不同的参数值。比如： 

```xml
<sql id="userColumns"> ${alias}.id,${alias}.username,${alias}.password </sql>
```

这个 SQL 片段可以在其它语句中使用，例如： 

```xml
<select id="selectUsers" resultType="map">
  select
    <include refid="userColumns"><property name="alias" value="t1"/></include>,
    <include refid="userColumns"><property name="alias" value="t2"/></include>
  from some_table t1
  cross join some_table t2
</select>
```

 也可以在 `include` 元素的 `refid` 属性或内部语句中使用属性值，例如： 

```
<sql id="sometable">
  ${prefix}Table
</sql>

<sql id="someinclude">
  from
    <include refid="${include_target}"/>
</sql>

<select id="select" resultType="map">
  select
    field1, field2, field3
  <include refid="someinclude">
    <property name="prefix" value="Some"/>
    <property name="include_target" value="sometable"/>
  </include>
</select>
```

## 4. 参数

# 6. 接口代理实现

## 1. 概述

`Mapper` 接口开发方法只需要编写 `Mapper` 接口，由 `Mybatis` 框架根据接口定义创建接口的动态代理对象，代理对象的方法体同上边 `Dao` 接口实现类方法。

## 2. 规范

`mapper` 接口开发需要遵循以下规范：

-  **`mapper` 接口的全限定名和 `Mapper.xml` 文件中的 `namespace` 相同**。
-  **`mapper` 接口方法名和 `Mapper.xml` 中定义的每个 `statement`的 `id` 相同**。
-  **`mapper` 接口方法的输入参数类型和 `mapper.xml` 中定义的每个 `SQL` 的 `parameterType` 的类型相同。**
-  **`mapper` 接口方法的输出参数类型和 `mapper.xml` 中定义的每个 `SQL` 的 `resultType` 的类型相同**。

## 3. 实现

### 1. StudentRepository

```java
package aygx.mybatis.repository;

import aygx.mybatis.entity.Student;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2022/03/12 11:37
 * @description
 */
public interface StudentRepository {
    /**
     * 获取学生数据
     *
     * @return 学生数据
     */
    List<Student> selectAll();
}
```

### 2. proxy

```java
@Test
public void proxy() throws IOException {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);
    // 执行查询（namespace+id，映射实体）根据属性名称和字段一一对应
    List<Student> studentList = studentRepository.selectAll();

    System.out.println(studentList);

    inputStream.close();
    sqlSession.close();
}
```

![1625151191977](img/1625151191977.png)

## 4. 分析

* 分析动态代理对象如何生成的？ 

  通过动态代理开发模式，我们只编写一个接口，不写实现类，我们通过 `getMapper()` 方法最终获取到 `org.apache.ibatis.binding.MapperProxy` 代理对象然后执行功能，而这个代理对象正是 `MyBatis` 使用了 `JDK` 的动态代理技术帮助我们生成了代理实现类对象。从而可以进行相关持久化操作。 

* 分析方法是如何执行的？

  动态代理实现类对象在执行方法的时候最终调用了 `mapperMethod.execute()` 方法，这个方法中通过 `switch` 语句根据操作类型来判断是新增、修改、删除、查询操作，最后一步回到了 `MyBatis` 最原生的 `SqlSession` 方式来执行增删改查。

# 7. 动态SQL

## 1. if

### 1. StudentRepository.xml

```xml
<select id="selectNameAndAge" resultType="aygx.mybatis.entity.Student">
    SELECT * FROM student
    WHERE 1=1
    <if test="name != null ">
        AND name like CONCAT('%',#{name},'%')
    </if>
    <if test="age != null ">
        AND age = #{age}
    </if>
</select>
```

### 2. StudentRepository

```java
List<Student> selectNameAndAge(Student student);
```

### 3. ifSql

这条语句提供了可选的查找文本功能 

```java
@Test
public void ifSql() throws IOException {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);
    // 执行查询（namespace+id，映射实体）根据属性名称和字段一一对应
    Student student = new Student();
    List<Student> studentList;
    studentList = studentRepository.selectNameAndAge(student);
    System.out.println(studentList);

    student.setName("暗");
    studentList = studentRepository.selectNameAndAge(student);
    System.out.println(studentList);

    student.setAge(18);
    studentList = studentRepository.selectNameAndAge(student);
    System.out.println(studentList);

    student.setName(null);
    studentList = studentRepository.selectNameAndAge(student);
    System.out.println(studentList);

    inputStream.close();
    sqlSession.close();
}
```

![1625152521823](img/1625152521823.png)

## 2. choose、when、otherwise

有时候，我们不想使用所有的条件，而只是想从多个条件中选择一个使用。针对这种情况，MyBatis 提供了 choose 元素，它有点像 Java 中的 switch 语句。 

### 1. StudentRepository.xml

```xml
<select id="selectChoose" resultType="aygx.mybatis.entity.Student">
    SELECT * FROM student
    WHERE 1=1
    <choose>
        <when test="name != null">
            AND name like CONCAT('%',#{name},'%')
        </when>
        <when test="age != null">
            AND age = #{age}
        </when>
        <otherwise>
            AND create_time >= now();
        </otherwise>
    </choose>
</select>
```

### 2. StudentRepository

```java
List<Student> selectChoose(Student student);
```

### 3. chooseSql

```java
@Test
public void chooseSql() throws IOException {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);
    // 执行查询（namespace+id，映射实体）根据属性名称和字段一一对应
    Student student = new Student();
    List<Student> studentList;
    studentList = studentRepository.selectChoose(student);
    System.out.println(studentList);

    student.setName("暗");
    studentList = studentRepository.selectChoose(student);
    System.out.println(studentList);

    student.setAge(18);
    studentList = studentRepository.selectChoose(student);
    System.out.println(studentList);

    student.setName(null);
    studentList = studentRepository.selectChoose(student);
    System.out.println(studentList);

    inputStream.close();
    sqlSession.close();
}
```

![1625153373869](img/1625153373869.png)

## 3. where

`where` 元素只会在子元素返回任何内容的情况下才插入 `WHERE` 子句。而且，若子句的开头为 `AND` 或 `OR`，`where` 元素也会将它们去除。 

### 1. StudentRepository.xml

```xml
<select id="selectWhere" resultType="aygx.mybatis.entity.Student">
    SELECT * FROM student
    <where>
        <if test="name != null ">
            AND name like CONCAT('%',#{name},'%')
        </if>
        <if test="age != null ">
            AND age = #{age}
        </if>
    </where>
</select>
```

### 2. StudentRepository

```java
List<Student> selectWhere(Student student);
```

### 3. whereSql

```java
@Test
public void whereSql() throws IOException {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);
    // 执行查询（namespace+id，映射实体）根据属性名称和字段一一对应
    Student student = new Student();
    List<Student> studentList;
    studentList = studentRepository.selectWhere(student);
    System.out.println(studentList);

    student.setName("暗");
    studentList = studentRepository.selectWhere(student);
    System.out.println(studentList);

    student.setAge(18);
    studentList = studentRepository.selectWhere(student);
    System.out.println(studentList);

    student.setName(null);
    studentList = studentRepository.selectWhere(student);
    System.out.println(studentList);

    inputStream.close();
    sqlSession.close();
}
```

![1625210960973](img/1625210960973.png)

### 4. trim

于 `where` 语句等价。`prefixOverrides` 属性会忽略通过管道符分隔的文本序列（注意此例中的空格是必要的）并且插入 `prefix` 属性中指定的内容。 

```xml
<trim prefix="WHERE" prefixOverrides="AND | OR ">
  ...
</trim>
```

## 5. set

`set` 元素会动态地在行首插入 `SET` 关键字，并会删掉额外的逗号（这些逗号是在使用条件语句给列赋值时引入的）。 

### 1. StudentRepository.xml

```xml
<update id="updateSet" parameterType="aygx.mybatis.entity.Student" >
    update student
    <set>
        <if test="name != null">
            ,name=#{name}
        </if>
        <if test="age != null">
            ,age=#{age}
        </if>
        ,update_time = now()
    </set>
    where id=#{id}
</update>
```

### 2. StudentRepository

```java
void updateSet(Student student);
```

### 3. setSql

```java
@Test
public void setSql() throws IOException {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);
    Student student = new Student();
    student.setId(2);
    studentRepository.updateSet(student);

    student.setName("暗");
    studentRepository.updateSet(student);

    student.setAge(18);
    studentRepository.updateSet(student);

    student.setName(null);
    studentRepository.updateSet(student);

    inputStream.close();
    sqlSession.close();
}
```

![1625214470613](img/1625214470613.png)

### 4. trim

与 `set` 元素等价的自定义 `trim` 元素

```xml
<trim prefix="SET" suffixOverrides=",">
  ...
</trim>
```

## 5. foreach

`foreach` 元素允许指定一个集合，声明可以在元素体内使用的集合项（`item`）和索引（`index`）变量。也允许指定开头与结尾的字符串以及集合项迭代之间的分隔符。当使用可迭代对象或者数组时，`index` 是当前迭代的序号，`item` 的值是本次迭代获取到的元素。当使用 `Map` 对象（或者 `Map.Entry` 对象的集合）时，`index` 是键，`item` 是值。 

### 1. StudentRepository.xml

```xml
<insert id="insertForeach" parameterType="aygx.mybatis.entity.Student">
    insert into student (name,age,create_time,update_time) values
    <foreach item="item" index="index" collection="list"
             open="" separator="," close="">
        ( #{item.name},#{item.age},now(),now())
    </foreach>
</insert>
```

### 2. StudentRepository

```xml
void insertForeach(List<Student> studentList);
```

### 3. foreachSql

```java
@Test
public void foreachSql() throws IOException {
// 获取Mybatis文件流
InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
// 获取SqlSessionFactory
SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
// 获取SqlSession
SqlSession sqlSession = sqlSessionFactory.openSession();

StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);

List<Student> studentList = new ArrayList<>();
    studentList.add(new Student("暗", 15));
    studentList.add(new Student("影", 16));
    studentList.add(new Student("孤", 17));
    studentList.add(new Student("星", 18));
    studentRepository.insertForeach(studentList);

    sqlSession.commit();
    inputStream.close();
    sqlSession.close();
    }
```

![1625226972484](img/1625226972484.png)

## 6. bind

`bind` 元素允许你在 `OGNL` 表达式以外创建一个变量，并将其绑定到当前的上下文。

```xml
<select id="selectBlogsLike" resultType="Blog">
  <bind name="pattern" value="'%' + _parameter.getTitle() + '%'" />
  SELECT * FROM BLOG
  WHERE title LIKE #{pattern}
</select>
```

## 7. 多数据库支持

如果配置了 `databaseIdProvider`，就可以在动态代码中使用名为 `_databaseId` 的变量来为不同的数据库构建特定的语句。 

```xml
<insert id="insert">
  <selectKey keyProperty="id" resultType="int" order="BEFORE">
    <if test="_databaseId == 'oracle'">
      select seq_users.nextval from dual
    </if>
    <if test="_databaseId == 'db2'">
      select nextval for seq_users from sysibm.sysdummy1"
    </if>
  </selectKey>
  insert into users values (#{id}, #{name})
</insert>
```

# 8. 分页插件

## 1. 概述

`MyBatis` 可以使用第三方的插件来对功能进行扩展，分页助手 `PageHelper` 是将分页的复杂操作进行封装，使用简单的方式即可获得分页的相关数据。

## 2. 使用

### 1. pom.xml

```xml
<!-- 分页插件 -->
<dependency>
    <groupId>com.github.pagehelper</groupId>
    <artifactId>pagehelper</artifactId>
    <version>5.2.0</version>
</dependency>
```

### 2. MybatisConfig.xml

```xml
<plugins>
    <!-- 注意：分页助手的插件  配置在通用mapper之前 -->
    <plugin interceptor="com.github.pagehelper.PageHelper">
        <!-- 指定方言 -->
        <property name="dialect" value="mysql"/>
    </plugin>
</plugins>
```

### 3. page

```java
@Test
public void page() throws IOException {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);

    //设置分页参数(当前页，煤业行数)
    PageHelper.startPage(2,2);
    List<Student> studentList = studentRepository.selectAll();
    System.out.println(studentList);

    PageInfo<Student> pageInfo = new PageInfo<>(studentList);
    System.out.println(pageInfo);

    inputStream.close();
    sqlSession.close();
}
```

![1625228963216](img/1625228963216.png)

# 9. 多表操作

## 1. 一对一

### 1. sql

```sql
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;
INSERT INTO `person` VALUES (1, '张三', 23);
INSERT INTO `person` VALUES (2, '李四', 24);
INSERT INTO `person` VALUES (3, '王五', 25);

DROP TABLE IF EXISTS `card`;
CREATE TABLE `card`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;
INSERT INTO `card` VALUES (1, 12345, 1);
INSERT INTO `card` VALUES (2, 23456, 2);
INSERT INTO `card` VALUES (3, 34567, 3);
```

### 2. Card

```java
package aygx.mybatis.entity;

import lombok.Data;

/**
 * @author 暗影孤星
 * @date 2022/03/12 12:37
 * @description
 */
@Data
public class Card {
    /**
     * 名称
     */
    private Integer id;

    /**
     * 号码
     */
    private Integer number;

    /**
     *
     */
    private Person person;
}
```

### 3. Person

```java
package aygx.mybatis.entity;

import lombok.Data;

/**
 * @author 暗影孤星
 * @date 2022/03/12 12:37
 * @description
 */
@Data
public class Person {
    /**
     * 名称
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;
}
```

### 4. OneToOneRepository

```java
package aygx.mybatis.repository;

import aygx.mybatis.entity.Card;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2022/03/12 12:38
 * @description
 */
public interface OneToOneRepository {
    List<Card> selectAll();
}
```

### 5. OneToOneRepository.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="aygx.mybatis.repository.OneToOneRepository">
    <!-- resultMap：配置字段和实体对象属性的映射关系
           id：唯一标识
           type：实体对象类型
    -->
    <resultMap id="oneToOne" type="card">
        <!-- id：主键列映射
                column：列名
                property：变量名
        -->
        <id column="cid" property="id" />
        <result column="number" property="number" />
        <!-- association：配置被包含对象的映射关系
                property：变量名
                javaType：实体对象类型
        -->
        <association property="person" javaType="person">
            <id column="pid" property="id" />
            <result column="name" property="name" />
            <result column="age" property="age" />
        </association>
    </resultMap>

    <select id="selectAll" resultMap="oneToOne">
        SELECT c.id cid,number,p.id pid,name,age FROM card c,person p WHERE c.person_id=p.id
    </select>
</mapper>
```

### 6. test

```java
@Test
public void oneToOne() throws Exception {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    //4.获取OneToOneMapper接口的实现类对象
    OneToOneRepository oneToOneRepository = sqlSession.getMapper(OneToOneRepository.class);

    //5.调用实现类的方法，接收结果
    oneToOneRepository.selectAll().forEach(System.out::println);


    //7.释放资源
    sqlSession.close();
    inputStream.close();
}
```

![1625582282371](img/1625582282371.png)

## 2. 一对多

### 1. sql

```sql
DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;
INSERT INTO `classes` VALUES (1, '一班');
INSERT INTO `classes` VALUES (2, '二班');

DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `age` int(11) NOT NULL COMMENT '年龄',
  `classes_id` int(11) NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生' ROW_FORMAT = Dynamic;
INSERT INTO student VALUES (NULL,'张三',23,1,NOW(),NOW());
INSERT INTO student VALUES (NULL,'李四',24,1,NOW(),NOW());
INSERT INTO student VALUES (NULL,'王五',25,2,NOW(),NOW());
INSERT INTO student VALUES (NULL,'赵六',26,2,NOW(),NOW());
```

### 2. Classes

```java
package aygx.mybatis.entity;

import lombok.Data;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2021/7/5 23:56
 * @description 班级
 */
@Data
public class Classes {

    /**
     * 名称
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 学生
     */
    private List<Student> studentList;
}
```

### 3. Student

```java
package aygx.mybatis.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author 暗影孤星
 * @date 2021/6/17 15:37
 * @description 学生
 */
@Data
@NoArgsConstructor
public class Student {

    /**
     * 名称
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
}
```

### 4. OneToManyRepository

```java
package aygx.mybatis.repository;

import aygx.mybatis.entity.Card;
import aygx.mybatis.entity.Classes;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2021/7/6 0:04
 * @description
 */
public interface OneToManyRepository {
    List<Classes> selectAll();
}
```

### 5. OneToManyRepository.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="aygx.mybatis.repository.OneToManyRepository">

    <resultMap id="oneToMany" type="classes">
        <id column="cid" property="id"/>
        <result column="cname" property="name"/>

        <!-- collection：被包含的集合对象映射关系
                property：变量名
                ofType：实体对象类型
        -->
        <collection property="studentList" ofType="student">
            <id column="sid" property="id"/>
            <result column="sname" property="name"/>
            <result column="sage" property="age"/>
        </collection>
    </resultMap>
    <select id="selectAll" resultMap="oneToMany">
        SELECT c.id cid,c.name cname,s.id sid,s.name sname,s.age sage FROM classes c,student s WHERE c.id=s.classes_id
    </select>
</mapper>
```

### 6. test

```java
@Test
public void oneToMany() throws Exception {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    OneToManyRepository mapper = sqlSession.getMapper(OneToManyRepository.class);

    //5.调用实现类的方法，接收结果
    mapper.selectAll().forEach(System.out::println);

    //7.释放资源
    sqlSession.close();
    inputStream.close();
}
```

![1625582479558](img/1625582479558.png)

## 3. 多对多

### 1. sql

```sql
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;
INSERT INTO `course` VALUES (1, '语文');
INSERT INTO `course` VALUES (2, '数学');

DROP TABLE IF EXISTS `course_student`;
CREATE TABLE `course_student`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NULL DEFAULT NULL,
  `student_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;
INSERT INTO `course_student` VALUES (1, 1, 1);
INSERT INTO `course_student` VALUES (2, 1, 2);
INSERT INTO `course_student` VALUES (3, 2, 1);
INSERT INTO `course_student` VALUES (4, 2, 2);
```

### 2. Classes

```java
package aygx.mybatis.entity;

import lombok.Data;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2021/7/5 23:56
 * @description 班级
 */
@Data
public class Classes {

    /**
     * 名称
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 学生
     */
    private List<Student> studentList;
}
```

### 3. Student

```java
package aygx.mybatis.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 暗影孤星
 * @date 2021/6/17 15:37
 * @description 学生
 */
@Data
@NoArgsConstructor
public class Student {

    /**
     * 名称
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    /**
     * 课程
     */
    private List<Course> courseList;

    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
}
```

### 4. ManyToManyRepository

```java
package aygx.mybatis.repository;

import aygx.mybatis.entity.Classes;
import aygx.mybatis.entity.Student;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2021/7/6 22:45
 * @description
 */
public interface ManyToManyRepository {
    List<Student> selectAll();
}
```

### 5. ManyToManyRepository.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="aygx.mybatis.repository.ManyToManyRepository">

    <resultMap id="manyToMany" type="student">
        <id column="student_id" property="id"/>
        <result column="sname" property="name"/>
        <result column="sage" property="age"/>

        <!-- collection：被包含的集合对象映射关系
                property：变量名
                ofType：实体对象类型
        -->
        <collection property="courseList" ofType="course">
            <id column="course_id" property="id"/>
            <result column="cname" property="name"/>
        </collection>
    </resultMap>
    <select id="selectAll" resultMap="manyToMany">
        SELECT cs.student_id,s.name sname,s.age sage,cs.course_id,c.name cname FROM student s,course c,course_student cs WHERE cs.student_id=s.id AND cs.course_id=c.id
    </select>
</mapper>
```

### 6. test

```java
@Test
public void manyToMany() throws Exception {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    ManyToManyRepository mapper = sqlSession.getMapper(ManyToManyRepository.class);

    //5.调用实现类的方法，接收结果
    mapper.selectAll().forEach(System.out::println);

    //7.释放资源
    sqlSession.close();
    inputStream.close();
}
```

# 10. 缓存

## 1. 一级缓存

一级缓存是 `SqlSession` 级别的，通过同一个 `SqlSession` 查询的数据会被缓存，下次查询相同的数据，就会从缓存中直接获取，不会从数据库重新访问。一级缓存默认开启。

使一级缓存失效的四种情况：

-  不同的 `SqlSession` 对应不同的一级缓存。
-  同一个 `SqlSession` 但是查询条件不同。
-  同一个 `SqlSession` 两次查询期间执行了任何一次增删改操作。
-  同一个 `SqlSession` 两次查询期间手动清空了缓存。

一级缓存生命周期：

-  `MyBatis` 在开启一个数据库会话时会创建一个新的 `SqlSession` 对象，`SqlSession` 对象中会有一个新的`Executor` 对象，`Executor` 对象中持有一个新的 `PerpetualCache` 对象；当会话结束时 `SqlSession` 对象及其内部的 `Executor` 对象还有 `PerpetualCache` 对象也一并释放掉。
-  如果 `SqlSession` 调用了 `close()`  则会释放掉一级缓存 `PerpetualCache` 对象，一级缓存将不可用。
-  如果 `SqlSession` 调用了 `clearCache()` ，会清空 `PerpetualCache` 对象中的数据，但是该对象仍可使用。
-  `SqlSession` 中执行了任何一个增删改操作都会清空 `PerpetualCache` 对象的数据，但是该对象可以继续使用。

```java
package aygx.mybatis.test;

import aygx.mybatis.repository.StudentRepository;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author 暗影孤星
 * @date 2022/4/19 19:18
 * @description 缓存
 */
public class CacheTest {

    @Test
    public void levelOne() throws IOException {
        // 获取Mybatis文件流
        InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
        // 获取SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 获取SqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 获取mapper
        //DEBUG - ==>  Preparing: SELECT * FROM student
        //DEBUG - ==> Parameters:
        //DEBUG - <==      Total: 5
        //[Student(id=1, name=暗影孤星, age=18, createTime=2022-03-23T20:16:29, updateTime=2022-03-23T20:16:29, courseList=null), Student(id=2, name=暗, age=16, createTime=2022-03-23T20:16:52, updateTime=2022-03-23T20:16:55, courseList=null), Student(id=3, name=影, age=14, createTime=2022-03-23T20:17:05, updateTime=2022-03-23T20:17:07, courseList=null), Student(id=4, name=孤, age=12, createTime=2022-03-23T20:17:32, updateTime=2022-03-23T20:17:34, courseList=null), Student(id=5, name=星, age=10, createTime=2022-03-23T20:17:46, updateTime=2022-03-23T20:17:48, courseList=null)]
        //[Student(id=1, name=暗影孤星, age=18, createTime=2022-03-23T20:16:29, updateTime=2022-03-23T20:16:29, courseList=null), Student(id=2, name=暗, age=16, createTime=2022-03-23T20:16:52, updateTime=2022-03-23T20:16:55, courseList=null), Student(id=3, name=影, age=14, createTime=2022-03-23T20:17:05, updateTime=2022-03-23T20:17:07, courseList=null), Student(id=4, name=孤, age=12, createTime=2022-03-23T20:17:32, updateTime=2022-03-23T20:17:34, courseList=null), Student(id=5, name=星, age=10, createTime=2022-03-23T20:17:46, updateTime=2022-03-23T20:17:48, courseList=null)]
        StudentRepository studentRepository = sqlSession.getMapper(StudentRepository.class);
        System.out.println(studentRepository.selectAll());
        System.out.println(studentRepository.selectAll());

        // 关闭资源
        inputStream.close();
        sqlSession.close();
    }
}
```

![image-20220419194944319](img/image-20220419194944319.png)

## 2. 二级缓存

二级缓存是 `SqlSessionFactory` 级别，通过同一个 `SqlSessionFactory` 创建的 `SqlSession` 查询的结果会被缓存。此后若再次执行相同的查询语句，结果就会从缓存中获取。

二级缓存开启的条件：

-  在核心配置文件中，设置全局配置属性 `<setting name="cacheEnabled" value="true"/>`。创建的执行器为 `CachingExecutor`。
-  在映。射文件中设置标签 `<cache/>`。
-  二级缓存必须在 `SqlSession` 关闭或提交之后有效。
-  查询的数据所转换的实体类类型必须实现序列化的接口。

使二级缓存失效的情况：

-  两次查询之间执行了任意的增删改会使一级和二级缓存同时失效。

## 3. 二级缓存的相关配置

在 `mapper` 配置文件中添加的 `cache` 标签可以设置一些属性：

-  `eviction` 属性：缓存回收策略
   -  `LRU（Least Recently Used）` – 最近最少使用的：移除最长时间不被使用的对象。
   -  `FIFO（First in First out）` – 先进先出：按对象进入缓存的顺序来移除它们。
   -  `SOFT` – 软引用：移除基于垃圾回收器状态和软引用规则的对象。当内存不足会触发 `JVM` 的 `GC`，如果 `GC` 后内存还是不足，就会把软引用包裹的对象给干掉，也就是只有内存不足，`JVM` 才会回收该对象。
   -  `WEAK` – 弱引用：更积极地移除基于垃圾收集器状态和弱引用规则的对象。弱引用的特点是不管内存是否足够，只要发生 `GC` 都会被回收。
   -  默认的是 `LRU`。
-  `flushInterval` 属性：刷新间隔，单位毫秒。默认情况是不设置，也就是没有刷新间隔，缓存仅仅调用语句时刷新。
-  `size` 属性：引用数目，正整数。代表缓存最多可以存储多少个对象，太大容易导致内存溢出
-  `readOnly` 属性：是否只读。`true`：只读缓存；会给所有调用者返回缓存对象的相同实例。因此这些对象不能被修改。这提供了很重要的性能优势。`false`：读写缓存；会返回缓存对象的拷贝（通过序列化）。这会慢一些，但是安全，因此默认是 `false`。

## 4. 缓存查询的顺序

先查询二级缓存，因为二级缓存中可能会有其他程序已经查出来的数据可以拿来直接使用。如果二级缓存没有命中再查询一级缓存。如果一级缓存也没有命中则查询数据库。`SqlSession` 关闭之后，一级缓存中的数据会写入二级缓存。

## 5. 整合 EHCache

### 1. pom.xml

```xml
<!-- Mybatis EHCache整合包 -->
<dependency>
    <groupId>org.mybatis.caches</groupId>
    <artifactId>mybatis-ehcache</artifactId>
    <version>1.2.1</version>
</dependency>
```

### 2. ehcache.xml

```xml
<?xml version="1.0" encoding="utf-8" ?>
<ehcache xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:noNamespaceSchemaLocation="../config/ehcache.xsd">
    <!-- 磁盘保存路径 -->
    <diskStore path="D:\atguigu\ehcache"/>
    <defaultCache maxElementsInMemory="1000"
                  maxElementsOnDisk="10000000"
                  eternal="false"
                  overflowToDisk="true"
                  timeToIdleSeconds="120"
                  timeToLiveSeconds="120"
                  diskExpiryThreadIntervalSeconds="120"
                  memoryStoreEvictionPolicy="LRU"/>
</ehcache>
```

### 3. 设置二级缓存的类型

```xml
<cache type="org.mybatis.caches.ehcache.EhcacheCache"/>
```



# 9. 注解开发

## 1. 常用注解

| 注解     | 说明                                                         |
| -------- | ------------------------------------------------------------ |
| @Insert  | 实现新增。                                                   |
| @Update  | 实现更新。                                                   |
| @Delete  | 实现删除。                                                   |
| @Select  | 实现查询。                                                   |
| @Result  | 实现结果集封装。代替 `<id>` 和 `<result>` 标签，属性：`column`：列名，`property`：属性名，`one`：需要使用 `@one` 注解（`@Result(one=@one)`），`many` ：需要使用 `@many` 注解（`@Result(many=@many)`） |
| @Results | 可以与 `@Result` 一起使用，封装多个结果集。代替了 `<resultMap>` 标签，该注解中可以使用单个的 `@Result` 注解，也可以使用 `@Result` 集合。格式：`@Results({@Result(),@Result()})` |
| @One     | 实现一对一结果集封装。代替了 `<assocation>` 标签，属性： `select` ：指定用来查询的语句，格式：`@Result(column="",property="",one=@One=(select=""))` |
| @Many    | 实现一对多结果集封装。代替了 `<collection>` 标签，属性： `select` ：指定用来查询的语句，格式：`@Result(column="",property="",many=@Many=(select=""))` |

## 2. 一对一

### 1. CardRepository

```java
package aygx.mybatis.repository;

import aygx.mybatis.entity.Card;
import aygx.mybatis.entity.Person;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2022/03/12 13:37
 * @description
 */
public interface CardRepository {
    /**
     * 获取身份证数据
     *
     * @return 身份证数据
     */
    @Select("SELECT * FROM card")
    @Results({
            @Result(column = "id", property = "id"),                                            // 列映射
            @Result(
                    property = "person",                                                        // 变量名
                    javaType = Person.class,                                                    // 被包含对象的实际数据类型
                    column = "person_id",                                                       // 列名
                    one = @One(select = "aygx.mybatis.repository.PersonRepository.selectById")  // one=@One 一对一固定写法
            )
    })
    List<Card> selectAll();
}
```

### 2. PersonRepository

```java
package aygx.mybatis.repository;

import aygx.mybatis.entity.Person;
import org.apache.ibatis.annotations.Select;

/**
 * @author 暗影孤星
 * @date 2022/03/12 13:37
 * @description
 */
public interface PersonRepository {
    /**
     * 根据ID获取用户数据
     *
     * @param id 主键
     * @return 用户数据
     */
    @Select("SELECT * FROM person WHERE id=#{id}")
    Person selectById(Integer id);
}
```

### 3. MybatisConfig.xml

```xml
<!-- mappers引入映射配置文件 -->
<mappers>
    <!-- mapper 引入指定的映射配置文件   resource属性指定映射配置文件的名称 -->
    <mapper resource="StudentRepository.xml"/>
    <mapper resource="OneToOneRepository.xml"/>
    <mapper resource="oneToManyRepository.xml"/>
    <mapper resource="manyToManyRepository.xml"/>
    <mapper class="aygx.mybatis.repository.PersonRepository"/>
    <mapper class="aygx.mybatis.repository.CardRepository"/>
    <mapper class="aygx.mybatis.repository.ClassesRepository"/>
    <mapper class="aygx.mybatis.repository.CourseRepository"/>
</mappers>
```

### 4. test

```java
@Test
public void oneToOne() throws Exception {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    //4.获取OneToOneMapper接口的实现类对象
    CardRepository repository = sqlSession.getMapper(CardRepository.class);

    //5.调用实现类的方法，接收结果
    repository.selectAll().forEach(System.out::println);


    //7.释放资源
    sqlSession.close();
    inputStream.close();
}
```

![1625585728076](img/1625585728076.png)

## 2. 一对多

### 1. ClassesRepository

```java
package aygx.mybatis.repository;

import aygx.mybatis.entity.Classes;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2022/03/12 14:01
 * @description
 */
public interface ClassesRepository {

    /**
     * 获取班级数据
     *
     * @return 班级数据
     */
    @Select("SELECT * FROM classes")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(
                    property = "studentList",                                                      // 被包含对象的变量名
                    javaType = List.class,                                                         // 被包含对象的实际数据类型
                    column = "id",                                                                 // 根据查询出的classes表的id字段来查询student表
                    many = @Many(select = "aygx.mybatis.repository.StudentRepository.selectByCid") // many=@Many一对多查询的固定写法
            )
    })
    List<Classes> selectAll();
}
```

### 2. StudentRepository

```java
/**
     * 根据班级ID获取学生数据
     *
     * @param classesId 班级ID
     * @return 学生数据
     */
@Select("SELECT * FROM student WHERE classes_id=#{classesId}")
List<Student> selectByCid(Integer classesId);
```

### 3. test

```java
@Test
public void oneToMany() throws Exception {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    ClassesRepository repository = sqlSession.getMapper(ClassesRepository.class);

    //5.调用实现类的方法，接收结果
    repository.selectAll().forEach(System.out::println);

    //7.释放资源
    sqlSession.close();
    inputStream.close();
}
```

![1625586026268](img/1625586026268.png)

## 3. 多对多

### 1. StudentRepository

```java
@Select("SELECT DISTINCT s.id,s.name,s.age FROM student s,course_student cs WHERE cs.student_id=s.id")
@Results({
    @Result(column = "id",property = "id"),
    @Result(
        property = "courseList",                                                           // 被包含对象的变量名
        javaType = List.class,                                                             // 被包含对象的实际数据类型
        column = "id",                                                                     // 根据查询出student表的id来作为关联条件，去查询中间表和课程表
        many = @Many(select = "aygx.mybatis.repository.CourseRepository.selectBySid")      // many=@Many一对多查询的固定写法
    )
})
List<Student> selectList();
```

### 2. CourseRepository

```java
@Select("SELECT c.id,c.name FROM course_student cs,course c WHERE cs.course_id=c.id AND cs.student_id=#{id}")
List<Course> selectBySid(Integer id);
```

### 3. test

```java
@Test
public void manyToMany() throws Exception {
    // 获取Mybatis文件流
    InputStream inputStream = Resources.getResourceAsStream("MybatisConfig.xml");
    // 获取SqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    // 获取SqlSession
    SqlSession sqlSession = sqlSessionFactory.openSession();

    StudentRepository mapper = sqlSession.getMapper(StudentRepository.class);

    //5.调用实现类的方法，接收结果
    mapper.selectList().forEach(System.out::println);

    //7.释放资源
    sqlSession.close();
    inputStream.close();
}
```

![1625586456647](img/1625586456647.png)

# 10. 构建 SQL

`MyBatis` 给我们提供了 `org.apache.ibatis.jdbc.SQL` 功能类，专门用于构建 `SQL` 语句  

| 方法名                              | 说明                         |
| ----------------------------------- | ---------------------------- |
| SELECT(String...column)             | 根据字段拼接查询语句         |
| FROM(String...table)                | 根据表名拼接语句             |
| WHERE(String...condition)           | 根据条件拼接语句             |
| INSERT_INTO(String table)           | 根据表名敏捷新增语句         |
| VALUES(String column,String values) | 根据字段和值拼接插入数据语句 |
| UPDATE(String table)                | 根据表名拼接修改语句         |
| DELETE_FROM(String table)           | 根据表名拼接删除语句         |

#### 3.2  查询功能的实现

* 定义功能类并提供获取查询的 SQL 语句的方法。 

* @SelectProvider：生成查询用的 SQL 语句注解。

  type 属性：生成 SQL 语句功能类对象 

  method 属性：指定调用方法    

#### 3.3  新增功能的实现

* 定义功能类并提供获取新增的 SQL 语句的方法。 

* @InsertProvider：生成新增用的 SQL 语句注解。 

  type 属性：生成 SQL 语句功能类对象 

  method 属性：指定调用方法    

#### 3.4  修改功能的实现

* 定义功能类并提供获取修改的 SQL 语句的方法。 

* @UpdateProvider：生成修改用的 SQL 语句注解。 

  type 属性：生成 SQL 语句功能类对象

   method 属性：指定调用方法    

#### 3.5  删除功能的实现

* 定义功能类并提供获取删除的 SQL 语句的方法。 

* @DeleteProvider：生成删除用的 SQL 语句注解。

  type 属性：生成 SQL 语句功能类对象 

  method 属性：指定调用方法    

***\*没有进行任何指定，MyBatis是如何把数据库查询结果中字段的值赋给Java中实体类的对应属性呢？这用到了自动映射技术\****。

Auto Mapping(结果自动映射)是MyBatis提供的按照名称自动把结果集中数据映射到对象属性的一种技术。查询到结果集后按照名称去类中找属性的set方法进行赋值。在新版本中如果属性没有提供set方法，则直接找同名属性进行赋值

![img](img/wps1-1625656450975.jpg) 

在映射过程中MyBatis会自动进行类型转换，也就是说即使varchar类型name，如

果可以保证里面值都能被转换为数字，在Java中也可以使用int类型name接收。

在数据库中列名规范是xx_xxx这种形式，中间使用下划线分割。而在Java中属性名按照小驼峰方式进行命名，这就可能导致列名和属性名不一致的问题，针对这种方式最简单的处理办法就是给列起别名，进行auto mapping

例如：列名 sxt_id 属性名sxtId 利用别名方式的SQL如下：

select sxt_id as sxtid from 表。

此处注意：列名或别名不考虑大小写问题



1) DML操作的底层调用executeUpdate()，返回值都是int类型，不需要提供，也没有resultType属性来指定。

2) 其实insert、update、delete任何一个元素都可以完成所有DML操作的映射

1) 执行DML操作，默认事务手动提交。此时可以两种方案解决

a. 使用自动提交

b. 手动执行commit ()或者rollback()结束事务 

推荐手动提交事务。因为复杂业务中一个事务会包括多个DML操作，自动提交只能做到一个事务只有一个DML操作。

2) 其实一个SqlSession的update、delete、insert中任意一个方法均可完成所有DML操作。***\*底层都是调用的update方法\****，就好比JDBC中的executeUpdate()可以完成DML操作一样。

***\*生命周期\****

SqlSessionFactoryBuilder：

该类用来创建SqlSessionFactory对象，当SqlSessionFactory对象被创建后，该对象也就没有存在的必要了。

SqlSessionFactory: 

该对象应该在你的应用执行期间一直存在，由于要从该对象中获取SqlSession对象，这样的操作会相当频繁，同时创建SqlSessionFactory对象是一件一起消耗资源的事，因此，该对象的生命周期应该为应用返回，即与当前应用具有相同生命周期。

SqlSession: 

每个线程都应该有自己的SqlSession实例，SqlSession实例不能被共享，是线程不安全的，因此最佳的范围是请求或方法范围。

Mapper

关闭SqlSessin的时候也就关闭了由其所产生的Mapper。

### ***\*3.4关于日志管理\****

Mybatis 的内置日志工厂（LogFactory）提供日志功能，内置日志工厂将日志交给以下其中一种工具作代理：

l SLF4J

l Apache Commons Logging

l Log4j 2

l Log4j

l JDK logging

l NO_LOGGING

MyBatis 内置日志工厂基于运行时自省机制选择合适的日志工具。它会使用第一个查找得到的工具（按上文列举的顺序查找）。如果一个都未找到，日志功能就会被禁用。也就是说在项目中把日志工具环境配置出来后，不用再MyBatis进行配置就可以让日志生效。

![img](img/wps2.jpg) 

不少应用服务器（如 Tomcat 和 WebShpere）的类路径中已经包含 Commons Logging，所以在这种配置环境下的 MyBatis 会把它作为日志工具，记住这点非常重要。这将意味着，在诸如 WebSphere 的环境中，它提供了 Commons Logging 的私有实现，你的 Log4J 配置将被忽略。MyBatis 将你的 Log4J 配置忽略掉是相当令人郁闷的（事实上，正是因为在这种配置环境下，MyBatis 才会选择使用 Commons Logging 而不是 Log4J）。如果你的应用部署在一个类路径已经包含 Commons Logging 的环境中，而你又想使用其它日志工具，你可以通过在 MyBatis 配置文件 mybatis-config.xml 里面添加一项 setting 来选择别的日志工具。



 

![img](img/wps3.jpg) 

无论使用哪种日志工具对于程序员来说目的都是一样：打印运行过程中日志信息。日志信息中对平时开发最重要的是运行过程中***\*SQL的打印\****，这也是在开发过程中MyBatis日志的重要性。

1) 使用#{}，底层使用PreparedStatement；而是要${}，底层使用Statement了，会有SQL注入风险，不建议使用。 

# 11. Mybatis 逆向工程

正向工程：先创建 `Java`实体类，由框架负责根据实体类生成数据库表。`Hibernate` 是支持正向工程的。

逆向工程：先创建数据库表，由框架负责根据数据库表，反向生成如下资源：`Java` 实体类，`Mapper` 接口，`Mapper` 映射文件。

## 1. 

# 12. Mybatis-Plus 介绍

## 1. 网址

https://baomidou.com/

## 2. 概述

`MyBatis-Plus`（简称 `MP`）是一个 `MyBatis` 的增强工具，在 `MyBatis` 的基础上只做增强不做改变，为简化开发、提高效率而生。 

## 3. 特点

- **无侵入**：只做增强不做改变，引入它不会对现有工程产生影响，如丝般顺滑
- **损耗小**：启动即会自动注入基本 `CURD`，性能基本无损耗，直接面向对象操作
- **强大的 `CRUD` 操作**：内置通用 `Mapper`、通用 `Service`，仅仅通过少量配置即可实现单表大部分 `CRUD` 操作，更有强大的条件构造器，满足各类使用需求
- **支持 `Lambda` 形式调用**：通过 `Lambda` 表达式，方便的编写各类查询条件，无需再担心字段写错
- **支持主键自动生成**：支持多达 4 种主键策略（内含分布式唯一 ID 生成器 - `Sequence`），可自由配置，完美解决主键问题
- **支持 `ActiveRecord` 模式**：支持 `ActiveRecord` 形式调用，实体类只需继承 `Model` 类即可进行强大的 `CRUD` 操作
- **支持自定义全局通用操作**：支持全局通用方法注入（ Write once, use anywhere ）
- **内置代码生成器**：采用代码或者 `Maven` 插件可快速生成 `Mapper` 、 `Model` 、 `Service` 、 `Controller` 层代码，支持模板引擎。
- **内置分页插件**：基于 `MyBatis` 物理分页，开发者无需关心具体操作，配置好插件之后，写分页等同于普通 `List` 查询
- **分页插件支持多种数据库**：支持 `MySQL`、`MariaDB`、`Oracle`、`DB2`、`H2`、`HSQL`、`SQLite`、`Postgre`、`SQL Server` 等多种数据库
- **内置性能分析插件**：可输出 `SQL` 语句以及其执行时间，建议开发测试时启用该功能，能快速揪出慢查询
- **内置全局拦截插件**：提供全表 `delete` 、 `update` 操作智能分析阻断，也可自定义拦截规则，预防误操作

## 4. 框架结构

![image-20220311105949060](img/image-20220311105949060.png)

## 5. pom.xml

引入 `MyBatis-Plus` 之后请不要再次引入 `MyBatis` 以及 `MyBatis-Spring`，以避免因版本差异导致的问题

```xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>mybatis-plus-latest-version</version>
</dependency>
```

# 13. Mybatis-Plus 入门

## 1. SQL

```sql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '账户' ROW_FORMAT = DYNAMIC;

INSERT INTO `user` VALUES (1, 'Jone', 18, 'test1@baomidou.com');
INSERT INTO `user` VALUES (2, 'Jack', 20, 'test2@baomidou.com');
INSERT INTO `user` VALUES (3, 'Tom', 28, 'test3@baomidou.com');
INSERT INTO `user` VALUES (4, 'Sandy', 21, 'test4@baomidou.com');
INSERT INTO `user` VALUES (5, 'Billie', 24, 'test5@baomidou.com');

SET FOREIGN_KEY_CHECKS = 1;
```

## 2. pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>aygx</groupId>
    <artifactId>mybatis_plus</artifactId>
    <version>1.0.0</version>

    <parent>
        <artifactId>spring-boot-starter-parent</artifactId>
        <groupId>org.springframework.boot</groupId>
        <version>2.3.4.RELEASE</version>
    </parent>

    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <maven.compiler.encoding>UTF-8</maven.compiler.encoding>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    </properties>

    <dependencies>

        <!-- mybatis-plus -->
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.5.0</version>
        </dependency>

        <!-- mysql -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>

        <dependency>
            <groupId>cn.hutool</groupId>
            <artifactId>hutool-all</artifactId>
            <version>5.7.19</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.junit.vintage</groupId>
                    <artifactId>junit-vintage-engine</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
```

## 3. application.yml

```yml
spring:
  datasource:
    username: root
    password: 123456
    url: jdbc:mysql:///spring_boot_mybatis_plus?serverTimezone=GMT%2B8&characterEncoding=utf-8

mybatis-plus:
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl                                                               # SQL 日志
  type-aliases-package: aygx.mybatis.plus.entity                                                                        # 别名
```

## 4. User

```java
package aygx.mybatis.plus.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 暗影孤星
 * @date 2022/3/12 11:47
 * @description
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    /**
     * 主键
     */
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 邮箱
     */
    private String email;

    public User(Long id) {
        this.id = id;
    }

    public User(String name, Integer age, String email) {
        this.name = name;
        this.age = age;
        this.email = email;
    }
}
```

## 5. UserMapper

```java
package aygx.mybatis.plus.mapper;

import aygx.mybatis.plus.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 暗影孤星
 * @date 2022/3/12 11:47
 * @description
 */
public interface UserMapper extends BaseMapper<User> {
}
```

## 6. BootApplication

```java
package aygx.mybatis.plus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 暗影孤星
 * @date 2022/3/12 11:47
 * @description MapperScan mybatis包扫描
 */
@SpringBootApplication
@MapperScan("aygx.mybatis.plus.mapper")
public class BootApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootApplication.class, args);
    }
}
```

## 7. MybatisPlusApplicationTests

```java
package aygx.mybatis.plus;

import aygx.mybatis.plus.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author 暗影孤星
 * @date 2022/3/12 11:50
 * @description
 */
@Slf4j
@SpringBootTest
public class MybatisPlusApplicationTests {
    @Resource
    private UserMapper userMapper;

    @Test
    void testSelectList() {
        // selectList()方法的参数：封装了查询条件 null 表示无条件
        userMapper.selectList(null).forEach(System.out::println);
    }
}

```

![image-20220311113210086](img/image-20220311113210086.png)

# 14. 通用 CRUD

## 1. 通用 Mapper

- 通用 `CRUD` 封装 `BaseMapper` 接口，为 `Mybatis-Plus` 启动时自动解析实体表关系映射转换为 `Mybatis` 内部对象注入容器
- 泛型 `T` 为任意实体对象
- 参数 `Serializable` 为任意类型主键 `Mybatis-Plus` 不推荐使用复合主键约定每一张表都有自己的唯一 `id` 主键
- 对象 `Wrapper` 为条件构造器

```java
package com.baomidou.mybatisplus.core.mapper;

/**
 * Mapper 继承该接口后，无需编写 mapper.xml 文件，即可获得CRUD功能
 * <p>这个 Mapper 支持 id 泛型</p>
 *
 * @author hubin
 * @since 2016-01-23
 */
public interface BaseMapper<T> extends Mapper<T> {

    /**
     * 插入一条记录
     *
     * @param entity 实体对象
     */
    int insert(T entity);

    /**
     * 根据 ID 删除
     *
     * @param id 主键ID
     */
    int deleteById(Serializable id);

    /**
     * 根据实体(ID)删除
     *
     * @param entity 实体对象
     * @since 3.4.4
     */
    int deleteById(T entity);

    /**
     * 根据 columnMap 条件，删除记录
     *
     * @param columnMap 表字段 map 对象
     */
    int deleteByMap(@Param(Constants.COLUMN_MAP) Map<String, Object> columnMap);

    /**
     * 根据 entity 条件，删除记录
     *
     * @param queryWrapper 实体对象封装操作类（可以为 null,里面的 entity 用于生成 where 语句）
     */
    int delete(@Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

    /**
     * 删除（根据ID或实体 批量删除）
     *
     * @param idList 主键ID列表或实体列表(不能为 null 以及 empty)
     */
    int deleteBatchIds(@Param(Constants.COLLECTION) Collection<?> idList);

    /**
     * 根据 ID 修改
     *
     * @param entity 实体对象
     */
    int updateById(@Param(Constants.ENTITY) T entity);

    /**
     * 根据 whereEntity 条件，更新记录
     *
     * @param entity        实体对象 (set 条件值,可以为 null)
     * @param updateWrapper 实体对象封装操作类（可以为 null,里面的 entity 用于生成 where 语句）
     */
    int update(@Param(Constants.ENTITY) T entity, @Param(Constants.WRAPPER) Wrapper<T> updateWrapper);

    /**
     * 根据 ID 查询
     *
     * @param id 主键ID
     */
    T selectById(Serializable id);

    /**
     * 查询（根据ID 批量查询）
     *
     * @param idList 主键ID列表(不能为 null 以及 empty)
     */
    List<T> selectBatchIds(@Param(Constants.COLLECTION) Collection<? extends Serializable> idList);

    /**
     * 查询（根据 columnMap 条件）
     *
     * @param columnMap 表字段 map 对象
     */
    List<T> selectByMap(@Param(Constants.COLUMN_MAP) Map<String, Object> columnMap);

    /**
     * 根据 entity 条件，查询一条记录
     * <p>查询一条记录，例如 qw.last("limit 1") 限制取一条记录, 注意：多条数据会报异常</p>
     *
     * @param queryWrapper 实体对象封装操作类（可以为 null）
     */
    default T selectOne(@Param(Constants.WRAPPER) Wrapper<T> queryWrapper) {
        List<T> ts = this.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(ts)) {
            if (ts.size() != 1) {
                throw ExceptionUtils.mpe("One record is expected, but the query result is multiple records");
            }
            return ts.get(0);
        }
        return null;
    }

    /**
     * 根据 Wrapper 条件，判断是否存在记录
     *
     * @param queryWrapper 实体对象封装操作类
     * @return
     */
    default boolean exists(Wrapper<T> queryWrapper) {
        Long count = this.selectCount(queryWrapper);
        return null != count && count > 0;
    }

    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param queryWrapper 实体对象封装操作类（可以为 null）
     */
    Long selectCount(@Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

    /**
     * 根据 entity 条件，查询全部记录
     *
     * @param queryWrapper 实体对象封装操作类（可以为 null）
     */
    List<T> selectList(@Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

    /**
     * 根据 Wrapper 条件，查询全部记录
     *
     * @param queryWrapper 实体对象封装操作类（可以为 null）
     */
    List<Map<String, Object>> selectMaps(@Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

    /**
     * 根据 Wrapper 条件，查询全部记录
     * <p>注意： 只返回第一个字段的值</p>
     *
     * @param queryWrapper 实体对象封装操作类（可以为 null）
     */
    List<Object> selectObjs(@Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

    /**
     * 根据 entity 条件，查询全部记录（并翻页）
     *
     * @param page         分页查询条件（可以为 RowBounds.DEFAULT）
     * @param queryWrapper 实体对象封装操作类（可以为 null）
     */
    <P extends IPage<T>> P selectPage(P page, @Param(Constants.WRAPPER) Wrapper<T> queryWrapper);

    /**
     * 根据 Wrapper 条件，查询全部记录（并翻页）
     *
     * @param page         分页查询条件
     * @param queryWrapper 实体对象封装操作类
     */
    <P extends IPage<Map<String, Object>>> P selectMapsPage(P page, @Param(Constants.WRAPPER) Wrapper<T> queryWrapper);
}
```

### 1. insert

```java
/**
 * 插入
 */
@Test
public void testInsert() {

    User user = new User("暗影孤星", 18, "123456789!qq.com");
    System.out.println("影响的行数：" + userMapper.insert(user));
    System.out.println("id自动回填：" + user.getId());
}
```

![image-20220311113702899](img/image-20220311113702899.png)

### 2. update

```java
/**
 * 根据ID修改
 */
@Test
public void testUpdate() {
    System.out.println("影响的行数：" + userMapper.updateById(User.builder().id(1L).age(28).build()));
}
```

![image-20220311113943867](img/image-20220311113943867.png)

### 3. delete

```java
/**
 * 根据ID删除
 */
@Test
public void testDelete() {
    System.out.println("影响的行数：" + userMapper.deleteById(1L));
}

/**
 * 根据ID批量删除
 */
@Test
public void testDeleteBatchIds() {
    System.out.println("受影响行数：" + userMapper.deleteBatchIds(Arrays.asList(1L, 2L, 3L)));
}

/**
 * 根据map集合中所设置的条件删除记录
 */
@Test
public void testDeleteByMap() {
    System.out.println("受影响行数：" + userMapper.deleteByMap(JSONUtil.createObj().set("age", 18).set("name", "暗影孤星")));
}
```

![image-20220311114146977](img/image-20220311114146977.png)

![image-20220311114213502](img/image-20220311114213502.png)

![image-20220311114234493](img/image-20220311114234493.png)

### 4. select

```java
/**
 * 根据ID查询
 */
@Test
public void testSelectById() {
    System.out.println(userMapper.selectById(4L));
}

/**
 * 根据ID集合查询
 */
@Test
public void testSelectBatchIds() {
    userMapper.selectBatchIds(Arrays.asList(4L, 5L)).forEach(System.out::println);
}

/**
 * 根据map条件查询
 */
@Test
public void testSelectByMap() {
    userMapper.selectByMap(JSONUtil.createObj().set("age", 18).set("name", "暗影孤星")).forEach(System.out::println);
}
```

![image-20220311114539455](img/image-20220311114539455.png)

![image-20220311114459186](img/image-20220311114459186.png)

![image-20220311114610891](img/image-20220311114610891.png)

## 2. 通用 Service

- 通用 `Service CRUD` 封装 `IService` 接口，进一步封装 `CRUD` 采用 `get` 查询单行、`remove` 删除、`list` 查询集合、`page` 分页。
- 泛型 `T` 为任意实体对象
- 如果存在自定义通用 Service 方法，可以创建自己的 `IBaseService` 继承 `Mybatis-Plus` 提供的基类
- 对象 `Wrapper` 为 [条件构造器](https://baomidou.com/01.指南/02.核心功能/wrapper.html)

```java
package com.baomidou.mybatisplus.extension.service;

/**
 * 顶级 Service
 *
 * @author hubin
 * @since 2018-06-23
 */
public interface IService<T> {

    /**
     * 默认批次提交数量
     */
    int DEFAULT_BATCH_SIZE = 1000;

    /**
     * 插入一条记录（选择字段，策略插入）
     *
     * @param entity 实体对象
     */
    default boolean save(T entity) {
        return SqlHelper.retBool(getBaseMapper().insert(entity));
    }

    /**
     * 插入（批量）
     *
     * @param entityList 实体对象集合
     */
    @Transactional(rollbackFor = Exception.class)
    default boolean saveBatch(Collection<T> entityList) {
        return saveBatch(entityList, DEFAULT_BATCH_SIZE);
    }

    /**
     * 插入（批量）
     *
     * @param entityList 实体对象集合
     * @param batchSize  插入批次数量
     */
    boolean saveBatch(Collection<T> entityList, int batchSize);

    /**
     * 批量修改插入
     *
     * @param entityList 实体对象集合
     */
    @Transactional(rollbackFor = Exception.class)
    default boolean saveOrUpdateBatch(Collection<T> entityList) {
        return saveOrUpdateBatch(entityList, DEFAULT_BATCH_SIZE);
    }

    /**
     * 批量修改插入
     *
     * @param entityList 实体对象集合
     * @param batchSize  每次的数量
     */
    boolean saveOrUpdateBatch(Collection<T> entityList, int batchSize);

    /**
     * 根据 ID 删除
     *
     * @param id 主键ID
     */
    default boolean removeById(Serializable id) {
        return SqlHelper.retBool(getBaseMapper().deleteById(id));
    }

    /**
     * 根据 ID 删除
     *
     * @param id      主键(类型必须与实体类型字段保持一致)
     * @param useFill 是否启用填充(为true的情况,会将入参转换实体进行delete删除)
     * @return 删除结果
     * @since 3.5.0
     */
    default boolean removeById(Serializable id, boolean useFill) {
        throw new UnsupportedOperationException("不支持的方法!");
    }

    /**
     * 根据实体(ID)删除
     *
     * @param entity 实体
     * @since 3.4.4
     */
    default boolean removeById(T entity) {
        return SqlHelper.retBool(getBaseMapper().deleteById(entity));
    }

    /**
     * 根据 columnMap 条件，删除记录
     *
     * @param columnMap 表字段 map 对象
     */
    default boolean removeByMap(Map<String, Object> columnMap) {
        Assert.notEmpty(columnMap, "error: columnMap must not be empty");
        return SqlHelper.retBool(getBaseMapper().deleteByMap(columnMap));
    }

    /**
     * 根据 entity 条件，删除记录
     *
     * @param queryWrapper 实体包装类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     */
    default boolean remove(Wrapper<T> queryWrapper) {
        return SqlHelper.retBool(getBaseMapper().delete(queryWrapper));
    }

    /**
     * 删除（根据ID 批量删除）
     *
     * @param list 主键ID或实体列表
     */
    default boolean removeByIds(Collection<?> list) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        return SqlHelper.retBool(getBaseMapper().deleteBatchIds(list));
    }

    /**
     * 批量删除
     *
     * @param list    主键ID或实体列表
     * @param useFill 是否填充(为true的情况,会将入参转换实体进行delete删除)
     * @return 删除结果
     * @since 3.5.0
     */
    @Transactional(rollbackFor = Exception.class)
    default boolean removeByIds(Collection<?> list, boolean useFill) {
        if (CollectionUtils.isEmpty(list)) {
            return false;
        }
        if (useFill) {
            return removeBatchByIds(list, true);
        }
        return SqlHelper.retBool(getBaseMapper().deleteBatchIds(list));
    }

    /**
     * 批量删除(jdbc批量提交)
     *
     * @param list 主键ID或实体列表(主键ID类型必须与实体类型字段保持一致)
     * @return 删除结果
     * @since 3.5.0
     */
    @Transactional(rollbackFor = Exception.class)
    default boolean removeBatchByIds(Collection<?> list) {
        return removeBatchByIds(list, DEFAULT_BATCH_SIZE);
    }

    /**
     * 批量删除(jdbc批量提交)
     *
     * @param list    主键ID或实体列表(主键ID类型必须与实体类型字段保持一致)
     * @param useFill 是否启用填充(为true的情况,会将入参转换实体进行delete删除)
     * @return 删除结果
     * @since 3.5.0
     */
    @Transactional(rollbackFor = Exception.class)
    default boolean removeBatchByIds(Collection<?> list, boolean useFill) {
        return removeBatchByIds(list, DEFAULT_BATCH_SIZE, useFill);
    }

    /**
     * 批量删除(jdbc批量提交)
     *
     * @param list      主键ID或实体列表
     * @param batchSize 批次大小
     * @return 删除结果
     * @since 3.5.0
     */
    default boolean removeBatchByIds(Collection<?> list, int batchSize) {
        throw new UnsupportedOperationException("不支持的方法!");
    }

    /**
     * 批量删除(jdbc批量提交)
     *
     * @param list      主键ID或实体列表
     * @param batchSize 批次大小
     * @param useFill   是否启用填充(为true的情况,会将入参转换实体进行delete删除)
     * @return 删除结果
     * @since 3.5.0
     */
    default boolean removeBatchByIds(Collection<?> list, int batchSize, boolean useFill) {
        throw new UnsupportedOperationException("不支持的方法!");
    }

    /**
     * 根据 ID 选择修改
     *
     * @param entity 实体对象
     */
    default boolean updateById(T entity) {
        return SqlHelper.retBool(getBaseMapper().updateById(entity));
    }

    /**
     * 根据 UpdateWrapper 条件，更新记录 需要设置sqlset
     *
     * @param updateWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper}
     */
    default boolean update(Wrapper<T> updateWrapper) {
        return update(null, updateWrapper);
    }

    /**
     * 根据 whereEntity 条件，更新记录
     *
     * @param entity        实体对象
     * @param updateWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper}
     */
    default boolean update(T entity, Wrapper<T> updateWrapper) {
        return SqlHelper.retBool(getBaseMapper().update(entity, updateWrapper));
    }

    /**
     * 根据ID 批量更新
     *
     * @param entityList 实体对象集合
     */
    @Transactional(rollbackFor = Exception.class)
    default boolean updateBatchById(Collection<T> entityList) {
        return updateBatchById(entityList, DEFAULT_BATCH_SIZE);
    }

    /**
     * 根据ID 批量更新
     *
     * @param entityList 实体对象集合
     * @param batchSize  更新批次数量
     */
    boolean updateBatchById(Collection<T> entityList, int batchSize);

    /**
     * TableId 注解存在更新记录，否插入一条记录
     *
     * @param entity 实体对象
     */
    boolean saveOrUpdate(T entity);

    /**
     * 根据 ID 查询
     *
     * @param id 主键ID
     */
    default T getById(Serializable id) {
        return getBaseMapper().selectById(id);
    }

    /**
     * 查询（根据ID 批量查询）
     *
     * @param idList 主键ID列表
     */
    default List<T> listByIds(Collection<? extends Serializable> idList) {
        return getBaseMapper().selectBatchIds(idList);
    }

    /**
     * 查询（根据 columnMap 条件）
     *
     * @param columnMap 表字段 map 对象
     */
    default List<T> listByMap(Map<String, Object> columnMap) {
        return getBaseMapper().selectByMap(columnMap);
    }

    /**
     * 根据 Wrapper，查询一条记录 <br/>
     * <p>结果集，如果是多个会抛出异常，随机取一条加上限制条件 wrapper.last("LIMIT 1")</p>
     *
     * @param queryWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     */
    default T getOne(Wrapper<T> queryWrapper) {
        return getOne(queryWrapper, true);
    }

    /**
     * 根据 Wrapper，查询一条记录
     *
     * @param queryWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     * @param throwEx      有多个 result 是否抛出异常
     */
    T getOne(Wrapper<T> queryWrapper, boolean throwEx);

    /**
     * 根据 Wrapper，查询一条记录
     *
     * @param queryWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     */
    Map<String, Object> getMap(Wrapper<T> queryWrapper);

    /**
     * 根据 Wrapper，查询一条记录
     *
     * @param queryWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     * @param mapper       转换函数
     */
    <V> V getObj(Wrapper<T> queryWrapper, Function<? super Object, V> mapper);

    /**
     * 查询总记录数
     *
     * @see Wrappers#emptyWrapper()
     */
    default long count() {
        return count(Wrappers.emptyWrapper());
    }

    /**
     * 根据 Wrapper 条件，查询总记录数
     *
     * @param queryWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     */
    default long count(Wrapper<T> queryWrapper) {
        return SqlHelper.retCount(getBaseMapper().selectCount(queryWrapper));
    }

    /**
     * 查询列表
     *
     * @param queryWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     */
    default List<T> list(Wrapper<T> queryWrapper) {
        return getBaseMapper().selectList(queryWrapper);
    }

    /**
     * 查询所有
     *
     * @see Wrappers#emptyWrapper()
     */
    default List<T> list() {
        return list(Wrappers.emptyWrapper());
    }

    /**
     * 翻页查询
     *
     * @param page         翻页对象
     * @param queryWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     */
    default <E extends IPage<T>> E page(E page, Wrapper<T> queryWrapper) {
        return getBaseMapper().selectPage(page, queryWrapper);
    }

    /**
     * 无条件翻页查询
     *
     * @param page 翻页对象
     * @see Wrappers#emptyWrapper()
     */
    default <E extends IPage<T>> E page(E page) {
        return page(page, Wrappers.emptyWrapper());
    }

    /**
     * 查询列表
     *
     * @param queryWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     */
    default List<Map<String, Object>> listMaps(Wrapper<T> queryWrapper) {
        return getBaseMapper().selectMaps(queryWrapper);
    }

    /**
     * 查询所有列表
     *
     * @see Wrappers#emptyWrapper()
     */
    default List<Map<String, Object>> listMaps() {
        return listMaps(Wrappers.emptyWrapper());
    }

    /**
     * 查询全部记录
     */
    default List<Object> listObjs() {
        return listObjs(Function.identity());
    }

    /**
     * 查询全部记录
     *
     * @param mapper 转换函数
     */
    default <V> List<V> listObjs(Function<? super Object, V> mapper) {
        return listObjs(Wrappers.emptyWrapper(), mapper);
    }

    /**
     * 根据 Wrapper 条件，查询全部记录
     *
     * @param queryWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     */
    default List<Object> listObjs(Wrapper<T> queryWrapper) {
        return listObjs(queryWrapper, Function.identity());
    }

    /**
     * 根据 Wrapper 条件，查询全部记录
     *
     * @param queryWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     * @param mapper       转换函数
     */
    default <V> List<V> listObjs(Wrapper<T> queryWrapper, Function<? super Object, V> mapper) {
        return getBaseMapper().selectObjs(queryWrapper).stream().filter(Objects::nonNull).map(mapper).collect(Collectors.toList());
    }

    /**
     * 翻页查询
     *
     * @param page         翻页对象
     * @param queryWrapper 实体对象封装操作类 {@link com.baomidou.mybatisplus.core.conditions.query.QueryWrapper}
     */
    default <E extends IPage<Map<String, Object>>> E pageMaps(E page, Wrapper<T> queryWrapper) {
        return getBaseMapper().selectMapsPage(page, queryWrapper);
    }

    /**
     * 无条件翻页查询
     *
     * @param page 翻页对象
     * @see Wrappers#emptyWrapper()
     */
    default <E extends IPage<Map<String, Object>>> E pageMaps(E page) {
        return pageMaps(page, Wrappers.emptyWrapper());
    }

    /**
     * 获取对应 entity 的 BaseMapper
     *
     * @return BaseMapper
     */
    BaseMapper<T> getBaseMapper();

    /**
     * 获取 entity 的 class
     *
     * @return {@link Class<T>}
     */
    Class<T> getEntityClass();

    /**
     * 以下的方法使用介绍:
     *
     * 一. 名称介绍
     * 1. 方法名带有 query 的为对数据的查询操作, 方法名带有 update 的为对数据的修改操作
     * 2. 方法名带有 lambda 的为内部方法入参 column 支持函数式的
     * 二. 支持介绍
     *
     * 1. 方法名带有 query 的支持以 {@link ChainQuery} 内部的方法名结尾进行数据查询操作
     * 2. 方法名带有 update 的支持以 {@link ChainUpdate} 内部的方法名为结尾进行数据修改操作
     *
     * 三. 使用示例,只用不带 lambda 的方法各展示一个例子,其他类推
     * 1. 根据条件获取一条数据: `query().eq("column", value).one()`
     * 2. 根据条件删除一条数据: `update().eq("column", value).remove()`
     *
     */

    /**
     * 链式查询 普通
     *
     * @return QueryWrapper 的包装类
     */
    default QueryChainWrapper<T> query() {
        return ChainWrappers.queryChain(getBaseMapper());
    }

    /**
     * 链式查询 lambda 式
     * <p>注意：不支持 Kotlin </p>
     *
     * @return LambdaQueryWrapper 的包装类
     */
    default LambdaQueryChainWrapper<T> lambdaQuery() {
        return ChainWrappers.lambdaQueryChain(getBaseMapper());
    }

    /**
     * 链式查询 lambda 式
     * kotlin 使用
     *
     * @return KtQueryWrapper 的包装类
     */
    default KtQueryChainWrapper<T> ktQuery() {
        return ChainWrappers.ktQueryChain(getBaseMapper(), getEntityClass());
    }

    /**
     * 链式查询 lambda 式
     * kotlin 使用
     *
     * @return KtQueryWrapper 的包装类
     */
    default KtUpdateChainWrapper<T> ktUpdate() {
        return ChainWrappers.ktUpdateChain(getBaseMapper(), getEntityClass());
    }

    /**
     * 链式更改 普通
     *
     * @return UpdateWrapper 的包装类
     */
    default UpdateChainWrapper<T> update() {
        return ChainWrappers.updateChain(getBaseMapper());
    }

    /**
     * 链式更改 lambda 式
     * <p>注意：不支持 Kotlin </p>
     *
     * @return LambdaUpdateWrapper 的包装类
     */
    default LambdaUpdateChainWrapper<T> lambdaUpdate() {
        return ChainWrappers.lambdaUpdateChain(getBaseMapper());
    }

    /**
     * <p>
     * 根据updateWrapper尝试更新，否继续执行saveOrUpdate(T)方法
     * 此次修改主要是减少了此项业务代码的代码量（存在性验证之后的saveOrUpdate操作）
     * </p>
     *
     * @param entity 实体对象
     */
    default boolean saveOrUpdate(T entity, Wrapper<T> updateWrapper) {
        return update(entity, updateWrapper) || saveOrUpdate(entity);
    }
}
```

### 1. UserService

```java
package aygx.mybatis.plus.service;

import aygx.mybatis.plus.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author 暗影孤星
 * @date 2022/3/12 11:47
 * @description
 */
public interface UserService extends IService<User> {
}
```

### 2. UserServiceImpl

```java
package aygx.mybatis.plus.service.impl;

import aygx.mybatis.plus.entity.User;
import aygx.mybatis.plus.mapper.UserMapper;
import aygx.mybatis.plus.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author 暗影孤星
 * @date 2022/3/12 11:47
 * @description ServiceImpl实现了IService，提供了IService中基础功能的实现
 * 若ServiceImpl无法满足业务需求，则可以使用自定的UserService定义方法，并在实现类中实现
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
```

### 3. insert

```java
/**
 * 批量插入
 */
@Test
public void testSaveBatch() {
    // SQL长度有限制，海量数据插入单条SQL无法实行，
    // 因此MP将批量插入放在了通用Service中实现，而不是通用Mapper
    ArrayList<User> users = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
        User user = new User();
        user.setName("ybc" + i);
        user.setAge(20 + i);
        users.add(user);
    }
    userService.saveBatch(users);
}
```

![image-20220311114347838](img/image-20220311114347838.png)

### 4. select

```java
/**
 * 获取总记录数据
 */
@Test
public void testGetCount() {
    System.out.println("总记录数：" + userService.count());
}
```

![image-20220311114637192](img/image-20220311114637192.png)

# 15 常用注解

## 1. @TableName

```java
package com.baomidou.mybatisplus.annotation;

import java.lang.annotation.*;

/**
 * 表名注解，标识实体类对应的表
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
public @interface TableName {

    /**
     * 实体对应的表名
     */
    String value() default "";

    /**
     * schema
     * 配置此值将覆盖全局配置的 schema
     */
    String schema() default "";

    /**
     * 是否保持使用全局的 tablePrefix 的值
     * <p> 只生效于 既设置了全局的 tablePrefix 也设置了上面 {@link #value()} 的值 </p>
     * <li> 如果是 false , 全局的 tablePrefix 不生效 </li>
     *
     * @since 3.1.1
     */
    boolean keepGlobalPrefix() default false;

    /**
     * 实体映射结果集,
     * 只生效于 mp 自动注入的 method
     */
    String resultMap() default "";

    /**
     * 是否自动构建 resultMap 并使用,
     * 只生效于 mp 自动注入的 method,
     * 如果设置 resultMap 则不会进行 resultMap 的自动构建并注入,
     * 只适合个别字段 设置了 typeHandler 或 jdbcType 的情况
     */
    boolean autoResultMap() default false;

    /**
     * 需要排除的属性名
     *
     * @since 3.3.1
     */
    String[] excludeProperty() default {};
}
```

## 2. @TableId

`MyBatis-Plus` 在实现 `CRUD` 时，会默认将 `id` 作为主键列，并在插入数据时默认基于雪花算法的策略生成 `id`

```java
package com.baomidou.mybatisplus.annotation;

import java.lang.annotation.*;

/**
 * 表主键标识
 *
 * @author hubin
 * @since 2016-01-23
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface TableId {

    /**
     * 字段名（该值可无）
     */
    String value() default "";

    /**
     * 主键类型
     * {@link IdType}
     */
    IdType type() default IdType.NONE;
}
```

## 3. @IdType

```java
package com.baomidou.mybatisplus.annotation;

import lombok.Getter;

/**
 * 生成ID类型枚举类
 */
@Getter
public enum IdType {
    /**
     * 数据库ID自增
     * <p>该类型请确保数据库设置了 ID自增 否则无效</p>
     */
    AUTO(0),
    /**
     * 该类型为未设置主键类型(注解里等于跟随全局,全局里约等于 INPUT)
     */
    NONE(1),
    /**
     * 用户输入ID
     * <p>该类型可以通过自己注册自动填充插件进行填充</p>
     */
    INPUT(2),

    /* 以下3种类型、只有当插入对象ID 为空，才自动填充。 */
    /**
     * 分配ID (主键类型为number或string）,
     * 默认实现类 {@link com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator}(雪花算法)
     *
     * @since 3.3.0
     */
    ASSIGN_ID(3),
    /**
     * 分配UUID (主键类型为 string)
     * 默认实现类 {@link com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator}(UUID.replace("-",""))
     */
    ASSIGN_UUID(4);

    private final int key;

    IdType(int key) {
        this.key = key;
    }
}
```

## 4. @TableField

```java
package com.baomidou.mybatisplus.annotation;


import java.lang.annotation.*;

/**
 * 字段注解（非主键）
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface TableField {

    /**
     * 数据库字段值
     */
    String value() default "";

    /**
     * 是否为数据库表字段
     */
    boolean exist() default true;

    /**
     * 字段 where 实体查询比较条件
     */
    String condition() default "";

    /**
     * 字段 update set 部分注入, 该注解优于 el 注解使用
     * <p>
     * 例1：@TableField(.. , update="%s+1") 其中 %s 会填充为字段
     * 输出 SQL 为：update 表 set 字段=字段+1 where ...
     * <p>
     * 例2：@TableField(.. , update="now()") 使用数据库时间
     * 输出 SQL 为：update 表 set 字段=now() where ...
     */
    String update() default "";

    /**
     * 字段验证策略之 insert: 当insert操作时，该字段拼接insert语句时的策略
     */
    FieldStrategy insertStrategy() default FieldStrategy.DEFAULT;

    /**
     * 字段验证策略之 update: 当更新操作时，该字段拼接set语句时的策略
     */
    FieldStrategy updateStrategy() default FieldStrategy.DEFAULT;

    /**
     * 字段验证策略之 where: 表示该字段在拼接where条件时的策略
     */
    FieldStrategy whereStrategy() default FieldStrategy.DEFAULT;

    /**
     * 字段自动填充策略
     * <p>
     * 在对应模式下将会忽略 insertStrategy 或 updateStrategy 的配置,等于断言该字段必有值
     */
    FieldFill fill() default FieldFill.DEFAULT;

    /**
     * 是否进行 select 查询
     * <p>
     * 大字段可设置为 false 不加入 select 查询范围
     */
    boolean select() default true;

    /**
     * 是否保持使用全局的 columnFormat 的值
     *
     * @since 3.1.1
     */
    boolean keepGlobalFormat() default false;

    /**
     * {@link ResultMapping#property} and {@link ParameterMapping#property}
     *
     * @since 3.4.4
     */
    String property() default "";

    /**
     * JDBC类型 (该默认值不代表会按照该值生效),
     * 只生效于 mp 自动注入的 method,
     * 建议配合 {@link TableName#autoResultMap()} 一起使用
     * <p>
     * {@link ResultMapping#jdbcType} and {@link ParameterMapping#jdbcType}
     *
     * @since 3.1.2
     */
    JdbcType jdbcType() default JdbcType.UNDEFINED;

    /**
     * 类型处理器 (该默认值不代表会按照该值生效),
     * 只生效于 mp 自动注入的 method,
     * 建议配合 {@link TableName#autoResultMap()} 一起使用
     * <p>
     * {@link ResultMapping#typeHandler} and {@link ParameterMapping#typeHandler}
     *
     * @since 3.1.2
     */
    Class<? extends TypeHandler> typeHandler() default UnknownTypeHandler.class;

    /**
     * 只在使用了 {@link #typeHandler()} 时判断是否辅助追加 javaType
     * <p>
     * 一般情况下不推荐使用
     * {@link ParameterMapping#javaType}
     *
     * @since 3.4.0 @2020-07-23
     */
    boolean javaType() default false;

    /**
     * 指定小数点后保留的位数,
     * 只生效于 mp 自动注入的 method,
     * 建议配合 {@link TableName#autoResultMap()} 一起使用
     * <p>
     * {@link ParameterMapping#numericScale}
     *
     * @since 3.1.2
     */
    String numericScale() default "";
}
```

## 5. @TableLogic

```java
package com.baomidou.mybatisplus.annotation;

import java.lang.annotation.*;

/**
 * 表字段逻辑处理注解（逻辑删除）
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface TableLogic {

    /**
     * 默认逻辑未删除值（该值可无、会自动获取全局配置）
     */
    String value() default "";

    /**
     * 默认逻辑删除值（该值可无、会自动获取全局配置）
     */
    String delval() default "";
}
```

## 6. @Version

```java
package com.baomidou.mybatisplus.annotation;

import java.lang.annotation.*;

/**
 * 乐观锁注解
 * <p>
 * 支持的字段类型:
 * long,
 * Long,
 * int,
 * Integer,
 * java.util.Date,
 * java.sql.Timestamp,
 * java.time.LocalDateTime
 *
 * @author TaoYu
 * @since 2016-01-23
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface Version {
}
```

## 7. @EnumValue

```java
package com.baomidou.mybatisplus.annotation;

import java.lang.annotation.*;

/**
 * 支持普通枚举类字段, 只用在enum类的字段上
 * <p>当实体类的属性是普通枚举，且是其中一个字段，使用该注解来标注枚举类里的那个属性对应字段</p>
 * <p>
 * 使用方式参考 com.baomidou.mybatisplus.test.h2.H2StudentMapperTest
 * <pre>
 * @TableName("student")
 * class Student {
 *     private Integer id;
 *     private String name;
 *     private GradeEnum grade;//数据库grade字段类型为int
 * }
 *
 * public enum GradeEnum {
 *     PRIMARY(1,"小学"),
 *     SECONDORY("2", "中学"),
 *     HIGH(3, "高中");
 *
 *     @EnumValue
 *     private final int code;
 *     private final String descp;
 * }
 * </pre>
 * </p>
 *
 * @author yuxiaobin
 * @date 2018/8/30
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface EnumValue {
}
```

## 8. @OrderBy 

```java
package com.baomidou.mybatisplus.annotation;

import java.lang.annotation.*;

/**
 * 自动排序，用法与SpringDtaJpa的OrderBy类似，优先级低于 wrapper 条件查询
 * 在执行MybatisPlus的方法selectList(),Page()等非手写查询时自动带上.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface OrderBy {

    /**
     * 默认倒序，设置 true 顺序
     */
    boolean asc() default false;

    @Deprecated
    boolean isDesc() default true;

    /**
     * 数字越小越靠前
     */
    short sort() default Short.MAX_VALUE;
}
```

# 16. 条件构造器

![image-20220311144420621](img/image-20220311144420621.png)

## 1. AbstractWrapper

-  `QueryWrapper`  和  `UpdateWrapper`  的父类，用于生成 `SQL` 的 `WHERE` 条件。
-  第一个入参 `boolean condition` 表示该条件是否加入最后生成的 `SQL` 中。
-  使用中如果入参的 `Map` 或者 `List` 为空,则不会加入最后生成的 `SQL` 中。

### 1. allEq

```java
allEq(Map<R, V> params)
allEq(Map<R, V> params, boolean null2IsNull)
allEq(boolean condition, Map<R, V> params, boolean null2IsNull)
allEq(BiPredicate<R, V> filter, Map<R, V> params)
allEq(BiPredicate<R, V> filter, Map<R, V> params, boolean null2IsNull)
allEq(boolean condition, BiPredicate<R, V> filter, Map<R, V> params, boolean null2IsNull) 
```

`filter`：是否允许字段传入比对条件中。

`params` ：`key` 为数据库字段名，`value` 为字段值。

`null2IsNull` ：`true` 则在 `map` 的 `value` 为 `null` 时调用 `isNull()` 方法；`false` 则忽略 `value` 为 `null` 的。

> allEq({id:1,name:"老王",age:null})--->id = 1 and name = '老王' and age is null
>
> allEq({id:1,name:"老王",age:null}, false)--->id = 1 and name = '老王'
>
> allEq((k,v) -> k.indexOf("a") >= 0, {id:1,name:"老王",age:null})--->name = '老王' and age is null
>
> allEq((k,v) -> k.indexOf("a") >= 0, {id:1,name:"老王",age:null}, false)--->name = '老王'

### 2. eq

```java
eq(R column, Object val)
eq(boolean condition, R column, Object val)
```

> eq("name", "老王")--->name = '老王'

### 3. ne

```java
ne(R column, Object val)
ne(boolean condition, R column, Object val)
```

> ne("name", "老王")--->name <> '老王'

### 4. gt

```java
gt(R column, Object val)
gt(boolean condition, R column, Object val)
```

> gt("age", 18)--->age > 18

### 5. ge

```java
ge(R column, Object val)
ge(boolean condition, R column, Object val)
```

> ge("age", 18)--->age >= 18

### 6. lt

```java
lt(R column, Object val)
lt(boolean condition, R column, Object val)
```

> lt("age", 18)--->age < 18

### 7. le

```java
le(R column, Object val)
le(boolean condition, R column, Object val)
```

> le("age", 18)--->age <= 18

### 8. between

```java
between(R column, Object val1, Object val2)
between(boolean condition, R column, Object val1, Object val2)
```

> between("age", 18, 30)--->age between 18 and 30

### 9. notBetween

```java
notBetween(R column, Object val1, Object val2)
notBetween(boolean condition, R column, Object val1, Object val2)
```

> notBetween("age", 18, 30)--->age not between 18 and 30

### 10. like

```java
like(R column, Object val)
like(boolean condition, R column, Object val)
```

> like("name", "王")--->name like '%王%'

### 11. likeLeft

```java
likeLeft(R column, Object val)
likeLeft(boolean condition, R column, Object val)
```

> likeLeft("name", "王")--->name like '%王'

### 12. likeRight

```java
likeRight(R column, Object val)
likeRight(boolean condition, R column, Object val)
```

> likeRight("name", "王")--->name like '王%'

### 13. isNull

```java
isNull(R column)
isNull(boolean condition, R column)
```

> isNull("name")--->name is null

### 14. isNotNull

```java
isNotNull(R column)
isNotNull(boolean condition, R column)
```

> isNotNull("name")--->name is not null

### 15. in

```java
in(R column, Collection<?> value)
in(boolean condition, R column, Collection<?> value)
```

> in("age",{1,2,3})--->age in (1,2,3)

### 16. notIn

```java
notIn(R column, Collection<?> value)
notIn(boolean condition, R column, Collection<?> value)
```

> notIn("age",{1,2,3})--->age not in (1,2,3)

### 17. inSql

```java
inSql(R column, String inValue)
inSql(boolean condition, R column, String inValue)
```

> inSql("age", "1,2,3,4,5,6")--->age in (1,2,3,4,5,6)
>
> inSql("id", "select id from table where id < 3")--->id in (select id from table where id < 3)

### 18. notInSql

```java
notInSql(R column, String inValue)
notInSql(boolean condition, R column, String inValue)
```

> notInSql("age", "1,2,3,4,5,6")--->age not in (1,2,3,4,5,6)
>
> notInSql("id", "select id from table where id < 3")--->id not in (select id from table where id < 3)

### 19. groupBy

```java
groupBy(R... columns)
groupBy(boolean condition, R... columns)
```

> groupBy("id", "name")--->group by id,name

### 20. orderByAsc

```java
orderByAsc(R... columns)
orderByAsc(boolean condition, R... columns)
```

> orderByDesc("id", "name")--->order by id DESC,name DESC

### 21. orderByDesc

```java
orderByDesc(R... columns)
orderByDesc(boolean condition, R... columns)
```

> orderByDesc("id", "name")--->order by id DESC,name DESC

### 22. orderBy

```java
orderBy(boolean condition, boolean isAsc, R... columns)
```

> orderBy(true, true, "id", "name")--->order by id ASC,name AS

### 23. having

```java
having(String sqlHaving, Object... params)
having(boolean condition, String sqlHaving, Object... params)
```

> having("sum(age) > 10")--->having sum(age) > 10
>
> having("sum(age) > {0}", 11)--->having sum(age) > 11

### 24. func

```java
func(Consumer<Children> consumer)
func(boolean condition, Consumer<Children> consumer)
```

> func(i -> if(true) {i.eq("id", 1)} else {i.ne("id", 1)})

### 25. or

```java
or()
or(boolean condition)
or(Consumer<Param> consumer)
or(boolean condition, Consumer<Param> consumer)
```

> 主动调用 `or` 表示紧接着下一个**方法**不是用 `and` 连接
>
> eq("id",1).or().eq("name","老王")--->id = 1 or name = '老王'
>
> or(i -> i.eq("name", "李白").ne("status", "活着"))--->or (name = '李白' and status <> '活着')

### 26. and

```java
and(Consumer<Param> consumer)
and(boolean condition, Consumer<Param> consumer)
```

> and(i -> i.eq("name", "李白").ne("status", "活着"))--->and (name = '李白' and status <> '活着')

### 27. nested

```java
nested(Consumer<Param> consumer)
nested(boolean condition, Consumer<Param> consumer)
```

> nested(i -> i.eq("name", "李白").ne("status", "活着"))--->(name = '李白' and status <> '活着')

### 28. apply

```java
apply(String applySql, Object... params)
apply(boolean condition, String applySql, Object... params)
```

> 可用于数据库**函数** 动态入参的 `params` 对应前面 `applySql` 内部的 `{index}` 部分，这样是不会有 `SQL` 注入风险的。
>
> apply("id = 1")--->id = 1
> apply("date_format(dateColumn,'%Y-%m-%d') = '2008-08-08'")--->date_format(dateColumn,'%Y-%m-%d') = '2008-08-08'")
> apply("date_format(dateColumn,'%Y-%m-%d') = {0}", "2008-08-08")--->date_format(dateColumn,'%Y-%m-%d') = '2008-08-08'")

### 29. last

```java
last(String lastSql)
last(boolean condition, String lastSql)
```

> 无视优化规则直接拼接到 `SQL` 的最后。
>
> 只能调用一次,多次调用以最后一次为准 有 `SQL` 注入的风险,请谨慎使用。
>
> last("limit 1")

### 30. exists

```java
exists(String existsSql)
exists(boolean condition, String existsSql)
```

> exists("select id from table where age = 1")--->exists (select id from table where age = 1)

### 31. notExists

```java
notExists(String notExistsSql)
notExists(boolean condition, String notExistsSql)
```

> notExists("select id from table where age = 1")--->not exists (select id from table where age = 1)

## 2. QueryWrapper

继承自 `AbstractWrapper` ，自身的内部属性 `entity` 也用于生成 `where` 条件及 `LambdaQueryWrapper`， 可以通过 `new QueryWrapper().lambda()` 方法获取

### 1. select

```java
select(String... sqlSelect)
select(Predicate<TableFieldInfo> predicate)
select(Class<T> entityClass, Predicate<TableFieldInfo> predicate)
```

> select("id", "name", "age")
>
> select(i -> i.getProperty().startsWith("test"))

## 3. UpdateWrapper

### 1. set

```java
set(String column, Object val)
set(boolean condition, String column, Object val)
```

> set("name", "老李头")
>
> set("name", "")--->数据库字段值变为空字符串
>
> set("name", null)--->数据库字段值变为null

### 2. setSql

```java
setSql(String sql)
```

> setSql("name = '老李头'")

## 4. lambda

在 `QueryWrapper` 中获取 `LambdaQueryWrapper`，在 `UpdateWrapper` 中获取`LambdaUpdateWrapper`

## 5. test

```java
@Test
public void test01() {
    // SELECT id,name,age,email FROM user WHERE (name LIKE ? AND age BETWEEN ? AND ? AND email IS NOT NULL) ORDER BY age DESC,id ASC
    QueryWrapper<User> queryWrapper = new QueryWrapper<User>()
            .like("name", "a")
            .between("age", 20, 30)
            .isNotNull("email")
            .orderByDesc("age")
            .orderByAsc("id");
    userMapper.selectList(queryWrapper).forEach(System.out::println);
}

@Test
public void test02() {
    // 条件构造器也可以构建删除语句的条件
    // DELETE FROM user WHERE (email IS NULL)
    System.out.println("受影响的行数：" + userMapper.delete(new QueryWrapper<User>().isNull("email")));
}

@Test
public void test03() {
    // UPDATE user SET age=?, email=? WHERE (name LIKE ? AND age > ? OR email IS NULL)
    QueryWrapper<User> queryWrapper = new QueryWrapper<User>()
            .like("name", "a")
            .gt("age", 20)
            .or()
            .isNull("email");
    User user = new User();
    user.setAge(18);
    user.setEmail("123456789@.com");
    System.out.println("受影响的行数：" + userMapper.update(user, queryWrapper));
}

@Test
public void test04() {
    // lambda表达式内的逻辑优先运算
    // UPDATE user SET name=?, email=? WHERE (name LIKE ? AND (age > ? OR email IS NULL))
    QueryWrapper<User> queryWrapper = new QueryWrapper<User>()
            .like("name", "a")
            .and(i -> i.gt("age", 20).or().isNull("email"));
    System.out.println("受影响的行数：" + userMapper.update(User.builder().name("aygx").email("123456789@email").build(), queryWrapper));
}

@Test
public void test05() {
    // SELECT name,age FROM user
    // selectMaps()返回Map集合列表，通常配合select()使用，避免User对象中没有被查询到的列值为null
    userMapper.selectMaps(new QueryWrapper<User>().select("name", "age")).forEach(System.out::println);
}

@Test
public void test06() {
    // SELECT id,name,age,email FROM user WHERE (id IN (select id from user where id <= 3))
    userMapper.selectList(new QueryWrapper<User>().inSql("id", "select id from user where id <= 3")).forEach(System.out::println);
}

@Test
public void test07() {
    UpdateWrapper<User> updateWrapper = new UpdateWrapper<User>()
            .set("age", 18)
            .like("name", "a")
            .set("email", "123456789@qq.com.com")
            .and(i -> i.gt("age", 20).or().isNull("email"));
    // UPDATE user SET age=?,email=? WHERE (name LIKE ? AND (age > ? OR email IS NULL))
    System.out.println("受影响的行数：" + userMapper.update(null, updateWrapper));
    // UPDATE user SET name=?, age=?, age=?,email=? WHERE (name LIKE ? AND (age > ? OR email IS NULL))
    System.out.println("受影响的行数：" + userMapper.update(User.builder().name("暗影孤星").age(12).build(), updateWrapper));

}

@Test
public void test08() {

    // SELECT id,name,age,email FROM user WHERE (age >= ? AND age <= ? AND name LIKE ?)
    LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
            .ge(User::getAge, 10)
            .le(User::getAge, 20)
            .like(User::getName, "aygx");
    userMapper.selectList(queryWrapper).forEach(System.out::println);
}

@Test
public void test09() {
    // UPDATE user SET age=?,email=? WHERE (name LIKE ? AND (age < ? OR email IS NULL))
    LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<User>()
            .set(User::getAge, 18)
            .like(User::getName, "aygx")
            .set(User::getEmail, "123456789@qq.com")
            .and(i -> i.lt(User::getAge, 24).or().isNull(User::getEmail));

    System.out.println("受影响的行数：" + userMapper.update(new User(), updateWrapper));
}
```

# 17. 插件

## 1. 分页

### 1. bean

```java
/**
 * 分页组件
 */
@Bean
public MybatisPlusInterceptor mybatisPlusInterceptor() {
    MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
    interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
    return interceptor;
}
```

### 2. test

```java
@Test
public void testPage() {
    // 设置分页参数
    Page<User> page = new Page<>(1, 5);
    userMapper.selectPage(page, null);
    // 获取分页数据
    page.getRecords().forEach(System.out::println);
    System.out.println("当前页数：" + page.getCurrent());
    System.out.println("每页显示条数：" + page.getSize());
    System.out.println("总记录数：" + page.getTotal());
    System.out.println("总页数：" + page.getPages());
    System.out.println("是否有上一页：" + page.hasPrevious());
    System.out.println("是否有下一页：" + page.hasNext());
}
```

### 3. xml 自定义分页

#### 1. UserMapper

```java
/**
 * 根据年龄查询用户列表
 *
 * @param page 分页对象,xml中可以从里面进行取值,传递参数 Page 即自动分页,必须放在第一位
 * @param age  年龄
 * @return 用户数据
 */
IPage<User> selectPageVo(@Param("page") Page<User> page, @Param("age") Integer age);
```

#### 2. UserMapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="aygx.mybatis.plus.mapper.UserMapper">

    <!--SQL片段，记录基础字段-->
    <select id="selectPageVo" resultType="User">
        SELECT *  FROM user WHERE age > #{age}
    </select>

</mapper>
```

#### 3. test

```java
@Test
public void testSelectPageVo() {
    //设置分页参数
    Page<User> page = new Page<>(1, 5);
    userMapper.selectPageVo(page, 20);
    //获取分页数据
    page.getRecords().forEach(System.out::println);
    System.out.println("当前页： " + page.getCurrent());
    System.out.println("每页显示的条数： " + page.getSize());
    System.out.println("总记录数： " + page.getTotal());
    System.out.println("总页数： " + page.getPages());
    System.out.println("是否有上一页： " + page.hasPrevious());
    System.out.println("是否有下一页： " + page.hasNext());
}
```

## 2. 乐观锁

乐观锁：在操作时很乐观，认为操作不会产生并发问题(不会有其他线程对数据进行修改)，因此不会上锁。但是在更新时会判断其他线程在这之前有没有对数据进行修改。

悲观锁：总是假设最坏的情况，每次取数据时都认为其他线程会修改，所以都会加（悲观）锁。一旦加锁，不同线程同时执行时,只能有一个线程执行，其他的线程在入口处等待，直到锁被释放。

### 1. SQL

```sql
CREATE TABLE product (
	id BIGINT ( 20 ) NOT NULL COMMENT '主键ID',
	NAME VARCHAR ( 30 ) NULL DEFAULT NULL COMMENT '商品名称',
	price INT ( 11 ) DEFAULT 0 COMMENT '价格',
	VERSION INT ( 11 ) DEFAULT 0 COMMENT '乐观锁版本号',
	PRIMARY KEY ( id ) 
);
INSERT INTO product ( id, NAME, price )VALUES( 1, '外星人笔记本', 100 );
```

### 2. bean

```java
/**
 * 分页组件
 */
@Bean
public MybatisPlusInterceptor mybatisPlusInterceptor() {
    MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
    // 乐观锁插件
    interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
    // 分页插件
    interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
    return interceptor;
}
```

### 3. test

```java
@Test
public void testConcurrentUpdate() {
    // 1、小李
    Product p1 = productMapper.selectById(1L);
    System.out.println("小李取出的价格： " + p1.getPrice());
    // 2、小王
    Product p2 = productMapper.selectById(1L);
    System.out.println("小王取出的价格： " + p2.getPrice());

    // 3、小李将价格加了50元，存入了数据库
    p1.setPrice(p1.getPrice() + 50);
    System.out.println("小李修改结果： " + productMapper.updateById(p1));
    // 4、小王将商品减了30元，存入了数据库
    p2.setPrice(p2.getPrice() - 30);
    int result2 = productMapper.updateById(p2);
    if (0 == result2) {
        // 失败重试，重新获取version并更新
        p2 = productMapper.selectById(1L);
        p2.setPrice(p2.getPrice() - 30);
        result2 = productMapper.updateById(p2);
    }
    System.out.println("小王修改结果： " + result2);
    // 最后的结果
    System.out.println("最后的结果： " + productMapper.selectById(1L).getPrice());
}
```

# 18. 通用枚举

# 19. 代码生成

## 1. pom.xml

```xml
<!-- mybatis-plus 代码生成 -->
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>3.5.2</version>
</dependency>
<!-- mybatis-plus 代码生成模板引擎 -->
<dependency>
    <groupId>org.freemarker</groupId>
    <artifactId>freemarker</artifactId>
    <version>2.3.31</version>
</dependency>
```

## 2. test

```java
package aygx.mybatis.plus;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * @author 暗影孤星
 * @date 2022/3/13 15:47
 * @description
 */
public class FastAutoGeneratorTest {
    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql:///spring_boot_mybatis_plus?serverTimezone=GMT%2B8&characterEncoding=utf-8", "root", "123456")
                .globalConfig(builder -> {
                    builder.author("暗影孤星")                     // 设置作者
                            .fileOverride()                      // 覆盖已生成文件
                            .outputDir("G:\\01_Note\\01_Java\\03_JavaFrame\\02_Mybatis\\02_Code\\mybatis_plus\\src\\test")                   // 指定输出目录
                            .commentDate("yyyy-MM-dd hh:mm:ss"); //  指定注释日期格式化
                })
                .packageConfig(builder -> {
                    builder.parent("aygx")                                                  // 设置父包名
                            .moduleName("system")                                           // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapper, "/aygx")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("user");       // 设置需要生成的表名
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }
}
```

# 21. 多数据源

## 1. 概述

`dynamic-datasource-spring-boot-starter` 是一个基于 `springboot` 的快速集成多数据源的启动器。其支持 **Jdk 1.7+, SpringBoot 1.4.x 1.5.x 2.x.x**。收费文档。

## 2. 特性

- 支持 **数据源分组** ，适用于多种场景 纯粹多库 读写分离 一主多从 混合模式。
- 支持数据库敏感配置信息 **加密** `ENC()`。
- 支持每个数据库独立初始化表结构 `schema` 和数据库 `database`。
- 支持无数据源启动，支持懒加载数据源（需要的时候再创建连接）。
- 支持 **自定义注解** ，需继承DS(3.2.0+)。
- 提供并简化对 `Druid`，`HikariCp`，`BeeCp`，`Dbcp2`的快速集成。
- 提供对 `Mybatis-Plus`，`Quartz`，`ShardingJdbc`，`P6sy`，`Jndi` 等组件的集成方案。
- 提供 **自定义数据源来源** 方案（如全从数据库加载）。
- 提供项目启动后 **动态增加移除数据源** 方案。
- 提供 `Mybatis` 环境下的 **纯读写分离** 方案。
- 提供使用 **spel动态参数** 解析数据源方案。内置 `spel`，`session`，`header`，支持自定义。
- 支持 **多层数据源嵌套切换** 。（ServiceA >>> ServiceB >>> ServiceC）。
- 提供 **基于seata的分布式事务方案。**
- 提供 **本地多数据源事务方案。** 附：不能和原生spring事务混用。

## 3. 约定



## 4. pom.xml

```xml
<dependency>
  <groupId>com.baomidou</groupId>
  <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
  <version>${version}</version>
</dependency>
```

# 22. IDEA 插件

# 23. 面试

## 1. MyBatis 缓存

`MyBatis` 的缓存分为一级缓存和二级缓存,一级缓存放在 `session` 里面，默认就有；二级缓存放在它的命名空间里，默认是不打开的。使用二级缓存属性类需要实现 `Serializable` 序列化接口(可用来保存对象的状态)，可在它的映射文件中配置 `<cache/>`。

## 2. MyBatis 分页

1.  直接编写带有物理分页的参数的 `SQL` 语句来完成物理分页语句。
2.  使用 `RowBounds`针对 `ResultSet` 结果集执行的内存分页。 
3.  分页插件：实现 `Mybatis` 提供的接口实现自定义插件，在插件的拦截方法内拦截待执行的 `SQL`，然后根据 `dialect` 方言重写 `SQL`。例如：`SELECT * FROM student`，拦截 `SQL` 后重写为：`SELECT t.* FROM（SELECT * FROM student）t limit 0，10`。

## 3. MyBatis 插件

1.  `Mybatis` 仅可以编写针对 `ParameterHandler`、`ResultSetHandler`、`StatementHandler`、`Executor` 这 4 种接口的插件。`Mybatis` 通过动态代理为需要拦截的接口生成代理对象以实现接口方法拦截功能，每当执行这 4 种接口对象的方法时就会进入拦截方法，具体就是 `InvocationHandler` 的 `invoke()` 方法会拦截那些指定需要拦截的方法。
2.  实现 `Mybatis` 的 `Interceptor` 接复写 `intercept()` 方法，然后在给插件编写注解，指定要拦截哪一个接口的哪些方法即可。

## 4. 动态 SQL

`Mybatis` 动态 `SQL`可以让我们在 `Xml` 映射文件内以标签的形式编写动态 `SQL`，完成逻辑判断和动态拼接 `SQL` 的功能。`Mybatis` 提供了9种动态 `SQL` 标签：`trim|where|set|foreach|if|choose|when|otherwise|bind`。其执行原理为，使用 `OGNL` 从 `SQL` 参数对象中计算表达式的值，根据表达式的值动态拼接 `SQL` 以此来完成动态 `SQL` 的功能。

## 5. #{} 、${}区别

1.   `${} ` 是 `Properties` 文件中的变量占位符。它可以用于标签属性值和 `SQL` 内部，属于静态文本替换。
2.     `#{}` 是 `SQL` 的参数占位符。`MyBatis`  在处理 `#{}` 时会将 `SQL` 中的 `#{}` 替换为 `?`，并使用 PreparedStatement 的 `set()` 进行赋值，可以有效防止 `SQL` 注入。

## 6. 获取自增主键

### 1. 注解

```java
@Options(useGeneratedKeys =true, keyProperty =”id”)
int insert();
```

### 2. xml

```xml
<insert id=”insert” useGeneratedKeys=”true” keyProperty=” id”>
	# SQL
</insert>
```

## 7. 为什么说 Mybatis 是半自动 ORM 映射工具

`Hibernate` 属于全自动 `ORM` 映射工具，使用 `Hibernate` 查询关联对象或者关联集合对象时可以根据对象关系模型直接获取，所以它是全自动的。而 `Mybatis` 在查询关联对象或关联集合对象时需要手动编写 `SQL` 来完成，所以称之为半自动 `ORM` 映射工具。

## 8. 延迟加载

`Mybatis` 仅支持 `association` 关联对象和 `collection` 关联集合对象的延迟加载，`association` 指的就是一对一，`collection` 指的就是一对多查询。在 `Mybatis` 配置文件中可以配置是否启用延迟加载 （`lazyLoadingEnabled=true|false`）。它的原理是使用 `CGLIB` 创建目标对象的代理对象，当调用目标方法时进入拦截器方法，比如调用 `a.getB().getName()`，拦截器 `invoke()`方法发现 `a.getB()`是 `null` ，那么就会单独
发送事先保存好的查询关联 B 对象的 `SQL`把 B 查询上来，然后调用 `a.setB(b)`，于是 a 的对象 b 属性就有值了，接着完成 `a.getB().getName()`方法的调用。这就是延迟加载的基本原理。

## 9. 简述 Mybatis 的 Xml 映射文件和 Mybatis 内部数据结构之间的映射关系？

`Mybatis` 将所有 `Xml` 配置信息都封装到 `All-In-One` 重量级对象 `Configuration` 内部。在映射文件中，`<parameterMap>` 标签会被解析为 `ParameterMap` 对象，其每个子元素会被解析为 `ParameterMapping` 对象。`<resultMap>` 标签会被解析为 `ResultMap` 对象，其每个子元素会被解析为 `ResultMapping` 对象。每一个 `<select>`、`<insert>`、`<update>`、`<delete>` 标签均会被解析为 `MappedStatement` 对象，标签内的 `SQL` 会被解析为 `BoundSql` 对象。

## 10. 接口绑定有几种实现方式?

一种是通过注解绑定,就是在接口的方法上面加上 `@Select`、`@Update` 等注解里面包含 `SQL` 语句来绑定；另外一种就是通过 `Xml` 里面写 `SQL` 来绑定，在这种情况下要指定 `Xml` 映射文件里面的 `namespace` 必须为接口的全限定名，接口方法名为映射文件的`statement` 的 `ID`

## 11. MyBatis 实现一对一有几种方式?

有联合查询和嵌套查询。联合查询是几个表联合查询，通过在 `resultMap` 里面配置 `association` 节点配置一对一的类就可以完成。嵌套查询是先查一个表，根据这个表里面的结果的外键 `id` 去再另外一个表里面查询数据，也是通过 `association` 配置，但另外一个表的查询通过 `select` 属性配置。

## 12. Mybatis 能执行一对一、一对多的关联查询吗？

`Mybatis` 不仅可以执行一对一、一对多的关联查询，还可以执行多对一，多对多的关联查询，多对一查询其实就是一对一查询，只需要把 `selectOne()` 修改为 `selectList()` 即可；多对多查询其实就是一对多查询，只需要把 `selectOne()` 修改为 `selectList()` 即可。关联对象查询有两种实现方式，一种是单独发送一个 `SQL` 去查询关联对象赋给主象，
然后返回主对象。另一种是使用嵌套查询，嵌套查询的含义为使用 `join` 查询，一部分列是 A 对象的属性值，另外一部分列是关联对象 B 的属性值，好处是只发一个 `SQL` 查询，就可以把主对象和其关联对象查出来。

## 13. Mybatis 是如何将 SQL 执行结果封装为目标对象并返回的？

第一种是使用 `<resultMap>` 标签逐一定义列名和对象属性名之间的映射关系。第二种是使用 `SQL` 列别名功能将列别名书写为对象属性名。有了列名与属性名的映射关系后 `Mybatis` 通过反射创建对象同时使用反射给对象的属性逐一赋值并返回，那些找不到映射关系的属性是无法完成赋值的。

## 14. 模糊查询 like 语句该怎么写

```sql
-- 1. 在JAVA代码中添加SQL通配符
string wildcardname = "%smi%";
list<name> names = mapper.selectlike(wildcardname);
<select id= "selectlike" resultType= "foo">
	select * from foo where bar like #{value}
</select>

-- 2. 在sql语句中拼接通配符。因为#{…}解析成SQL语句时候，会在变量外侧自动加单引号，所以%这里  需要使用双引号，不然会查不到任何结果。
string wildcardname = "smi";
list<name> names = mapper.selectlike(wildcardname);
<select id= "selectlike" resultType= "foo">
	select * from foo where bar like "%"#{value}"%"
</select>

-- 3. 在sql语句中拼接通配符（会引起SQL注入）
string wildcardname = "smi";
list<name> names = mapper.selectlike(wildcardname);
<select id= "selectlike" resultType= "foo">
	select * from foo where bar like '%${value}%'
</select>

-- 4. 在sql语句中使用函数（推荐）
string wildcardname = "smi";
list<name> names = mapper.selectlike(wildcardname);
<select id= "selectlike" resultType= "foo">
	select * from foo where bar like CONCAT('%',#{value},'%') 
</select>

-- 5. 在sql语句中bind标签
<select id= "selectlike" resultType= "foo">
	<bindname = "pattern" value = "'%' + username + '%'"/> 
	select * from foo where bar like LIKE #{pattern}
</select>
```

## 15. Xml 映射文件对应 Mapper 接口的方法可以重载么

`Mapper` 接口的全限定名是 `Xml` 映射文件中的 `namespace` 的值，`Mapper` 接口的方法名是 `Xml` 映射文件中的 `MappedStatement` 的 `ID` 值，`Mapper` 接口的参数值是 `Xml` 映射文件中的 `MappedStatement` 的参数值。当调用方法时，通过接口全限定名+方法名定位一个 `MappedStatement`。

`Mapper` 接口可以重载，但是映射文件中的 `ID` 不能重复。接口重载的条件为：1、多个接口对应的映射只能有一个。2、仅有一个无参方法和一个有参方法。3、多个有参方法时，参数数量必须保持一致，且使用相同的 `@Param`。

```java
/**
 * 获取学生数据
 *
 * @return 学生数据
 */
List<Student> selectAll();

/**
 * 获取学生数据
 *
 * @param name 名称
 * @return 学生数据
 */
List<Student> selectAll(@Param("name") String name);

/**
 * 获取学生数据
 *
 * @param name 名称
 * @param age  年龄
 * @return 学生数据
 */
List<Student> selectAll(@Param("name") String name, @Param("age") Integer age);
```

```xml
<select id="selectAll" resultType="aygx.mybatis.entity.Student">
    SELECT * FROM student
    <where>
        <if test="name != null ">
            AND name like CONCAT('%',#{name},'%')
        </if>
        <if test="age != null ">
            AND age = #{age}
        </if>
    </where>
</select>
```

`org.apache.ibatis.scripting.xmltags. DynamicContext. ContextAccessor#getProperty` 方法用于获取 `<if>` 标签中的条件值。

```java
@Override
public Object getProperty(Map context, Object target, Object name) {
    Map map = (Map) target;

    Object result = map.get(name);
    if (map.containsKey(name) || result != null) {
        return result;
    }

    Object parameterObject = map.get(PARAMETER_OBJECT_KEY);
    if (parameterObject instanceof Map) {
        return ((Map)parameterObject).get(name);
    }

    return null;
}
```

`parameterObject` 为 `map`，存放的是 `Mapper` 接口中参数相关信息。

```java
public V get(Object key) {
    if (!super.containsKey(key)) {
        throw new BindingException("Parameter '" + key + "' not found. Available parameters are " + keySet());
    }
    return super.get(key);
}
```

1. `selectAll()` 方法执行时，`parameterObject` 为 `null`，`getProperty` 方法返回 `null`  值，`<if>` 标签获取的所有条件值都为 `null`，动态 `SQL` 可以正常执行。

1. `selectAll("暗影孤星")` 方法执行时，`parameterObject` 为 `map`，包含了 `name` 和`param1` 两个 `key` 值。当获取 `<if>` 标签中 `name` 的属性值时，进入`((Map)parameterObject).get(name)` 方法中，`map` 中 `key` 不包含 `age`，所以抛出异常。
2. `selectAll("暗影孤星",18)`方法执行时，`parameterObject`中 包含`name`,`param1`,`age`,`param2` 四个 `key`  值，`name` 和 `age`属性都可以获取到，动态 `SQL` 正常执行。

`Mybatis` 运行时会使用 `JDK` 动态代理为 `Mapper` 接口生成代理对象，代理对象会拦截接口方法转而执行 `MappedStatement` 所代表的 `SQL`并将 `SQL` 执行结果返回。

## 16. Mybatis 映射文件中，如果 A 标签通过 include 引用了 B 标签的内容，请问，B 标签能否定义在 A 标签的后面，还是说必须定义在 A 标签的前面？
虽然 `Mybatis` 解析 `Xml` 映射文件是按照顺序解析的，但是被引用的 `B` 标签依然可以定义在任何地方。原理是 `Mybatis` 解析 `A` 标签时发现 `A` 标签引用了 `B` 标签，但是 `B` 标签尚未解析到，此时 `Mybatis` 会将 `A` 标签标记为未解析状态，然后继续解析余下的标签，待所有标签解析完毕，`Mybatis` 会重新解析那些被标记为未解析的标签，此时再解析 `A` 标签时`B` 标签已经存在，`A` 标签也就可以正常解析完成了。

## 17. Mybatis 的 Xml 映射文件，id 是否可以重复？

不同的 `Xml` 映射文件，如果配置了 `namespace`，那么 `id` 可以重复；如果没有配置 `namespace`，那么 `id` 不能重复；毕竟 `namespace` 不是必须的，只是最佳实践而已。原因就是 `namespace+id` 是作为 `Map<String, MappedStatement>` 的 `key` 使用的，如果没有 `namespace` 就剩下 `id`，那么 `id` 重复会导致数据互相覆盖。

## 18. Mybatis 中如何执行批处理？

### 1. foreach

foreach 的主要用在构建 in 条件中，它可以在 SQL 语句中进行迭代一个集合。foreach 标签的属性主要有 item、index、collection、open、separator、close。

```
item 表示集合中每一个元素进行迭代时的别名，随便起的变量名；
index 指定一个名字，用于表示在迭代过程中，每次迭代到的位置，不常用；
open 表示该语句以什么开始，常用"("；
separator 表示在每次进行迭代之间以什么符号作为分隔符，常用","
close 表示以什么结束，常用")"
```

在使用 foreach 的时候最关键的也是最容易出错的就是 collection 属性，该属性是必须指定的，但是在不同情况下，该属性的值是不一样的，主要有一下3种情况：

-  如果传入的是单参数且参数类型是一个 List 的时候，collection 属性值为 list
-  如果传入的是单参数且参数类型是一个 array 数组的时候，collection的属性值为 array
-  如果传入的参数是多个的时候，我们就需要把它们封装成一个 Map了，当然单参数也可以封装成map，实际上如果你在传入参数的时候，在 MyBatis 里面也是会把它封装成一个 Map 的.

```xml
// 推荐
<insert id = "addEmpsBatch" > 
    INSERT INTO emp(ename,gender,email,did) VALUES
		<foreach collection = "emps" item = "emp" separator = ","> 
   			(#{emp.eName},#{emp.gender},#{emp.email},#{emp.dept.id}) 
    	</foreach>
</insert>

<insert id= "addEmpsBatch" >
    <foreach collection= "emps" item = "emp" separator=";">
		INSERT INTO emp(ename,gender,email,did) VALUES(#{emp.eName},#{emp.gender},#{emp.email},#{emp.dept.id}) 
    </foreach>
</insert>
```

### 2. ExecutorType.BATCH

Mybatis 内置的 ExecutorType 有3种，默认为 simple ,该模式下它为每个语句的执行创建一个新的预处理语句，单条提交 SQL；而 batch 模式重复使用已经预处理的语句，并且批量执行所有更新语句，显然batch性能将更优； 但 batch 模式也有自己的问题，比如在 Insert 操作时，在事务没有提交之前，是没有办法获取到自增的 id，这在某型情形下是不符合业务要求的具体用法如下：

```java

```

## 19. Mybatis 是否可以映射 Enum 枚举类？

`Mybatis` 可以映射枚举类，不单可以映射枚举类，`Mybatis` 可以映射任何对象到表的一列上。映射方式为自定义一个 `TypeHandler`，实现 `TypeHandler` 的 `setParameter()` 和 `getResult()` 接口方法。`TypeHandler` 有两个作用，一是完成从 `javaType` 至 `jdbcType` 的转换，二是完成 `jdbcType` 至 `javaType` 的转换，体现为 `setParameter()` 和 `getResult()` 两个方法，分别代表设置 `SQL` 问号占位符参数和获取列查询结果。

## 20. resultType、 resultMap 的区别

类的名字和数据库相同时可以直接设置 `resultType` 参数为 `POJO` 类， 若类的名字和数据库不同需要设置 `resultMap` 将结果名字和 `POJO` 名字进行转换。

## 21. 预编译

`SQL` 预编译指的是数据库驱动在发送 `SQL` 语句和参数给 `DBMS` 之前对 `SQL` 语句进行编译，这样 `DBMS` 执行 `SQL` 时就不需要重新编译。`JDBC` 中使用对象 `PreparedStatement` 来抽象预编译语句，预编译阶段可以优化 `SQL` 的执行，预编译之后的 `SQL` 多数情况下可以直接执行，`DBMS` 不需要再次编译，越复杂的 `SQL` 编译的复杂度将越大，预编译阶段可以合并多次操作为一个操作。同时预编译语句对象可以重复利用。把一个 `SQL` 预编译后产生的 `PreparedStatement` 对象缓存下来，下次对于同一个 `SQL` 可以直接使用这个缓存的 `PreparedState` 对象。`Mybatis` 默认情况下，将对所有的 `SQL` 进行预编译。

## 22. Mybatis 都有哪些执行器

`Mybatis` 有三种基本的执行器。`SimpleExecutor`、`ReuseExecutor`、`BatchExecutor`。

1.  **`SimpleExecutor`**：每执行一次 `UPDATE` 或 `SELECT` 就开启一个 `Statement` 对象，用完立刻关闭该对象。
2.  **`ReuseExecutor`**：执行 `UPDATE` 或 `SELECT`，以 `SQL` 作为 `key` 查找 `Statement` 对象，存在就使用，不存在就创建，用完后不关闭 `Statement` 对象，而是放置于 `Map` 以便下次使用。
3.  **`BatchExecutor`**：执行 `UPDATE `将所有 `SQL` 都添加到批处理中（`addBatch()`），等待统一执行（`executeBatch()`），它缓存了多个 `Statement` 对象，每个 `Statement` 对象都是批处理完毕后，等待逐一执行 `executeBatch( )` 批处理。与 `JDBC` 批处理相同。

在 `Mybatis` 配置文件 `setting` 中可以指定默认的执行器类型（`ExecutorType`），也可以手动给 `DefaultSqlSessionFactory` 的创建 `SqlSession` 的方法传递 `ExecutorType` 类型参数配置默认的执行器。

## 23. 在 mapper 中如何传递多个参数

```sql
-- 1 #{} 里面的数字代表传入参数的顺序
public User selectUser(String name,int deptId);
<select id = "selectUser" resultMap= "UserResultMap">
	select * from user where username = #{0} and dept_id = #{1}
</select>

-- 2 @param 注解 通过#{属性}来获取
public User selectUser(@Param("userName") String name,@Param("deptId") int deptId);
<select id = "selectUser" resultMap = "UserResultMap"> 
	select * from user where user_name = #{userName} and dept_id = #{deptId}
</select>

-- 3 Map 传参 通过#{key}来获取
public User selectUser(Map<String, Object> params);
<select id = "selectUser" parameterType = "java.util.Map" resultMap = "UserResultMap"> 
	select * from user where user_name = #{userName} and dept_id = #{deptId}
</select>

-- 3 Bean 传参 通过#{属性}来获取
public User selectUser(User user);
<select id = "selectUser" parameterType = "user" resultMap= "user"> 
	select * from user where user_name = #{userName} and dept_id = #{deptId}
</select>

```
