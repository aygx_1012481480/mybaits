package aygx.mybatis.plus.mapper;

import aygx.mybatis.plus.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 暗影孤星
 * @date 2022/3/13 11:47
 * @description
 */
public interface ProductMapper extends BaseMapper<Product> {
}
