package aygx.mybatis.plus.service;

import aygx.mybatis.plus.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author 暗影孤星
 * @date 2022/3/12 11:47
 * @description
 */
public interface UserService extends IService<User> {
}
