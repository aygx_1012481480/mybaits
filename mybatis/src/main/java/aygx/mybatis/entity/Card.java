package aygx.mybatis.entity;

import lombok.Data;

/**
 * @author 暗影孤星
 * @date 2022/03/12 12:37
 * @description
 */
@Data
public class Card {
    /**
     * 名称
     */
    private Integer id;

    /**
     * 号码
     */
    private Integer number;

    /**
     *
     */
    private Person person;
}
