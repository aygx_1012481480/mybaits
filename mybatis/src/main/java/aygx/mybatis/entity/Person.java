package aygx.mybatis.entity;

import lombok.Data;

/**
 * @author 暗影孤星
 * @date 2022/03/12 12:37
 * @description
 */
@Data
public class Person {
    /**
     * 名称
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 年龄
     */
    private Integer age;
}
