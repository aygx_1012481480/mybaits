package aygx.mybatis.repository;

import aygx.mybatis.entity.Student;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2022/03/12 11:37
 * @description
 */
public interface StudentRepository {
    /**
     * 获取学生数据
     *
     * @return 学生数据
     */
    List<Student> selectAll();

    /**
     * 根据名称和年龄获取学生数据
     *
     * @return 学生数据
     */
    List<Student> selectNameAndAge(Student student);

    /**
     * 根据名称和年龄获取学生数据
     *
     * @return 学生数据
     */
    List<Student> selectChoose(Student student);


    /**
     * where语句
     *
     * @param student 学生数据
     * @return 学生数据
     */
    List<Student> selectWhere(Student student);

    /**
     * set语句
     *
     * @param student 学生数据
     */
    void updateSet(Student student);

    /**
     * Foreach语句
     *
     * @param studentList 学生数据
     */
    void insertForeach(List<Student> studentList);


    /**
     * 根据班级ID获取学生数据
     *
     * @param classesId 班级ID
     * @return 学生数据
     */
    @Select("SELECT * FROM student WHERE classes_id=#{classesId}")
    List<Student> selectByCid(Integer classesId);


    /**
     * 查询学生数据
     *
     * @return 学生数据
     */
    @Select("SELECT DISTINCT s.id,s.name,s.age FROM student s,course_student cs WHERE cs.student_id=s.id")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(
                    property = "courseList",                                                           // 被包含对象的变量名
                    javaType = List.class,                                                             // 被包含对象的实际数据类型
                    column = "id",                                                                     // 根据查询出student表的id来作为关联条件，去查询中间表和课程表
                    many = @Many(select = "aygx.mybatis.repository.CourseRepository.selectBySid")      // many=@Many一对多查询的固定写法
            )
    })
    List<Student> selectList();
}
