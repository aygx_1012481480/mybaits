package aygx.mybatis.plus.mapper;

import aygx.mybatis.plus.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * @author 暗影孤星
 * @date 2022/3/12 11:47
 * @description
 */
public interface UserMapper extends BaseMapper<User> {
    /**
     * 根据年龄查询用户列表
     *
     * @param page 分页对象,xml中可以从里面进行取值,传递参数 Page 即自动分页,必须放在第一位
     * @param age  年龄
     * @return 用户数据
     */
    IPage<User> selectPageVo(@Param("page") Page<User> page, @Param("age") Integer age);
}
