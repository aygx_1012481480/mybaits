package aygx.mybatis.repository;

import aygx.mybatis.entity.Card;
import aygx.mybatis.entity.Person;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2022/03/12 13:37
 * @description
 */
public interface CardRepository {
    /**
     * 获取身份证数据
     *
     * @return 身份证数据
     */
    @Select("SELECT * FROM card")
    @Results({
            @Result(column = "id", property = "id"),                                            // 列映射
            @Result(
                    property = "person",                                                        // 变量名
                    javaType = Person.class,                                                    // 被包含对象的实际数据类型
                    column = "person_id",                                                       // 列名
                    one = @One(select = "aygx.mybatis.repository.PersonRepository.selectById")  // one=@One 一对一固定写法
            )
    })
    List<Card> selectAll();
}
