package aygx.mybatis.repository;

import aygx.mybatis.entity.Classes;
import aygx.mybatis.entity.Student;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2021/7/6 22:45
 * @description
 */
public interface ManyToManyRepository {
    List<Student> selectAll();
}
