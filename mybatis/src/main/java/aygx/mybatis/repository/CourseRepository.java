package aygx.mybatis.repository;

import aygx.mybatis.entity.Course;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 暗影孤星
 * @date 2021/7/6 23:09
 * @description
 */
public interface CourseRepository {
    /**
     * 根据学生id获取课程数据
     *
     * @param id 学生ID
     * @return 课程数据
     */
    @Select("SELECT c.id,c.name FROM course_student cs,course c WHERE cs.course_id=c.id AND cs.student_id=#{id}")
    List<Course> selectBySid(Integer id);
}
